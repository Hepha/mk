package com.endless.hov.mangakeeper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.endless.hov.mangakeeper.API.mangaapi;
import com.endless.hov.mangakeeper.CustomAdapters.ListCoverAdapter;
import com.endless.hov.mangakeeper.model.searchmodel;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnlineListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OnlineListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OnlineListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    //

    ListView lvMain;
    RelativeLayout loading;


    List<searchmodel> listSearch = new ArrayList<>();
    ArrayAdapter<String> adapter;
    ListCoverAdapter adapterCover;



    SharedPreferences settings;

    final String endPoint = "https://doodle-manga-scraper.p.mashape.com";

    final String pref_site = "apiList";
    final String pref_name = "PREF_NAME";

    String siteid;


    int count = 1;

    boolean bCover = false;

    //
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OnlineListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OnlineListFragment newInstance(String param1, String param2) {
        OnlineListFragment fragment = new OnlineListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public OnlineListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        //VIEWS
        adapter = new ArrayAdapter<String>(getActivity().getBaseContext(), android.R.layout.simple_list_item_1);
        adapterCover = new ListCoverAdapter(getActivity().getBaseContext(), R.layout.list_cover_adapter);
        lvMain = (ListView)getView().findViewById(R.id.lvMain);
        loading = (RelativeLayout)getView().findViewById(R.id.loadingPanel);

        //PREFERENCES
        settings = getActivity().getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        siteid = settings.getString(pref_site, "mangafox.me");


        //ImageLoader
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity()).build();

        ImageLoader.getInstance().init(config);

        //
        if(OnlineListFragment.this.isVisible())
            setList();

    }

    public void setList(){
        setListFromAPI();
    }

    public void setListFromAPI(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        bCover = preferences.getBoolean("listWithCover", false);


        int visibleTreshold = 50;


        RestAdapter ra = new RestAdapter.Builder()
                .setEndpoint(endPoint)
                .build();
        mangaapi m = ra.create(mangaapi.class);

        m.getSearch(siteid, 1, "", 0, 0, "", new Callback<List<searchmodel>>() {
            @Override
            public void success(List<searchmodel> searchmodels, Response response) {
                listSearch.clear();
                listSearch.addAll(searchmodels);
                if (bCover) {
                    for (int i = 0; i < 10; i++)
                        adapterCover.add(listSearch.get(i));
                    adapterCover.setSize(listSearch.size());
                    lvMain.setAdapter(adapterCover);
                } else {
                    String[] arr = new String[50];
                    for (int i = 0; i < 50; i++)
                        arr[i] = listSearch.get(i).getName();

                    adapter.clear();
                    adapter.addAll(arr);
                    lvMain.setAdapter(adapter);
                    lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent i = new Intent(getActivity(), ShowOnlineActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.putExtra("mangaId", listSearch.get(position).getMangaId());
                            startActivity(i);
                        }
                        });
                    if(getActivity()!=null)
                        if(getActivity().getBaseContext()!=null)
                            Toast.makeText(getActivity().getBaseContext(), "List loaded", Toast.LENGTH_SHORT).show();
                }
                loading.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("it f*cking failed", error.getLocalizedMessage());
            }
        });

        if(bCover)
            visibleTreshold = 10;


        lvMain.setOnScrollListener(new EndlessScrollListener(visibleTreshold) {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                if (bCover) {
                    if (count * 10 + 10 > listSearch.size()) {
                        for (int i = count * 10; i < listSearch.size(); i++)
                            adapterCover.add(listSearch.get(i));
                    }
                    else{
                        for (int i = count * 10; i < count * 10 + 10; i++)
                            adapterCover.add(listSearch.get(i));
                    }
                    adapterCover.notifyDataSetChanged();
                } else {
                    String[] arr = new String[50];
                    if (count * 50 + 50 > listSearch.size()) {
                        for (int i = count * 50; i < listSearch.size(); i++)
                            arr[i - (count * 50)] = listSearch.get(i).getName();
                    } else {
                        for (int i = count * 50; i < count * 50 + 50; i++)
                            arr[i - (count * 50)] = listSearch.get(i).getName();
                    }
                    adapter.addAll(arr);
                    adapter.notifyDataSetChanged();
                }
                count++;
                return false;
            }
        });
    }

    public void setListFromScraper(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_online_list, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
