package com.endless.hov.mangakeeper.CustomAdapters;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.endless.hov.mangakeeper.All;
import com.endless.hov.mangakeeper.Manga;
import com.endless.hov.mangakeeper.R;
import com.endless.hov.mangakeeper.ShowMangaActivity;

import java.io.File;
import java.util.List;


public class MangaListAdapter extends ArrayAdapter<Manga> {

    final String coverPath = Environment.getExternalStorageDirectory().toString()+"/MankaKeeper/";
    private int longClickPosition = 0;


    public MangaListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public MangaListAdapter(Context context, int resource, List<Manga> mangas) {
        super(context, resource, mangas);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.manga_shelf, null);
        }

        Manga m1 = null;
        if(super.getCount()>position*3)
            m1 = getItem(position*3);
        Manga m2= null;
        if(super.getCount()>(position*3+1)) {
            m2 = getItem(position * 3 + 1);
        }
        Manga m3= null;
        if(super.getCount()>(position*3+2)) {
            m3 = getItem(position * 3 + 2);
        }


        if (m1 != null) {


            ImageView iv1 = (ImageView) v.findViewById(R.id.ivList1);
            ImageView iv2 = (ImageView) v.findViewById(R.id.ivList2);
            ImageView iv3 = (ImageView) v.findViewById(R.id.ivList3);
            TextView tv1 = (TextView) v.findViewById(R.id.tvChapter1);
            TextView tv2 = (TextView) v.findViewById(R.id.tvChapter2);
            TextView tv3 = (TextView) v.findViewById(R.id.tvChapter3);

            tv1.setVisibility(View.GONE);
            tv2.setVisibility(View.GONE);
            tv3.setVisibility(View.GONE);

            if (iv1 != null&&m1!=null) {
                final Manga m = m1;
                File imgFile = new File(coverPath+m1.getId()+".jpg");
                if(imgFile.exists()){
                    Bitmap bmp = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    iv1.setImageBitmap(bmp);
                    iv1.setVisibility(View.VISIBLE);
                    iv1.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            longClickPosition = 0;
                            return false;
                        }
                    });
                    iv1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(getContext(), ShowMangaActivity.class);
                            i.putExtra("ID", m.getId());
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            getContext().startActivity(i);
                        }
                    });

                }

                if(m.getReadChapter() < m.getReleasedChapter()){

                    if(tv1 != null) {
                        tv1.setVisibility(View.VISIBLE);
                        tv1.setText("" + (m.getReleasedChapter() - m.getReadChapter()));
                    }
                }
            }

            if (iv2 != null&&m2!=null) {
                File imgFile = new File(coverPath+(m2.getId())+".jpg");
                final Manga m = m2;
                if(imgFile.exists()){
                    Bitmap bmp = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    iv2.setImageBitmap(bmp);
                    iv2.setVisibility(View.VISIBLE);
                    iv2.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            longClickPosition = 1;
                            return false;
                        }
                    });
                    iv2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(getContext(), ShowMangaActivity.class);
                            i.putExtra("ID", m.getId());
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            getContext().startActivity(i);
                        }
                    });
                }
                if(m.getReadChapter() < m.getReleasedChapter()){
                    if(tv2 != null) {
                        tv2.setVisibility(View.VISIBLE);
                        tv2.setText("" + (m.getReleasedChapter() - m.getReadChapter()));
                    }
                }
            }
            else
                iv2.setVisibility(View.GONE);

            if (iv3 != null&&m3!=null) {
                final Manga m = m3;
                File imgFile = new File(coverPath+(m3.getId())+".jpg");
                if(imgFile.exists()){
                    Bitmap bmp = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    iv3.setImageBitmap(bmp);
                    iv3.setVisibility(View.VISIBLE);
                    iv3.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            longClickPosition = 2;
                            return false;
                        }
                    });
                    iv3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(getContext(), ShowMangaActivity.class);
                            i.putExtra("ID", m.getId());
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            getContext().startActivity(i);
                        }
                    });
                }

                if(m.getReadChapter() < m.getReleasedChapter()){
                    if(tv3 != null) {
                        tv3.setVisibility(View.VISIBLE);
                        tv3.setText("" + (m.getReleasedChapter() - m.getReadChapter()));
                    }
                }
            }
            else
                iv3.setVisibility(View.GONE);
            return v;
        }
        else
            return null;

    }

    @Override
    public int getCount() {
        if(super.getCount()%3==0)
            return super.getCount()/3;
        else
            return super.getCount()/3+1;
    }

    public int getLonClickPostion(){
        return longClickPosition;
    }
}

