package com.endless.hov.mangakeeper.Scraper;


import org.apache.commons.lang3.text.WordUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Scraper {

    public static List<ScraperManga> getLatestReleasesMangas(int page) throws IOException {
        String url = "http://mangafox.me/releases/";
        if (page > 1) url += page + ".htm";
        Document doc = Jsoup.connect(url).get();

        Element updates = doc.getElementById("updates");
        Elements lis = updates.getElementsByTag("li");
        List<ScraperManga> list = new ArrayList<ScraperManga>();
        for (Element e : lis) {
            list.add(getManga(e.getElementsByAttributeValue("class", "title").first().getElementsByTag("a").first().attr("href")
                    .replace("http://mangafox.me/manga/", "").replace("/", "")));
        }

        return list;
    }

    public static String[][] getLatestReleasesStrings(int page) throws IOException {//0-id 1-title
        String url = "http://mangafox.me/releases/";
        if (page > 1) url += page + ".htm";
        Document doc = Jsoup.connect(url).get();

        Element updates = doc.getElementById("updates");
        Elements lis = updates.getElementsByTag("li");
        String[][] list = new String[lis.size()][2];
        for (int i = 0; i < lis.size(); i++) {
            list[i][0] = lis.get(i).getElementsByAttributeValue("class", "title").first().getElementsByTag("a").first().attr("href")
                    .replace("http://mangafox.me/manga/", "").replace("/", "");
            list[i][1] = lis.get(i).getElementsByAttributeValue("class", "title").first().getElementsByTag("a").first().text();
        }

        return list;
    }

    public static ScraperManga getManga(String scrapId) throws IOException {
        return MangareaderScraper.getManga(scrapId);
        /*
        Document doc = Jsoup.connect("http://mangafox.me/manga/" + scrapId + "/").get();
        Element page = doc.getElementById("page").getElementsByAttributeValue("class", "left").first();
        Element info = page.getElementById("series_info");
        Element title = page.getElementById("title");
        Element chapters = page.getElementById("chapters");

        //Cover
        String cover = info.getElementsByAttributeValue("class", "cover").get(0).getElementsByTag("img").attr("src");
        //Name
        String name;
        try {
            name = title.getElementsByTag("h2").first().getElementsByTag("a").first().text();
            name = name.replace(" Manga", "");
            name = name.replace(" Manhua", "");
            name = name.replace(" Manhwa", "");
        } catch (NullPointerException e) {
            name = title.getElementsByTag("h1").first().text();
            name = name.replace(" Manga", "");
            name = name.replace(" Manhua", "");
            name = name.replace(" Manhwa", "");
            name = name.toLowerCase();
            name = WordUtils.capitalize(name);
        }

        //Status
        String status = "Ongoing";
        if (info.getElementsByAttributeValue("class", "data").get(0).getElementsByTag("span").text().contains("Completed"))
            status = "Complete";

        //Released Year
        Element tr = title.getElementsByTag("tr").get(1);
        String year = tr.getElementsByTag("td").get(0).getElementsByTag("a").first().text();
        //Authors
        List<String> authors = new ArrayList<String>();
        Elements auElements = tr.getElementsByTag("td").get(1).getElementsByTag("a");
        for (Element e : auElements)
            authors.add(e.text());
        //Artists
        List<String> artists = new ArrayList<String>();
        Elements arElements = tr.getElementsByTag("td").get(2).getElementsByTag("a");
        for (Element e : arElements)
            artists.add(e.text());
        //Genres
        List<String> genres = new ArrayList<String>();
        Elements gElements = tr.getElementsByTag("td").get(3).getElementsByTag("a");
        for (Element e : gElements)
            genres.add(e.text());

        //Details
        String details = null;
        try {
            details = title.getElementsByTag("p").get(0).text();
        } catch (Exception e) {
            details = "";
        }

        //Chapters
        List<ScraperChapter> chapterList = getChaptersFromElement(chapters, name);

        ScraperManga manga = new ScraperManga(scrapId, name, cover, year, details, status, authors, artists, genres, chapterList);

        return manga;
        */
    }

    public static List<ScraperChapter> getChaptersFromElement(Element chaptersElement, String mangaTitle) {
        List<ScraperChapter> list = new ArrayList<ScraperChapter>();

        Elements chlists = chaptersElement.getElementsByAttributeValue("class", "chlist");
        for (int i = 0; i < chlists.size(); i++) {
            Element e = chlists.get(i);
            Element v = chaptersElement.getElementsByAttributeValue("class", "slide").get(i);
            String volume = v.getElementsByAttributeValue("class", "volume").get(0).text();
            String span = v.getElementsByAttributeValue("class", "volume").get(0).getElementsByTag("span").first().text();
            volume = volume.replace(" " + span, "");
            ScraperVolume sv = new ScraperVolume(volume, span);
            Elements lis = e.getElementsByTag("li");
            for (Element l : lis) {
                String releaseDate = l.getElementsByAttributeValue("class", "date").first().text();
                String title = null;
                try {
                    title = l.getElementsByAttributeValue("class", "title nowrap").first().text();
                } catch (NullPointerException e1) {
                    title = "";
                }
                String chapterId = l.getElementsByAttributeValue("class", "tips").first().text();
                chapterId = chapterId.substring(mangaTitle.length() + 1, chapterId.length());
                ScraperChapter c = new ScraperChapter(title, sv, chapterId, releaseDate);
                list.add(c);
            }
        }

        return list;
    }

    //public static  List<ScraperChapter> getChapterWithId(String mangaId){}

    public static List<String> getChapterPages(ScraperManga manga, ScraperChapter chapter) throws IOException {
        return getChapterPagesWithStrings(manga.getScrapId(), chapter.getVolume().getTitle(), chapter.getChapterId());
    }

    public static List<String> getChapterPagesWithStrings(String mangaId, String volume, String chapter) throws IOException {
        List<String> list = new ArrayList<String>();

        String url = "http://mangafox.me/manga/" + mangaId + "/"; //fuuka/c087/1.html
        if (!volume.equals("Volume Not Available"))
            url += "v" + volume.replace("Volume ", "") + "/";
        url += "c" + chapter + "/";
        int limit = 1;
        for (int i = 1; i <= limit; i++) {
            //System.out.println(url+);
            Document doc = Jsoup.connect(url + i + ".html").get();
            String img = doc.getElementById("body").getElementById("viewer").getElementsByAttributeValue("class", "read_img")
                    .first()
                    .getElementsByTag("img")
                    .first()
                    .attr("src");
            list.add(img);
            if (i == 1) {
                String all = doc.getElementById("top_bar").getElementsByAttributeValue("class", "r m").first()
                        .getElementsByAttributeValue("class", "l").text();
                limit = Integer.parseInt(all.substring(all.indexOf("of") + 3, all.length()));
            }
        }

        return list;
    }

    public static String[] getFirstPageAndLimit(String mangaid, String volumeid, String chapterid) throws IOException {
        String url = "http://mangafox.me/manga/" + mangaid + "/"; //fuuka/c087/1.html
        if (!volumeid.equals("Volume Not Available"))
            url += "v" + volumeid.replace("Volume ", "") + "/";
        url += "c" + chapterid + "/";
        String temp = new String(url);
        Document doc = Jsoup.connect(url + 1 + ".html").get();
        String img = doc.getElementById("body").getElementById("viewer").getElementsByAttributeValue("class", "read_img")
                .first()
                .getElementsByTag("img")
                .first()
                .attr("src");

        String all = doc.getElementById("top_bar").getElementsByAttributeValue("class", "r m").first()
                .getElementsByAttributeValue("class", "l").text();
        String limit = all.substring(all.indexOf("of") + 3, all.length());

        return new String[]{img, limit, temp};
    }

    public static String getChapterPage(ScraperManga manga, ScraperChapter chapter, int page) throws IOException {
        return getChapterPageWithStrings(manga.getScrapId(), chapter.getVolume().getTitle(), chapter.getChapterId(), page);
    }

    public static String getChapterPageWithStrings(String mangaId, String volume, String chapter, int page) throws IOException {
        String url = "http://mangafox.me/manga/" + mangaId + "/"; //fuuka/c087/1.html
        if (!volume.equals("Volume Not Available"))
            url += "v" + volume.replace("Volume ", "") + "/";
        url += "c" + chapter + "/";
        Document doc = Jsoup.connect(url + page + ".html").get();
        String img = doc.getElementById("body").getElementById("viewer").getElementsByAttributeValue("class", "read_img")
                .first()
                .getElementsByTag("img")
                .first()
                .attr("src");
        return img;
    }

    public static String getChapterPageWithUrl(String url, int page) throws IOException{
        Document doc = Jsoup.connect(url + page + ".html").get();
        String img = doc.getElementById("body").getElementById("viewer").getElementsByAttributeValue("class", "read_img")
                .first()
                .getElementsByTag("img")
                .first()
                .attr("src");
        return img;
    }


    public static String[][] getAllMangas() throws IOException {
        return MangareaderScraper.getAllMangas();

        /*
        String url = "http://mangafox.me/manga/";
        Document doc = Jsoup.connect(url).maxBodySize(0).timeout(0).get();

        Elements lists = doc.getElementsByAttributeValue("class", "manga_list");
        Elements mangas = new Elements();
        for (Element e : lists) {
            mangas.addAll(e.getElementsByTag("li"));
        }
        //0-id 1-title
        String[][] ss = new String[mangas.size()][2];
        for (int i = 0; i < ss.length; i++) {
            Element a = mangas.get(i).getElementsByTag("a").first();
            ss[i][0] = a.
                    attr("href").
                    replace("http://mangafox.me/manga/", "").
                    replace("/", "");
            ss[i][1] = a.text();
        }

        return ss;
        */
    }

    public static String[][] getSearch(String title, int type, String author, String artist, int[] genres, int releaseType, String released, int ratingType, int rating, int isCompleted, int page) throws IOException {
        String url = "http://mangafox.me/search.php?name_method=cw&name=" + title +
                "&type=" + new String[]{"", "1", "2", "3"}[type] +
                "&author_method=cw&author=" + author +
                "&artist_method=cw&artist=" + artist +
                "&genres[Action]=" + genres[0] +
                "&genres[Adult]=" + genres[1] +
                "&genres[Adventure]=" + genres[2] +
                "&genres[Comedy]=" + genres[3] +
                "&genres[Doujinshi]=" + genres[4] +
                "&genres[Drama]=" + genres[5] +
                "&genres[Ecchi]=" + genres[6] +
                "&genres[Fantasy]=" + genres[7] +
                "&genres[Gender%20Bender]=" + genres[8] +
                "&genres[Harem]=" + genres[9] +
                "&genres[Historical]=" + genres[10] +
                "&genres[Horror]=" + genres[11] +
                "&genres[Josei]=" + genres[12] +
                "&genres[Martial%20Arts]=" + genres[13] +
                "&genres[Mature]=" + genres[14] +
                "&genres[Mecha]=" + genres[15] +
                "&genres[Mystery]=" + genres[16] +
                "&genres[One%20Shot]=" + genres[17] +
                "&genres[Psychological]=" + genres[18] +
                "&genres[Romance]=" + genres[19] +
                "&genres[School%20Life]=" + genres[20] +
                "&genres[Sci-fi]=" + genres[21] +
                "&genres[Seinen]=" + genres[22] +
                "&genres[Shoujo]=" + genres[23] +
                "&genres[Shoujo%20Ai]=" + genres[24] +
                "&genres[Shounen]=" + genres[25] +
                "&genres[Shounen%20Ai]=" + genres[26] +
                "&genres[Slice%20of%20Life]=" + genres[27] +
                "&genres[Smut]=" + genres[28] +
                "&genres[Sports]=" + genres[29] +
                "&genres[Supernatural]=" + genres[30] +
                "&genres[Tragedy]=" + genres[31] +
                "&genres[Webtoons]=" + genres[32] +
                "&genres[Yaoi]=" + genres[33] +
                "&genres[Yuri]=" + genres[34] +
                "&released_method=" + new String[]{"eq", "lt", "gt"}[releaseType] +
                "&released=" + released +
                "&rating_method=" + new String[]{"eq", "lt", "gt"}[ratingType] +
                "&rating=" + new String[]{"", "0", "1", "2", "3", "4", "5"}[rating] +
                "&is_completed=" + new String[]{"1", "0", ""}[isCompleted] +
                "&advopts=1";//&page=2
        if (page != 1)
            url += "&page=" + page;

        Document doc = Jsoup.connect(url).get();
        Element p = doc.getElementById("page");
        Element c = p.getElementsByAttributeValue("class", "left").first();
        Element listing = c.getElementById("listing");
        Elements trs = listing.getElementsByTag("tr");
        trs.remove(0);
        String[][] list = new String[trs.size()][6];//0-id 1-title 2-Views 3-chapter 4-last update 5-pageLimit
        for (int i = 0; i < trs.size(); i++) {
            Elements tds = trs.get(i).getElementsByTag("td");
            list[i][0] = tds.first().getElementsByTag("a").first().attr("href").replace("http://mangafox.me/manga/", "").replace("/", "");
            list[i][1] = tds.first().getElementsByTag("a").first().text();
            list[i][2] = tds.get(2).text();
            list[i][3] = tds.get(3).text();
            list[i][4] = tds.get(4).text().split(" on ")[1];
            if (page == 1) {
                Elements es = c.getElementById("nav").getElementsByTag("li");
                list[i][5] = es.get(es.size() - 2).text();
            }
        }

        return list;
    }
}
