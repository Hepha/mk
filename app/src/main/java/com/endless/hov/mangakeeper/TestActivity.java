package com.endless.hov.mangakeeper;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.endless.hov.mangakeeper.API.mangaapi;
import com.endless.hov.mangakeeper.CustomAdapters.MangaListAdapter;
import com.endless.hov.mangakeeper.Scraper.MangareaderScraper;
import com.endless.hov.mangakeeper.model.Chapter;
import com.endless.hov.mangakeeper.model.mangamodel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class TestActivity extends ActionBarActivity {


   Button btnTest;
    MangaDatabaseHandler db;

    final String endPoint = "https://doodle-manga-scraper.p.mashape.com";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        btnTest = (Button)findViewById(R.id.btnTest);

        btnTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ShowOnlineActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("mangaId", "naruto");
                startActivity(i);

            }
        });

/*
        db = new MangaDatabaseHandler(this);
        tags = new ArrayList<>();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, tags);

        etTag = (EditText)findViewById(R.id.etTag);
        btnAddTag = (Button)findViewById(R.id.btnAddTag);
        lvTags = (ListView)findViewById(R.id.lvTags);
        lvManga = (ListView)findViewById(R.id.lvManga);
        lvChapters = (ListView)findViewById(R.id.lvChapters);
        lvTags.setAdapter(adapter);

        btnAddTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = etTag.getText().toString();
                if (!s.equals("")) {
                    if (!tags.contains(s)) {
                        db.addTag(s);
                        loadTags();
                    } else
                        Toast.makeText(getBaseContext(), "Tag already exists!", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getBaseContext(), "Tag can't be empty", Toast.LENGTH_SHORT).show();
            }
        });

        lvTags.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                db.deleteTag(tags.get(position).);
                loadTags();
                return false;
            }
        });
    }

    public void loadTags(){
        tags = db.getTags();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, tags);
        lvTags.setAdapter(adapter);
        if(tags.size()==0)
            Toast.makeText(this, "No tags in db", Toast.LENGTH_SHORT).show();
    }

    public void loadMangas(){
        List<Manga> list = db.getAllMangas();
        lvManga.setAdapter(new MangaListAdapter(this, R.layout.manga_shelf, list));
    }


    public void getChapters(String apiID){
        RestAdapter ra = new RestAdapter.Builder()
                .setEndpoint(endPoint)
                .build();
        mangaapi m = ra.create(mangaapi.class);

        m.getManga("mangareader.net", apiID, new Callback<mangamodel>() {
            @Override
            public void success(mangamodel mangamodel, Response response) {
                chapters = mangamodel.getChapters();
                Collections.reverse(chapters);
                String[] arr = new String[mangamodel.getChapters().size()];
                for (int i=0;i<arr.length;i++){
                    arr[i] =  mangamodel.getChapters().get(i).getChapterId() + "-" + mangamodel.getChapters().get(i).getName();
                }
                lvChapters.setAdapter(new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, arr));
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });


        lvChapters.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getBaseContext(), ReadChapterActivity.class);
                i.putExtra("mangaid", "bleach");
                i.putExtra("chapterid", String.valueOf(chapters.get(position).getChapterId()));
                startActivity(i);
            }
        });*/
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
