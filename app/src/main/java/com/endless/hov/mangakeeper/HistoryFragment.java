package com.endless.hov.mangakeeper;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.endless.hov.mangakeeper.CustomAdapters.HistoryAdapter;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HistoryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HistoryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    ListView lvHistory;

    RelativeLayout loading;

    MangaDatabaseHandler db;

    List<Manga> mangas;

    List<HistoryEntry> list;

    String arr[];



    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HistoryFragment newInstance(String param1, String param2) {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onStart() {
        super.onStart();


        loading = (RelativeLayout)getView().findViewById(R.id.loadingPanel);


        //startProgress();
        setList();

    }

    @Override
    public void onPause() {
        super.onPause();
        db.close();
    }


    public void startProgress() {
        // do something long
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                db = new MangaDatabaseHandler(getActivity().getBaseContext());

                lvHistory = (ListView)getView().findViewById(R.id.lvHistory);

                mangas = db.getAllMangas();
                ArrayList<String> list = new ArrayList<>();
                for(Manga m:mangas)
                    list.addAll(m.historyOuput());



                int c,d, n = list.size();
                String swap;

                for (c = 0; c < ( n - 1 ); c++) {
                    for (d = 0; d < n - c - 1; d++) {
                        long time1 = Long.parseLong(list.get(d).split("==")[1]);
                        long time2 = Long.parseLong(list.get(d+1).split("==")[1]);
                        if (time1< time2) /* For descending order use < */
                        {
                            swap = list.get(d);
                            list.set(d, list.get(d+1));
                            list.set(d+1, swap);
                        }
                    }
                }

                arr = new String[list.size()];
                for(int i = 0; i<list.size(); i++)
                    arr[i] = list.get(i);


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loading.setVisibility(View.GONE);
                        setList();
                    }
                });
            }
        };
        new Thread(runnable).start();
    }

    public void setList(){
        //lvHistory.setAdapter(new HistoryAdapter(getActivity().getBaseContext(), R.layout.history_adapter, arr));

        db = new MangaDatabaseHandler(getActivity().getBaseContext());
        list = db.getAllHistory();

        lvHistory = (ListView)getView().findViewById(R.id.lvHistory);
        lvHistory.setAdapter(new HistoryAdapter(getActivity().getBaseContext(), R.layout.history_adapter, list));
        registerForContextMenu(lvHistory);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()==R.id.lvHistory) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            String title = list.get(info.position).getMangaTitle();
            if(title.length()>30)
                title = title.substring(0,28) + "...";
           menu.add("Go To Manga: " + title);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();

        /*
        String title = arr[info.position].split("==")[0];

        if(title!=null&&title!="") {
            Manga m = mangas.get(1);
            for (int i = 0; i<mangas.size(); i++){
                if(mangas.get(i).getTitle().equals(title)){
                    m = mangas.get(i);
                    break;
                }
            }

            Intent i = new Intent(getActivity(), ShowMangaActivity.class);
            i.putExtra("ID", m.getId());
            startActivity(i);
        }
        */
        Intent i = new Intent(getActivity(), ShowMangaActivity.class);
        i.putExtra("ID", list.get(info.position).getMangaId());
        startActivity(i);


        return true;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public class ThreadedRequest
    {
        private Handler mHandler;
        private Runnable pRunnable;

        public ThreadedRequest()
        {

            mHandler = new Handler();
        }

        public void start(Runnable newRun)
        {
            pRunnable = newRun;
            processRequest.start();
        }

        private Thread processRequest = new Thread()
        {
            public void run()
            {
                //Do you request here...
                if (pRunnable == null || mHandler == null) return;
                mHandler.post(pRunnable);

                db = new MangaDatabaseHandler(getActivity().getBaseContext());

                lvHistory = (ListView)getView().findViewById(R.id.lvHistory);

                List<Manga> mangas = db.getAllMangas();
                ArrayList<String> list = new ArrayList<>();
                for(Manga m:mangas)
                    list.addAll(m.historyOuput());



                int c,d, n = list.size();
                String swap;

                for (c = 0; c < ( n - 1 ); c++) {
                    for (d = 0; d < n - c - 1; d++) {
                        long time1 = Long.parseLong(list.get(d).split("==")[1]);
                        long time2 = Long.parseLong(list.get(d+1).split("==")[1]);
                        if (time1< time2) /* For descending order use < */
                        {
                            swap = list.get(d);
                            list.set(d, list.get(d+1));
                            list.set(d+1, swap);
                        }
                    }
                }

                arr = new String[list.size()];
                for(int i = 0; i<list.size(); i++)
                    arr[i] = list.get(i);


            }
        };
    }

}
