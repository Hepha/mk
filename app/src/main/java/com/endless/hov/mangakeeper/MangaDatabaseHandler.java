package com.endless.hov.mangakeeper;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Barış on 26.6.2015.
 */
public class MangaDatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERION = 9;//newest version
    private static final String DATABASE_NAME = "mangaManager";
    private static final String TABLE_MANGAS = "mangas";
    private static final String TABLE_TAGS = "tagsTable";
    private static final String TABLE_MANGA_TAGS = "mangaTagsTable";
    private static final String TABLE_HISTORY = "historyTable";

    //Manga Table
    private static final String KEY_ID = "id";
    private static final String KEY_API_ID = "apiId";
    private static final String KEY_TITLE = "title";
    private static final String KEY_AUTHOR = "author";
    private static final String KEY_DETAIL = "detail";
    private static final String KEY_NOTES = "notes";
    private static final String KEY_RELEASED_CHAPTER = "releasedChapter";
    private static final String KEY_READ_CHAPTER = "readChapter";
    private static final String KEY_RELEASED_VOLUME = "releasedVolume";
    private static final String KEY_READ_VOLUME = "readVolume";
    private static final String KEY_STATUS = "status";
    private static final String KEY_GENRES = "genres";
    private static final String KEY_UPDATE = "updateTime";
    private static final String KEY_ADD = "addTime";
    private static final String KEY_FAVORITE = "favorite";
    private static final String KEY_HISTORY = "history";

    //Tags Table
    private static final String KEY_TAG_ID = "tagId";
    private static final String KEY_TAG_TEXT = "tagText";

    //MangaTags Table
    private static final String KEY_TAGS_MANGA_ID = "tagsMangaId";
    private static final String KEY_TAGS_TAG_ID = "tagsTagId";

    //History Table
    private static final String KEY_HISTORY_MANGA_ID = "historyMangaId";
    private static final String KEY_HISTORY_TYPE = "historyType";
    private static final String KEY_HISTORY_TIME = "historyTime";
    private static final String KEY_HISTORY_DATA = "historyData";




    final String coverPath = Environment.getExternalStorageDirectory().toString()+"/MankaKeeper/";

    SharedPreferences settings;



    public MangaDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_MANGA_TABLE = "CREATE TABLE " + TABLE_MANGAS + "(" +
                KEY_ID + " INTEGER PRIMARY KEY, " + KEY_TITLE + " TEXT, " + KEY_AUTHOR + " TEXT, " +
                KEY_DETAIL + " TEXT, " + KEY_NOTES + " TEXT, " + KEY_RELEASED_CHAPTER + " INTEGER, " +
                KEY_READ_CHAPTER + " INTEGER, " + KEY_RELEASED_VOLUME + " INTEGER, " + KEY_READ_VOLUME + " INTEGER, " +
                KEY_STATUS + " INTEGER, " + KEY_GENRES + " TEXT," + KEY_UPDATE + " INTEGER," + KEY_ADD + " INTEGER," +
                KEY_FAVORITE + " INTEGER," + KEY_HISTORY + " TEXT, " + KEY_API_ID + " TEXT" +")";
        db.execSQL(CREATE_MANGA_TABLE);

        String CREATE_TAG_TABLE = "CREATE TABLE " + TABLE_TAGS + "(" + KEY_TAG_ID + " INTEGER PRIMARY KEY, "+ KEY_TAG_TEXT + " TEXT " + ")";
        db.execSQL(CREATE_TAG_TABLE);

        String CREATE_TAGS_TABLE = "CREATE TABLE " + TABLE_MANGA_TAGS + "(" + KEY_TAGS_MANGA_ID + " INTEGER, "+ KEY_TAGS_TAG_ID + " INTEGER " + ")";
        db.execSQL(CREATE_TAGS_TABLE);

        String CREATE_HISTORY_TABLE = "CREATE TABLE " + TABLE_HISTORY + "(" + KEY_HISTORY_MANGA_ID + " INTEGER, "+ KEY_HISTORY_TYPE + " INTEGER, " + KEY_HISTORY_TIME + " INTEGER, " +
                KEY_HISTORY_DATA + " TEXT" + ")";
        db.execSQL(CREATE_HISTORY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /*
        if(oldVersion==8&&newVersion==9) {
            //Create the table
            String CREATE_HISTORY_TABLE = "CREATE TABLE " + TABLE_HISTORY + "(" + KEY_HISTORY_MANGA_ID + " INTEGER, "+ KEY_HISTORY_TYPE + " INTEGER, " + KEY_HISTORY_TIME + " INTEGER, " +
                    KEY_HISTORY_DATA + " TEXT" + ")";
            db.execSQL(CREATE_HISTORY_TABLE);

            //Pass the old values from manga table to new table
            List<Manga> list = new ArrayList<>();
            String selectQuery =  "SELECT * FROM " + TABLE_MANGAS;

            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()){
                do{
                    Manga manga = new Manga(Integer.parseInt(cursor.getString(0)),
                            cursor.getString(15),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            Integer.parseInt(cursor.getString(5)),
                            Integer.parseInt(cursor.getString(6)),
                            Integer.parseInt(cursor.getString(7)),
                            Integer.parseInt(cursor.getString(8)),
                            Integer.parseInt(cursor.getString(9)),
                            cursor.getString(10),
                            cursor.getLong(11),
                            cursor.getLong(12),
                            cursor.getInt(13),
                            cursor.getString(14));
                    list.add(manga);
                }while(cursor.moveToNext());
            }

            if(cursor!=null && !cursor.isClosed())
                cursor.close();


            for(Manga m : list){
                for(String h : m.getHistory()){
                    String[] arr = h.split("==");
                    HistoryEntry entry = new HistoryEntry(m.getId(), "", Integer.parseInt(arr[0]), Long.parseLong(arr[1]), "");
                    String data = "";
                    for(int i = 2;i<arr.length;i++){
                        data += arr[i];
                        if(i != arr.length-1)
                            data += "==";
                    }

                    entry.setData(data);


                    ContentValues values = new ContentValues();

                    values.put(KEY_HISTORY_MANGA_ID, entry.getMangaId());
                    values.put(KEY_HISTORY_TYPE, entry.getType());
                    values.put(KEY_HISTORY_TIME, entry.getTime());
                    values.put(KEY_HISTORY_DATA, entry.getData());

                    db.insert(TABLE_HISTORY, null, values);
                }
            }



        }
        */
    }

    //region MANGA TABLE

    public void addManga(Manga manga){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();


        values.put(KEY_TITLE, manga.getTitle());
        values.put(KEY_AUTHOR, manga.getAuthor());
        values.put(KEY_DETAIL, manga.getDetail());
        values.put(KEY_NOTES, manga.getNotesAsString());
        values.put(KEY_RELEASED_CHAPTER, manga.getReleasedChapter());
        values.put(KEY_READ_CHAPTER, manga.getReadChapter());
        values.put(KEY_RELEASED_VOLUME, manga.getReleasedVolume());
        values.put(KEY_READ_VOLUME, manga.getReadVolume());
        values.put(KEY_STATUS, manga.getStatus());
        values.put(KEY_GENRES, manga.getGenresString());
        values.put(KEY_UPDATE, manga.getUpdateTime());
        values.put(KEY_ADD, manga.getAddTime());
        values.put(KEY_FAVORITE, manga.getFavorite());
        values.put(KEY_HISTORY, manga.getHistoryAsString());
        values.put(KEY_API_ID, manga.getApiID());

        db.insert(TABLE_MANGAS, null, values);
        Log.d("Manga added", manga.getTitle());
        db.close();
    }

    public Manga getManga(int id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_MANGAS, new String[]{KEY_ID, KEY_TITLE, KEY_AUTHOR, KEY_DETAIL, KEY_NOTES, KEY_RELEASED_CHAPTER, KEY_READ_CHAPTER,
        KEY_RELEASED_VOLUME, KEY_READ_VOLUME, KEY_STATUS, KEY_GENRES, KEY_UPDATE, KEY_ADD,KEY_FAVORITE, KEY_HISTORY, KEY_API_ID}, KEY_ID + "=?",  new String[]{String.valueOf(id)},null,null,null,null);

        if(cursor!=null) cursor.moveToFirst();

        Manga manga = new Manga(Integer.parseInt(cursor.getString(0)),
                cursor.getString(15),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                Integer.parseInt(cursor.getString(5)),
                Integer.parseInt(cursor.getString(6)),
                Integer.parseInt(cursor.getString(7)),
                Integer.parseInt(cursor.getString(8)),
                Integer.parseInt(cursor.getString(9)),
                cursor.getString(10),
                cursor.getLong(11),
                cursor.getLong(12),
                cursor.getInt(13),
                cursor.getString(14));

        if(cursor!=null && !cursor.isClosed())
            cursor.close();
        db.close();

        return manga;
    }

    public int exitsMangaID(String s){
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<Manga> list = new ArrayList<Manga>();

        Cursor cursor = db.query(TABLE_MANGAS, new String[]{KEY_ID, KEY_TITLE, KEY_AUTHOR, KEY_DETAIL, KEY_NOTES, KEY_RELEASED_CHAPTER, KEY_READ_CHAPTER,
                KEY_RELEASED_VOLUME, KEY_READ_VOLUME, KEY_STATUS, KEY_GENRES, KEY_UPDATE, KEY_ADD,KEY_FAVORITE, KEY_HISTORY, KEY_API_ID}, KEY_API_ID + "=?",  new String[]{s},null,null,null,null);

        if(cursor.moveToFirst()){
            do{
                Manga manga = new Manga(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(15),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        Integer.parseInt(cursor.getString(5)),
                        Integer.parseInt(cursor.getString(6)),
                        Integer.parseInt(cursor.getString(7)),
                        Integer.parseInt(cursor.getString(8)),
                        Integer.parseInt(cursor.getString(9)),
                        cursor.getString(10),
                        cursor.getLong(11),
                        cursor.getLong(12),
                        cursor.getInt(13),
                        cursor.getString(14));

                list.add(manga);
            }while (cursor.moveToNext());
        }

        if(cursor!=null && !cursor.isClosed())
            cursor.close();

        if(list.size()==0)
            return -1;
        else
            return list.get(0).getId();
    }

    public List<Manga> getMangasWithGenre(String s){
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<Manga> list = new ArrayList<Manga>();

        Cursor cursor = db.query(TABLE_MANGAS, new String[]{KEY_ID, KEY_TITLE, KEY_AUTHOR, KEY_DETAIL, KEY_NOTES, KEY_RELEASED_CHAPTER, KEY_READ_CHAPTER,
                KEY_RELEASED_VOLUME, KEY_READ_VOLUME, KEY_STATUS, KEY_GENRES, KEY_UPDATE, KEY_ADD,KEY_FAVORITE, KEY_HISTORY, KEY_API_ID}, KEY_GENRES + "=?",  new String[]{s},null,null,null,null);

        if(cursor.moveToFirst()){
            do{
                Manga manga = new Manga(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(15),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        Integer.parseInt(cursor.getString(5)),
                        Integer.parseInt(cursor.getString(6)),
                        Integer.parseInt(cursor.getString(7)),
                        Integer.parseInt(cursor.getString(8)),
                        Integer.parseInt(cursor.getString(9)),
                        cursor.getString(10),
                        cursor.getLong(11),
                        cursor.getLong(12),
                        cursor.getInt(13),
                        cursor.getString(14));

                list.add(manga);
            }while (cursor.moveToNext());
        }

        if(cursor!=null && !cursor.isClosed())
            cursor.close();

        return list;
    }


    public List<Manga> getFavorites(){
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<Manga> list = new ArrayList<Manga>();

        Cursor cursor = db.query(TABLE_MANGAS, new String[]{KEY_ID, KEY_TITLE, KEY_AUTHOR, KEY_DETAIL, KEY_NOTES, KEY_RELEASED_CHAPTER, KEY_READ_CHAPTER,
                KEY_RELEASED_VOLUME, KEY_READ_VOLUME, KEY_STATUS, KEY_GENRES, KEY_UPDATE, KEY_ADD,KEY_FAVORITE, KEY_HISTORY, KEY_API_ID}, KEY_FAVORITE + "=?",  new String[]{"1"},null,null,null,null);

        if(cursor.moveToFirst()){
            do{
                Manga manga = new Manga(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(15),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        Integer.parseInt(cursor.getString(5)),
                        Integer.parseInt(cursor.getString(6)),
                        Integer.parseInt(cursor.getString(7)),
                        Integer.parseInt(cursor.getString(8)),
                        Integer.parseInt(cursor.getString(9)),
                        cursor.getString(10),
                        cursor.getLong(11),
                        cursor.getLong(12),
                        cursor.getInt(13),
                        cursor.getString(14));
                list.add(manga);
            }while (cursor.moveToNext());
        }

        if(cursor!=null && !cursor.isClosed())
            cursor.close();

        db.close();

        return list;
    }

    public List<Manga> getWithStatus(int status){

        if(status!=5) {
            SQLiteDatabase db = this.getReadableDatabase();

            List<Manga> list = new ArrayList<Manga>();

            Cursor cursor = db.query(TABLE_MANGAS, new String[]{KEY_ID, KEY_TITLE, KEY_AUTHOR, KEY_DETAIL, KEY_NOTES, KEY_RELEASED_CHAPTER, KEY_READ_CHAPTER,
                    KEY_RELEASED_VOLUME, KEY_READ_VOLUME, KEY_STATUS, KEY_GENRES, KEY_UPDATE, KEY_ADD, KEY_FAVORITE, KEY_HISTORY, KEY_API_ID}, KEY_STATUS + "=?", new String[]{String.valueOf(status)}, null, null, null, null);

            if (cursor.moveToFirst()) {
                do {
                    Manga manga = new Manga(Integer.parseInt(cursor.getString(0)),
                            cursor.getString(15),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            Integer.parseInt(cursor.getString(5)),
                            Integer.parseInt(cursor.getString(6)),
                            Integer.parseInt(cursor.getString(7)),
                            Integer.parseInt(cursor.getString(8)),
                            Integer.parseInt(cursor.getString(9)),
                            cursor.getString(10),
                            cursor.getLong(11),
                            cursor.getLong(12),
                            cursor.getInt(13),
                            cursor.getString(14));

                    list.add(manga);
                } while (cursor.moveToNext());
            }

            if (cursor != null && !cursor.isClosed())
                cursor.close();

            db.close();

            return list;
        }
        else{
            List<Manga> list = getAllMangas();
            List<Manga> temp = new ArrayList<>();
            for(int i = 0; i<list.size();i++){
                if(list.get(i).getReadChapter()<list.get(i).getReleasedChapter())
                    temp.add(list.get(i));
            }
            return temp;
        }

    }

    public List<Manga> getWithStatusFavorites(int status) {
        if(status!=5) {
            SQLiteDatabase db = this.getReadableDatabase();

            ArrayList<Manga> list = new ArrayList<Manga>();

            Cursor cursor = db.query(TABLE_MANGAS, new String[]{KEY_ID, KEY_TITLE, KEY_AUTHOR, KEY_DETAIL, KEY_NOTES, KEY_RELEASED_CHAPTER, KEY_READ_CHAPTER,
                    KEY_RELEASED_VOLUME, KEY_READ_VOLUME, KEY_STATUS, KEY_GENRES, KEY_UPDATE, KEY_ADD, KEY_FAVORITE, KEY_HISTORY, KEY_API_ID}, KEY_STATUS + "=?", new String[]{String.valueOf(status)}, null, null, null, null);

            if (cursor.moveToFirst()) {
                do {
                    Manga manga = new Manga(Integer.parseInt(cursor.getString(0)),
                            cursor.getString(15),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            Integer.parseInt(cursor.getString(5)),
                            Integer.parseInt(cursor.getString(6)),
                            Integer.parseInt(cursor.getString(7)),
                            Integer.parseInt(cursor.getString(8)),
                            Integer.parseInt(cursor.getString(9)),
                            cursor.getString(10),
                            cursor.getLong(11),
                            cursor.getLong(12),
                            cursor.getInt(13),
                            cursor.getString(14));

                    if (manga.getFavoriteBool())
                        list.add(manga);
                } while (cursor.moveToNext());
            }

            if (cursor != null && !cursor.isClosed())
                cursor.close();

            db.close();

            return list;
        }
        else{
            List<Manga> list = getAllMangas();
            List<Manga> temp = new ArrayList<>();
            for(int i = 0; i<list.size();i++){
                if(list.get(i).getReadChapter()<list.get(i).getReleasedChapter()&&list.get(i).getFavoriteBool())
                    temp.add(list.get(i));
            }
            return temp;
        }
    }

    public List<Manga> getAllMangas(){
        ArrayList<Manga> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery =  "SELECT * FROM " + TABLE_MANGAS;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()){
            do{
                Manga manga = new Manga(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(15),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        Integer.parseInt(cursor.getString(5)),
                        Integer.parseInt(cursor.getString(6)),
                        Integer.parseInt(cursor.getString(7)),
                        Integer.parseInt(cursor.getString(8)),
                        Integer.parseInt(cursor.getString(9)),
                        cursor.getString(10),
                        cursor.getLong(11),
                        cursor.getLong(12),
                        cursor.getInt(13),
                        cursor.getString(14));
                list.add(manga);
            }while(cursor.moveToNext());
        }

        if(cursor!=null && !cursor.isClosed())
            cursor.close();

        db.close();

        return list;
    }
    public int getMangasCount() {
        String countQuery = "SELECT  * FROM " + TABLE_MANGAS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        db.close();

        // return count
        return cursor.getCount();
    }

    public void updateManga(Manga m, Context appContext){


        Manga manga = m;

        //CHANGE HISTORY
        settings = PreferenceManager.getDefaultSharedPreferences(appContext);

        int gap = Integer.parseInt(settings.getString("statusChangeGap", "0"));

        if(gap!=0&&manga.getStatus()==0&&(manga.getReleasedChapter()-manga.getReadChapter())>=gap)
            manga.setStatus(2);

        Manga old = getManga(manga.getId());


        if(manga.getReleasedChapter()>old.getReleasedChapter()) {
            //manga.addHistoryReleased((manga.getReleasedChapter() - old.getReleasedChapter()), old.getReleasedChapter() + 1);
            String data = (manga.getReleasedChapter() - old.getReleasedChapter()) + "==" + (old.getReleasedChapter() + 1);
            HistoryEntry h = new HistoryEntry(m.getId(), "", 4, System.currentTimeMillis(), data);
            addHistory(h);
        }
        if(manga.getReleasedChapter()<old.getReleasedChapter()) {
            //manga.addHistoryUnreleased((old.getReleasedChapter() - manga.getReleasedChapter()), old.getReleasedChapter());
            String data = (old.getReleasedChapter() - manga.getReleasedChapter()) + "==" + old.getReleasedChapter();
            HistoryEntry h = new HistoryEntry(m.getId(), "", 5, System.currentTimeMillis(), data);
            addHistory(h);
        }
        if(manga.getReadChapter()>old.getReadChapter()) {
            //manga.addHistoryRead((manga.getReadChapter() - old.getReadChapter()), old.getReadChapter() + 1);
            String data = (manga.getReadChapter() - old.getReadChapter()) + "==" + (old.getReadChapter() + 1);
            HistoryEntry h = new HistoryEntry(m.getId(), "", 2, System.currentTimeMillis(), data);
            addHistory(h);
        }
        if(manga.getReadChapter()<old.getReadChapter()) {
            //manga.addHistoryUnread((old.getReadChapter() - manga.getReadChapter()), old.getReadChapter());
            String data = (old.getReadChapter() - manga.getReadChapter()) + "==" + old.getReadChapter();
            HistoryEntry h = new HistoryEntry(m.getId(), "", 3, System.currentTimeMillis(), data);
            addHistory(h);
        }
        if(!manga.getTitle().equals(old.getTitle())) {
            //manga.addHistoryTitleChange(old.getTitle(), manga.getTitle());
            String data = old.getTitle() + "==" + manga.getTitle();
            HistoryEntry h = new HistoryEntry(m.getId(), "", 6, System.currentTimeMillis(), data);
            addHistory(h);
        }
        if(!manga.getAuthor().equals(old.getAuthor())) {
            //manga.addHistoryAuthorChange(old.getAuthor(), manga.getAuthor());
            String data = old.getAuthor() + "==" + manga.getAuthor();
            HistoryEntry h = new HistoryEntry(m.getId(), "", 7, System.currentTimeMillis(), data);
            addHistory(h);
        }
        if(!manga.getDetail().equals(old.getDetail())) {
            //manga.addHistoryDetailChange();
            HistoryEntry h = new HistoryEntry(m.getId(), "", 8, System.currentTimeMillis(), "");
            addHistory(h);
        }
        if(manga.getStatus()!=old.getStatus()) {
            //manga.addHistoryStatusChange(old.getStatus(), manga.getStatus());
            String data = old.getStatus() + "==" + manga.getStatus();
            HistoryEntry h = new HistoryEntry(m.getId(), "", 10, System.currentTimeMillis(), data);
            addHistory(h);
        }




        List<String> oldG = old.getGenres();
        List<String> newG = manga.getGenres();

        for(int i= 0; i<oldG.size(); i++){
            if(!newG.contains(oldG.get(i))){
                //manga.addHistoryGenreRemove(oldG.get(i));
                String data = oldG.get(i);
                HistoryEntry h = new HistoryEntry(m.getId(), "", 12, System.currentTimeMillis(), data);
                addHistory(h);
            }
        }
        for(int i= 0; i<newG.size(); i++){
            if(!oldG.contains(newG.get(i))){
                //manga.addHistoryGenreAdd(newG.get(i));
                String data = newG.get(i);
                HistoryEntry h = new HistoryEntry(m.getId(), "", 11, System.currentTimeMillis(), data);
                addHistory(h);
            }
        }

        List<String> oldN = old.getNotes();
        List<String> newN = manga.getNotes();

        for(int i= 0; i<oldN.size(); i++){
            if(!newN.contains(oldN.get(i))){
                if(oldN.get(i).length()<15) {
                    //manga.addHistoryNoteRemove(oldN.get(i));
                    String data = oldN.get(i);
                    HistoryEntry h = new HistoryEntry(m.getId(), "", 14, System.currentTimeMillis(), data);
                    addHistory(h);
                }
                else {
                    //manga.addHistoryNoteRemove(oldN.get(i).substring(0, 12) + "...");
                    String data = oldN.get(i).substring(0, 12) + "...";
                    HistoryEntry h = new HistoryEntry(m.getId(), "", 14, System.currentTimeMillis(), data);
                    addHistory(h);
                }
            }
        }
        for(int i= 0; i<newN.size(); i++){
            if(!oldN.contains(newN.get(i))){
                if(newN.get(i).length()<15) {
                    //manga.addHistoryNoteAdd(newN.get(i));
                    String data = newN.get(i);
                    HistoryEntry h = new HistoryEntry(m.getId(), "", 13, System.currentTimeMillis(), data);
                    addHistory(h);
                }
                else {
                    //manga.addHistoryNoteAdd(newN.get(i).substring(0, 12) + "...");
                    String data = newN.get(i).substring(0, 12) + "...";
                    HistoryEntry h = new HistoryEntry(m.getId(), "", 13, System.currentTimeMillis(), data);
                    addHistory(h);
                }
            }
        }

        if(old.getFavorite()!=manga.getFavorite()){
            if(manga.getFavoriteBool()) {
                //manga.addHistoryFavorite();
                HistoryEntry h = new HistoryEntry(m.getId(), "", 15, System.currentTimeMillis(), "");
                addHistory(h);
            }
            else {
                //manga.addHistoryUnfavorite();
                HistoryEntry h = new HistoryEntry(m.getId(), "", 16, System.currentTimeMillis(), "");
                addHistory(h);
            }
        }

        //CHANGE HISTORY
        //OLD SYSTEM. REQUIRED AN UPDATE ON THE MANGA ITSELF
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();


        values.put(KEY_TITLE, manga.getTitle());
        values.put(KEY_AUTHOR, manga.getAuthor());
        values.put(KEY_DETAIL, manga.getDetail());
        values.put(KEY_NOTES, manga.getNotesAsString());
        values.put(KEY_RELEASED_CHAPTER, manga.getReleasedChapter());
        values.put(KEY_READ_CHAPTER, manga.getReadChapter());
        values.put(KEY_RELEASED_VOLUME, manga.getReleasedVolume());
        values.put(KEY_READ_VOLUME, manga.getReadVolume());
        values.put(KEY_STATUS, manga.getStatus());
        values.put(KEY_GENRES, manga.getGenresString());
        values.put(KEY_UPDATE, System.currentTimeMillis());
        values.put(KEY_ADD, manga.getAddTime());
        values.put(KEY_FAVORITE, manga.getFavorite());
        values.put(KEY_HISTORY, manga.getHistoryAsString());
        values.put(KEY_API_ID, manga.getApiID());

        db.update(TABLE_MANGAS, values, KEY_ID + "=?", new String[]{String.valueOf(manga.getId())});

        db.close();

    }

    public void deleteManga(Manga manga){
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_MANGAS, KEY_ID + "=?", new String[]{String.valueOf(manga.getId())});
        db.close();
    }

    public void deleteManga(int id){
        SQLiteDatabase db = this.getWritableDatabase();

        File file = new File(coverPath + id + ".jpg");
        boolean deleted = file.delete();
        Log.d("Deleted:", file.getAbsolutePath());


        db.delete(TABLE_MANGAS, KEY_ID + "=?", new String[]{String.valueOf(id)});
        db.close();
    }


    public int getHighestID(){
        int id = 0;

        SQLiteDatabase db = this.getReadableDatabase();

        final String QUERY = "SELECT MAX("+KEY_ID+") AS " + KEY_ID + " FROM " + TABLE_MANGAS;
        Cursor cursor = db.rawQuery(QUERY, null);
        try{
            if(cursor.getCount()>0){
                cursor.moveToFirst();
                id = cursor.getInt(0);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            cursor.close();
            db.close();
        }

        return id;
    }

    //endregion

    //Just the history tiles

    /*public List<String> getAllHistory(){   //lookls like it won't change anything and I need to get them with their titles
        List<String> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT "+ KEY_HISTORY + " FROM " + TABLE_MANGAS;
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            do{
                list.add(cursor.getString(0));
            }while (cursor.moveToNext());
        }

        return list;
    }*/

    //region HISTORY TABLE

    public void addHistory(HistoryEntry h){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_HISTORY_MANGA_ID, h.getMangaId());
        values.put(KEY_HISTORY_TYPE, h.getType());
        values.put(KEY_HISTORY_TIME, h.getTime());
        values.put(KEY_HISTORY_DATA, h.getData());

        db.insert(TABLE_HISTORY, null, values);
        db.close();
    }

    public List<HistoryEntry> getAllHistory(){
        List<HistoryEntry> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT " + KEY_TITLE + "," + KEY_HISTORY_MANGA_ID + "," + KEY_HISTORY_TYPE + "," + KEY_HISTORY_TIME + "," + KEY_HISTORY_DATA
                + " FROM " + TABLE_HISTORY + " INNER JOIN " + TABLE_MANGAS + " ON " + TABLE_HISTORY + "." + KEY_HISTORY_MANGA_ID + " = " + TABLE_MANGAS + "." + KEY_ID +
                " ORDER BY " + TABLE_HISTORY + "." + KEY_HISTORY_TIME + " DESC";

        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                HistoryEntry h = new HistoryEntry(
                        cursor.getInt(1),
                        cursor.getString(0),
                        cursor.getInt(2),
                        cursor.getLong(3),
                        cursor.getString(4)
                );

                list.add(h);
            }while (cursor.moveToNext());
        }

        if(cursor != null && !cursor.isClosed())
            cursor.close();

        db.close();

        return list;
    }

    public List<HistoryEntry> getMangaHistory(int mangaId, String title){
        List<HistoryEntry> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_HISTORY + " WHERE " + KEY_HISTORY_MANGA_ID + " = " +mangaId +
                " ORDER BY " + KEY_HISTORY_TIME + " DESC";

        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                HistoryEntry h = new HistoryEntry(
                        cursor.getInt(0),
                        title,
                        cursor.getInt(1),
                        cursor.getLong(2),
                        cursor.getString(3)
                );

                list.add(h);
            }while (cursor.moveToNext());
        }

        if(cursor != null && !cursor.isClosed())
            cursor.close();

        db.close();

        return list;
    }

    //endregion


    //region TAG TABLE

    public void addTag(String s){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_TAG_TEXT, s);

        db.insert(TABLE_TAGS, null, values);
        db.close();
    }

    public List<String> getTags(){
        List<String> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery =  "SELECT * FROM " + TABLE_TAGS;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()){
            do{
                list.add(cursor.getString(0));
            }while(cursor.moveToNext());
        }

        if(cursor!=null && !cursor.isClosed())
            cursor.close();

        db.close();

        return list;
    }

    public void deleteTag(int id){
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_TAGS, KEY_TAG_ID + "=?", new String[]{String.valueOf(id)});
        db.close();
    }

    //endregion

    //region MANGATAGS TABLE

    public void addMangaTags(int mangaId, int tagId){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_TAGS_MANGA_ID, mangaId);
        values.put(KEY_TAGS_TAG_ID, tagId);

        db.insert(TABLE_MANGA_TAGS, null, values);
        db.close();
    }

    public List<Tag> getMangaTags(int mangaId){
        List<Tag> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT " + KEY_TAG_ID + ", " + KEY_TAG_TEXT + " FROM " + TABLE_MANGA_TAGS + " INNER JOIN " + TABLE_TAGS + " ON " +
                TABLE_MANGA_TAGS + "." + KEY_TAGS_TAG_ID + " = " + TABLE_TAGS + "." + KEY_TAG_ID + " WHERE " + TABLE_MANGA_TAGS + "." + KEY_TAGS_MANGA_ID + " = " + mangaId;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                Tag tag  = new Tag(cursor.getInt(0), cursor.getString(1));
                list.add(tag);
            }while (cursor.moveToNext());
        }

        if(cursor != null&&!cursor.isClosed()){
            cursor.close();
        }

        db.close();

        return list;
    }

    //endregion

}
