package com.endless.hov.mangakeeper;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Debug;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.RemoteViews;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, All.OnFragmentInteractionListener, SearchFragment.OnFragmentInteractionListener,
        HistoryFragment.OnFragmentInteractionListener, StatisticsFragment.OnFragmentInteractionListener,
        OnlineListFragment.OnFragmentInteractionListener, OnlineSearchFragment.OnFragmentInteractionListener, LatestReleasesFragment.OnFragmentInteractionListener,
        ScraperSearchFragment.OnFragmentInteractionListener, OfflineReleasesFragment.OnFragmentInteractionListener
{

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    boolean doubleBackToExitPressedOnce = false;


    final String pref_name = "PREF_NAME";
    final String pref_frag = "FRAG";
    final String pref_status = "STATUS";

    SharedPreferences settings;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));



        settings = getSharedPreferences(pref_name, Context.MODE_PRIVATE);



    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent i = getIntent();

        String frag = i.getStringExtra("frag");

        if(frag != null){
            if(frag.equals("search")){
                onSectionAttached(2);
            }
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    public void onSectionAttached(int number) {
        Fragment frag = new All();
        FragmentManager fm = getSupportFragmentManager();

        SharedPreferences.Editor editor = settings.edit();


        switch (number) {
            case 1:
                frag = All.newInstance(false);
                settings = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
                String title = settings.getString(pref_status, "All");
                if(title.equals("OnWait"))
                    title = "On Wait";
                else if(title.equals("PlanTo"))
                    title = "Plan To Read";
                mTitle = title;
                editor.putString(pref_frag, "All");

                break;
            case 2:
                frag = new SearchFragment();
                mTitle = getString(R.string.mangas2);

                break;
            case 3:
                frag = All.newInstance(true);
                mTitle = getString(R.string.mangas3);
                editor.putString(pref_frag, "Favorite");

                break;
            case 4:
                frag = new HistoryFragment();
                mTitle = getString(R.string.mangas4);

                break;
            case 5:
                frag = new StatisticsFragment();
                mTitle = getString(R.string.other1);

                break;
            case 6:
                //mTitle = getString(R.string.other2);
                Intent i = new Intent(this, Preferences.class);
                startActivity(i);
                break;
            case 7:
                Intent i2 = new Intent(this, TestActivity.class);
                startActivity(i2);
                break;
            case 8:
                frag = new OfflineReleasesFragment();
                mTitle = "Test Offline Releases";
                break;
            case 9:
                frag = new OnlineListFragment();
                mTitle = getString(R.string.online1);
                break;
            case 10:
                frag = new ScraperSearchFragment();
                mTitle = getString(R.string.online2);
                break;
            case 11:
                frag = new LatestReleasesFragment();
                mTitle = getString(R.string.online3);
                break;
        }

        fm.beginTransaction().replace(R.id.container, frag).commit();
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
        if(f instanceof All){
            String title = settings.getString(pref_status, "All");
            if(title.equals("OnWait"))
                title = "On Wait";
            else if(title.equals("PlanTo"))
                title = "Plan To Read";
            mTitle = title;
        }

        actionBar.setTitle(mTitle);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }else{
            FragmentManager fragmentManager = getSupportFragmentManager();

            String fragMain = settings.getString(pref_frag, "All");

            Fragment frag;
            if(fragMain.equals("All")) {
                frag = All.newInstance(false);
                mTitle = getString(R.string.mangas1);
            }
            else {
                frag = All.newInstance(true);
                mTitle = getString(R.string.mangas3);
            }
            fragmentManager.beginTransaction().replace(R.id.container, frag).commit();
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.





        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public NavigationDrawerFragment getmNavigationDrawerFragment(){
        return this.mNavigationDrawerFragment;
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

}
