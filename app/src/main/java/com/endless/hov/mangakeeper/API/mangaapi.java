package com.endless.hov.mangakeeper.API;


import com.endless.hov.mangakeeper.model.ChapterRead;
import com.endless.hov.mangakeeper.model.allmodel;
import com.endless.hov.mangakeeper.model.mangamodel;
import com.endless.hov.mangakeeper.model.searchmodel;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Barış on 30.6.2015.
 */
public interface mangaapi {
    @Headers("X-Mashape-Key: AIAdnJpgEEmsh2TiKTtoJZVq8zeTp1IvRSjjsn9QG6Pq9s8pwP")
    @GET("/{siteid}/manga/{mangaid}")      //here is the other url part.best way is to start using /
    public void getManga(@Path("siteid") String siteid, @Path("mangaid") String mangaid, Callback<mangamodel> response);


    @Headers("X-Mashape-Key: AIAdnJpgEEmsh2TiKTtoJZVq8zeTp1IvRSjjsn9QG6Pq9s8pwP")
    @GET("/{siteid}")
    public void getAll(@Path("siteid") String siteid, Callback<List<allmodel>> response);

    @Headers("X-Mashape-Key: AIAdnJpgEEmsh2TiKTtoJZVq8zeTp1IvRSjjsn9QG6Pq9s8pwP")

    @GET("/{siteid}/search")
    public void getSearch(@Path("siteid") String siteid, @Query("cover") int cover, @Query("g") String genres, @Query("info") int info, @Query("l") int limit, @Query("q") String query, Callback<List<searchmodel>> response);

    @Headers("X-Mashape-Key: AIAdnJpgEEmsh2TiKTtoJZVq8zeTp1IvRSjjsn9QG6Pq9s8pwP")
    @GET("/{siteid}/manga/{mangaid}/{chapterid}")
    public void getChapter(@Path("siteid") String siteid, @Path("mangaid") String mangaid, @Path("chapterid") String chapterid, Callback<ChapterRead> response);

}
