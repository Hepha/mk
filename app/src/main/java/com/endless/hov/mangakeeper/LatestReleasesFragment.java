package com.endless.hov.mangakeeper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.endless.hov.mangakeeper.Scraper.Scraper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LatestReleasesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LatestReleasesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LatestReleasesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    NonScrollListView lvList;
    ArrayAdapter<String> adapter;

    Button btnLoad;

    ProgressBar prog;

    List<String> idList = new ArrayList<>(), nameList = new ArrayList<>();

    int pageCount = 0;

    public LatestReleasesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LatestReleasesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LatestReleasesFragment newInstance(String param1, String param2) {
        LatestReleasesFragment fragment = new LatestReleasesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        init();

        if(nameList.size()==0) {
            pageCount=0;
            loadMore(pageCount++);
        }
        else{
            adapter.clear();
            adapter.addAll(nameList);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        adapter.clear();
        adapter.addAll(nameList);
        adapter.notifyDataSetChanged();
    }

    public void init(){
        lvList = (NonScrollListView) getView().findViewById(R.id.lvList);
        adapter = new ArrayAdapter<String>(getActivity().getBaseContext(), android.R.layout.simple_list_item_1);
        lvList.setAdapter(adapter);
        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MangaDatabaseHandler db = new MangaDatabaseHandler(getActivity().getBaseContext());
                int sid = db.exitsMangaID(idList.get(position));
                if (sid == -1) {
                    Intent si = new Intent(getActivity().getBaseContext(), ShowOnlineActivity.class);
                    si.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    si.putExtra("mangaId", idList.get(position));
                    startActivity(si);
                } else {
                    Intent si = new Intent(getActivity().getBaseContext(), ShowMangaActivity.class);
                    si.putExtra("ID", sid);
                    startActivity(si);
                }
            }
        });

        btnLoad = (Button)getView().findViewById(R.id.btnLoad);
        btnLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMore(pageCount++);
            }
        });

        prog = (ProgressBar)getView().findViewById(R.id.prog);
    }

    public void loadMore(final int page){
        pageCount++;
        prog.setVisibility(View.VISIBLE);
        btnLoad.setText("");
        btnLoad.setClickable(false);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    final String[][] list = Scraper.getLatestReleasesStrings(page);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            for(int i = 0; i<list.length;i++){
                                idList.add(list[i][0]);
                                nameList.add(list[i][1]);
                            }
                            adapter.clear();
                            adapter.addAll(nameList);

                            prog.setVisibility(View.GONE);
                            btnLoad.setText("Load More...");
                            btnLoad.setClickable(true);
                            adapter.notifyDataSetChanged();
                        }
                    });
                } catch (IOException e) {
                    Toast.makeText(getActivity().getBaseContext(), "Loading Failed", Toast.LENGTH_SHORT).show();
                }
            }
        };
        new Thread(runnable).start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_latest_releases, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) activity;
        } else {
            throw new RuntimeException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
