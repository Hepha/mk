package com.endless.hov.mangakeeper;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by Hepha on 11.10.2016.
 */
public class ThreeStateCheckBox extends Button {

    private int mState = 0;

    public ThreeStateCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public int getState(){
        return mState;
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        switch (mState){
            case 0:
                mState = 1;

                break;
            case 1:
                mState = 2;
                break;
            case 2:
                mState = 0;
                break;
        }
    }


}
