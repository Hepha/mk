package com.endless.hov.mangakeeper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.endless.hov.mangakeeper.Scraper.Scraper;

import org.apache.commons.lang3.ArrayUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ScraperSearchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ScraperSearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ScraperSearchFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;




    NonScrollListView lvSearch;
    ArrayAdapter<String> adapter;

    CheckBox cbAction, cbAdult, cbAdventure, cbComedy, cbDouhinshi, cbDrama, cbEcchi, cbFantasy, cbGenderBender, cbHarem, cbHistorical,
            cbHorror, cbJosei, cbMartialArts, cbMature, cbMecha, cbMystery, cbOneSHot, cbPsychological, cbRomance, cbSchoolLife, cbSciFi, cbSeinen, cbShoujo,
            cbShoujoAi, cbShouen, cbShounenAi, cbSliceOfLife, cbSmut, cbSports, cbSupernatural, cbTragedy, cbWebtoons, cbYaoi, cbYuri;

    CheckBox[] cbList;

    Button btnGenres, btnAdvanced, btnSearch, btnClear, btnLoad;

    HorizontalScrollView genresView;

    LinearLayout llAdvanced;

    RelativeLayout rlLoad;

    EditText etSearchTitle, etAuthor, etArtist, etReleased;

    Spinner spinReleasedType, spinRating, spinRatingType, spinCompleted, spinType;

    ProgressBar prog;

    String[][] loadedList;

    String[] lastSearchedStrings;
    int[] lastSearchedInts, lastSearcehCB;

    int pageCount, pageLimit;



    boolean genres = false, advanced = false;



    public ScraperSearchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ScraperSearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ScraperSearchFragment newInstance(String param1, String param2) {
        ScraperSearchFragment fragment = new ScraperSearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        init();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(loadedList!=null) {
            adapter.clear();
            for (int i = 0; i < loadedList.length; i++)
                adapter.add(loadedList[i][1]);
        }
    }

    public void init(){




        //SET LISTVIEWS
        lvSearch = (NonScrollListView)getView().findViewById(R.id.lvSearch);
        adapter = new ArrayAdapter<String>(getActivity().getBaseContext(), android.R.layout.simple_list_item_1, new ArrayList<String>());
        lvSearch.setAdapter(adapter);
        registerForContextMenu(lvSearch);
        lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MangaDatabaseHandler db = new MangaDatabaseHandler(getActivity().getBaseContext());
                int sid = db.exitsMangaID(loadedList[position][0]);
                if (sid == -1) {
                    Intent si = new Intent(getActivity().getBaseContext(), ShowOnlineActivity.class);
                    si.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    si.putExtra("mangaId", loadedList[position][0]);
                    startActivity(si);
                } else {
                    Intent si = new Intent(getActivity().getBaseContext(), ShowMangaActivity.class);
                    si.putExtra("ID", sid);
                    startActivity(si);
                }
            }
        });


        //SET EDITTEXT
        etSearchTitle = (EditText)getView().findViewById(R.id.etSearchTitle);
        etAuthor = (EditText)getView().findViewById(R.id.etAuthor);
        etArtist = (EditText)getView().findViewById(R.id.etArtist);
        etReleased = (EditText)getView().findViewById(R.id.etReleased);


        //SET CHECKBOXES
        cbAction = (CheckBox)getView().findViewById(R.id.cbAction);
        cbAdult = (CheckBox)getView().findViewById(R.id.cbAdult);
        cbAdventure = (CheckBox)getView().findViewById(R.id.cbAdventure);
        cbComedy = (CheckBox)getView().findViewById(R.id.cbComedy);
        cbDouhinshi = (CheckBox)getView().findViewById(R.id.cbDoujinshi);
        cbDrama = (CheckBox)getView().findViewById(R.id.cbDrama);
        cbEcchi = (CheckBox)getView().findViewById(R.id.cbEcchi);
        cbFantasy = (CheckBox)getView().findViewById(R.id.cbFantasy);
        cbGenderBender = (CheckBox)getView().findViewById(R.id.cbGenderBender);
        cbHarem = (CheckBox)getView().findViewById(R.id.cbHarem);
        cbHistorical = (CheckBox)getView().findViewById(R.id.cbHistorical);
        cbHorror = (CheckBox)getView().findViewById(R.id.cbHorror);
        cbJosei = (CheckBox)getView().findViewById(R.id.cbJosei);
        cbMartialArts = (CheckBox)getView().findViewById(R.id.cbMartialArts);
        cbMature = (CheckBox)getView().findViewById(R.id.cbMature);
        cbMecha = (CheckBox)getView().findViewById(R.id.cbMecha);
        cbMystery = (CheckBox)getView().findViewById(R.id.cbMystery);
        cbOneSHot = (CheckBox)getView().findViewById(R.id.cbOneShot);
        cbPsychological = (CheckBox)getView().findViewById(R.id.cbPsychological);
        cbRomance = (CheckBox)getView().findViewById(R.id.cbRomance);
        cbSchoolLife = (CheckBox)getView().findViewById(R.id.cbSchoolLife);
        cbSciFi = (CheckBox)getView().findViewById(R.id.cbSciFi);
        cbSeinen = (CheckBox)getView().findViewById(R.id.cbSeinen);
        cbShoujo = (CheckBox)getView().findViewById(R.id.cbShoujo);
        cbShoujoAi = (CheckBox)getView().findViewById(R.id.cbShoujoAi);
        cbShouen = (CheckBox)getView().findViewById(R.id.cbShounen);
        cbShounenAi = (CheckBox)getView().findViewById(R.id.cbShounenAi);
        cbSliceOfLife = (CheckBox)getView().findViewById(R.id.cbSliceOfLife);
        cbSmut = (CheckBox)getView().findViewById(R.id.cbSmut);
        cbSports = (CheckBox)getView().findViewById(R.id.cbSports);
        cbSupernatural = (CheckBox)getView().findViewById(R.id.cbSupernatural);
        cbTragedy = (CheckBox)getView().findViewById(R.id.cbTragedy);
        cbWebtoons = (CheckBox)getView().findViewById(R.id.cbWebtoons);
        cbYaoi = (CheckBox)getView().findViewById(R.id.cbYaoi);
        cbYuri = (CheckBox)getView().findViewById(R.id.cbYuri);

        cbList = new CheckBox[]{cbAction, cbAdult, cbAdventure, cbComedy, cbDouhinshi, cbDrama, cbEcchi, cbFantasy, cbGenderBender, cbHarem, cbHistorical,
                cbHorror, cbJosei, cbMartialArts, cbMature, cbMecha, cbMystery, cbOneSHot, cbPsychological, cbRomance, cbSchoolLife, cbSciFi, cbSeinen, cbShoujo,
                cbShoujoAi, cbShouen, cbShounenAi, cbSliceOfLife, cbSmut, cbSports, cbSupernatural, cbTragedy, cbWebtoons, cbYaoi, cbYuri};



        //SET HORIZONTAL/LINEAR VIEWS
        genresView = (HorizontalScrollView)getView().findViewById(R.id.genresView);
        llAdvanced = (LinearLayout)getView().findViewById(R.id.llAdvanced);
        rlLoad = (RelativeLayout) getView().findViewById(R.id.rlLoad);


        //SPINNERS
        spinReleasedType = (Spinner)getView().findViewById(R.id.spinReleasedType);
        spinReleasedType.setAdapter(new ArrayAdapter<String>(getActivity().getBaseContext(), android.R.layout.simple_list_item_1, new String[]{"on", "before", "after"}));

        spinRating = (Spinner)getView().findViewById(R.id.spinRating);
        spinRating.setAdapter(new ArrayAdapter<String>(getActivity().getBaseContext(), android.R.layout.simple_list_item_1, new String[]{"any star", "no star", "1 star", "2 stars", "3 stars", "4 stars", "5 stars"}));

        spinRatingType = (Spinner)getView().findViewById(R.id.spinRatingType);
        spinRatingType.setAdapter(new ArrayAdapter<String>(getActivity().getBaseContext(), android.R.layout.simple_list_item_1, new String[]{"is", "less than", "more than"}));

        spinCompleted = (Spinner)getView().findViewById(R.id.spinCompleted);
        spinCompleted.setAdapter(new ArrayAdapter<String>(getActivity().getBaseContext(), android.R.layout.simple_list_item_1, new String[]{"Yes", "No", "Either"}));
        spinCompleted.setSelection(2);

        spinType = (Spinner)getView().findViewById(R.id.spinType);
        spinType.setAdapter(new ArrayAdapter<String>(getActivity().getBaseContext(), android.R.layout.simple_list_item_1, new String[]{"Any", "Japanese Manga","Korean Manhwa","Chinese Manhua"}));


        prog = (ProgressBar)getView().findViewById(R.id.prog);

        //SET BUTTONS
        btnGenres = (Button)getView().findViewById(R.id.btnGenres);
        btnGenres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!advanced) {
                    if (genres) {
                        genres = false;
                        genresView.setVisibility(View.GONE);
                        btnGenres.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.ic_action_expand), null, null, null);
                    } else {
                        genres = true;
                        genresView.setVisibility(View.VISIBLE);
                        btnGenres.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.ic_action_collapse), null, null, null);
                    }
                }
            }
        });

        btnAdvanced = (Button)getView().findViewById(R.id.btnAdvanced);
        btnAdvanced.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!genres) {
                    if (advanced) {
                        advanced = false;
                        llAdvanced.setVisibility(View.GONE);
                        btnAdvanced.setCompoundDrawablesWithIntrinsicBounds(null, null, getActivity().getResources().getDrawable(R.drawable.ic_action_expand), null);
                    } else {
                        advanced = true;
                        llAdvanced.setVisibility(View.VISIBLE);
                        btnAdvanced.setCompoundDrawablesWithIntrinsicBounds(null, null, getActivity().getResources().getDrawable(R.drawable.ic_action_collapse), null);
                    }
                }
            }
        });

        btnSearch = (Button)getView().findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadedList = null;
                adapter.clear();
                lastSearchedStrings = new String[]{etSearchTitle.getText().toString().toLowerCase(),etAuthor.getText().toString().toLowerCase(),
                        etArtist.getText().toString().toLowerCase(),etReleased.getText().toString()};
                lastSearchedInts = new int[]{spinType.getSelectedItemPosition(), spinReleasedType.getSelectedItemPosition(),
                        spinRatingType.getSelectedItemPosition(),
                        spinRating.getSelectedItemPosition(),
                        spinCompleted.getSelectedItemPosition()};
                lastSearcehCB = checkboxCheck();
                pageCount = 1;

                search(pageCount);
            }
        });

        btnClear = (Button)getView().findViewById(R.id.btnClear);
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearAll();
            }
        });

        btnLoad = (Button)getView().findViewById(R.id.btnLoad);
        btnLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageCount++;
                search(pageCount);
            }
        });
    }

    public void search(final int page){
        rlLoad.setVisibility(View.VISIBLE);
        prog.setVisibility(View.VISIBLE);
        btnLoad.setText("");
        btnLoad.setClickable(false);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    final String[][] temp = Scraper.getSearch(lastSearchedStrings[0],
                            lastSearchedInts[0],
                            lastSearchedStrings[1],
                            lastSearchedStrings[2],
                            lastSearcehCB, lastSearchedInts[1],
                            lastSearchedStrings[3],
                            lastSearchedInts[2],
                            lastSearchedInts[3],
                            lastSearchedInts[4],
                            page);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(loadedList==null)
                                loadedList = temp;
                            else{
                                loadedList = ArrayUtils.addAll(loadedList, temp);
                            }
                            String[] titles = new String[temp.length];
                            for(int i = 0;i<temp.length;i++) {
                                titles[i] = temp[i][1];
                            }
                            adapter.addAll(titles);
                            adapter.notifyDataSetChanged();
                            if(page==1) {
                                pageLimit = Integer.parseInt(loadedList[0][5]);
                            }
                            if(pageCount==pageLimit){
                                rlLoad.setVisibility(View.GONE);
                            }
                            else{
                                prog.setVisibility(View.GONE);
                                btnLoad.setText("Load More...");
                                btnLoad.setClickable(true);
                            }
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(runnable).start();
    }

    public void clearAll(){
        for(int i = 0;i<35;i++)
            cbList[i].setChecked(false);

        etAuthor.setText("");
        etArtist.setText("");
        etReleased.setText("");
        etSearchTitle.setText("");
        spinRatingType.setSelection(0);
        spinRating.setSelection(0);
        spinReleasedType.setSelection(0);
        spinCompleted.setSelection(2);
    }

    public int[] checkboxCheck(){
        int[] boxes = new int[35];
        for(int i = 0;i<35;i++){
            if(cbList[i].isChecked())
                boxes[i] = 1;
            else
                boxes[i] = 0;
        }
        return boxes;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_scraper_search, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
