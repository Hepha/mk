package com.endless.hov.mangakeeper.Scraper;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Hepha on 14.09.2017.
 */

public class MangareaderScraper extends ScraperModel{

    private static final String siteUrl = "http://www.mangareader.net";

    public static ScraperManga getManga(String scrapId) throws IOException{
        scrapId = scrapId.replace("_", "-");
        Document doc = Jsoup.connect( siteUrl + "/" + scrapId).get();
        Element propTable = doc.getElementById("mangaproperties").child(1).child(0);

        //cover
        String cover = doc.getElementById("mangaimg").childNode(0).attr("src");


        //name
        String name = doc.getElementsByClass("aname").text();


        //status
        String status = propTable.child(3).child(1).text();

        //Released Year
        String year = propTable.child(2).child(1).text();

        //author
        String author = propTable.child(4).child(1).text();
        List<String> authors = new ArrayList<String>();
        String[] authorArr = author.split(",");
        for (String s: authorArr) {
            if(s.charAt(0)==" ".charAt(0)) s = s.substring(1, s.length());
            if(!s.isEmpty()) authors.add(s);
        }

        //artists
        String artist = propTable.child(5).child(1).text();
        List<String> artists = new ArrayList<String>();
        String[] artistArr = artist.split(",");
        for (String s: artistArr) {
            if(s.charAt(0)==" ".charAt(0)) s = s.substring(1, s.length());
            if(!s.isEmpty()) artists.add(s);
        }

        //genres
        List<String> genres = new ArrayList<>();
        Elements gElements = doc.getElementsByClass("genretags");
        for(Element e : gElements){
            genres.add(e.text());
        }

        //details
        String details = null;
        try{
            details = doc.getElementById("readmangasum").child(1).text();
        }catch(Exception e){
            details = "";
        }

        //chapters
        List<ScraperChapter> chapterList = new ArrayList<>();

        Elements tbodyList = doc.getElementById("listing").child(0).children();

        for(int i = 1; i<tbodyList.size();i++){
            Element e = tbodyList.get(i);

            //get id
            String id = e.child(0).getElementsByTag("a").first().text().replace(name + " ", "");

            //get title
            String title = e.child(0).text().replace(name + " " + id + " : ", "");

            //get release date
            String release = e.child(1).text();

            ScraperChapter chapter = new ScraperChapter(title, null, id, release);

            chapterList.add(chapter);
        }

        //reverse the list
        Collections.reverse(chapterList);

        ScraperManga manga = new ScraperManga(scrapId, name, cover, year, details, status, authors, artists, genres, chapterList);

        return manga;
    }


    public static String[][] getAllMangas() throws IOException{
        Document doc = Jsoup.connect(siteUrl + "/alphabetical").maxBodySize(0).timeout(0).get();

        Elements mangas = new Elements();

        Elements cols = doc.getElementsByClass("series_col");

        //work on both columns
        for(Element e:cols){
            Elements divs = e.children();
            for(Element d : divs){
                if(d.className().equals("series_alpha"))
                    mangas.addAll(d.child(1).children());
            }
        }

        //0-id 1-title
        String[][] ss = new String[mangas.size()][2];

        for(int i = 0; i<ss.length;i++){{
            Element m = mangas.get(i);

            //id
            String id = m.child(0).attr("href").replace("/", "");

            //title
            String title = m.child(0).text();

            ss[i][0] = id;
            ss[i][1] = title;
        }}

        return ss;
    }

}
