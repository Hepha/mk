package com.endless.hov.mangakeeper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import com.endless.hov.mangakeeper.API.mangaapi;
import com.endless.hov.mangakeeper.CustomAdapters.MangaListAdapter;
import com.endless.hov.mangakeeper.CustomAdapters.TemporaryAdapter;
import com.endless.hov.mangakeeper.Scraper.Scraper;
import com.endless.hov.mangakeeper.Scraper.ScraperManga;
import com.endless.hov.mangakeeper.model.mangamodel;
import com.melnykov.fab.FloatingActionButton;

import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link All.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link All#newInstance} factory method to
 * create an instance of this fragment.
 */
public class All extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_FAV = "isFav";

    // TODO: Rename and change types of parameters
    private boolean isFav;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment All.
     */


    TextView tvSort;

    ListView lvMain;
    TemporaryAdapter adapter;
    MangaListAdapter adapterGrid;

    FloatingActionButton fabAdd, fabTop;


    List<Manga> list, search;


    //FOR SEARCH
    boolean activityViewOn = false;
    String sea = "";


    final String endPoint = "https://doodle-manga-scraper.p.mashape.com/";
    final String coverPath = Environment.getExternalStorageDirectory().toString()+"/MankaKeeper";

    final String pref_name = "PREF_NAME";
    final String pref_sort = "SORT";
    final String pref_status = "STATUS";
    final String pref_site = "apiList";
    final String pref_adap = "layoutAdapter";
    final String pref_frag = "FRAG";

    int status = -1, adap;

    String sort, stat;

    String siteid;

    int Lindex = 0, Ltop = 0;


    SharedPreferences settings;

    boolean isAPI = true;

    MangaDatabaseHandler db;


    // TODO: Rename and change types and number of parameters
    public static All newInstance(boolean isFav) {
        All fragment = new All();
        Bundle args = new Bundle();
        args.putBoolean(ARG_FAV, isFav);
        fragment.setArguments(args);
        return fragment;
    }

    public All() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            isFav = getArguments().getBoolean(ARG_FAV);
        }
    }

    @Override
    public void onStart() {
        super.onStart();


        settings = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());

        if (!settings.getString("callSystem", "API").equals("API")) isAPI = false;

        settings = getActivity().getSharedPreferences(pref_name, Context.MODE_PRIVATE);

        setHasOptionsMenu(true);

        tvSort = (TextView)getView().findViewById(R.id.tvSort);


        adap = settings.getInt(pref_adap, 0);


        lvMain = (ListView)getView().findViewById(R.id.lvMain);
        adapter = new TemporaryAdapter(getActivity().getBaseContext(), R.layout.temporary, new ArrayList<Manga>());
        adapterGrid = new MangaListAdapter(getActivity(),  R.layout.manga_shelf);
        if(adap==0)
            lvMain.setAdapter(adapter);
        else if(adap==1)
            lvMain.setAdapter(adapterGrid);


        registerForContextMenu(getView().findViewById(R.id.lvMain));

        db = new MangaDatabaseHandler(getActivity().getBaseContext());


        siteid = settings.getString(pref_site, "mangafox.me");



        setList();




        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getActivity().getBaseContext(), ShowMangaActivity.class);
                if (activityViewOn || sea.length() != 0 || sea != null)
                    i.putExtra("ID", search.get(position).getId());
                else
                    i.putExtra("ID", list.get(position).getId());
                startActivity(i);
            }
        });


        fabAdd = (FloatingActionButton)getView().findViewById(R.id.fabAdd);
        fabAdd.attachToListView(lvMain);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getBaseContext(), AddMangaActivity.class);
                startActivity(i);
            }
        });

        fabTop = (FloatingActionButton)getView().findViewById(R.id.fabTop);


    }

    @Override
    public void onPause() {
        super.onPause();
        Lindex = lvMain.getFirstVisiblePosition();
        View v = lvMain.getChildAt(0);
        Ltop = (v == null) ? 0 : (v.getTop() - lvMain.getPaddingTop());
    }

    @Override
    public void onResume() {
        super.onResume();
        adap = settings.getInt(pref_adap, 0);

        setList();

        lvMain.setSelectionFromTop(Lindex, Ltop);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(db!=null)
            db.close();
    }

    public void setList(){

        sort = settings.getString(pref_sort, "Update");
        stat = settings.getString(pref_status, "All");

        if (stat.equals("All"))
            status = -1;
        else if (stat.equals("Completed"))
            status = 1;
        else if (stat.equals("Reading"))
            status = 0;
        else if (stat.equals("OnWait"))
            status = 2;
        else if (stat.equals("PlanTo"))
            status = 3;
        else if (stat.equals("Hiatus"))
            status = 4;
        else if (stat.equals("CatchUp"))
            status = 5;


        String[] ar = tvSort.getText().toString().split(":");
        String f = ar[0] + ":";
        switch (status){
            case -1:
                f += "All";
                break;
            case 0:
                f += "Reading";
                break;
            case 1:
                f += "Completed";
                break;
            case 2:
                f += "On Wait";
                break;
            case 3:
                f += "Plan To Read";
                break;
            case 4:
                f += "Hiatus";
                break;
            case 5:
                f += "Catch Up";
                break;
        }


        tvSort.setText(f);

        refreshList();

        if(sort.equals("ABC"))
            sortListWithABC();
        else if(sort.equals("Update"))
            sortListWithUpdate();
        else if(sort.equals("Add"))
            sortListWithAdd();

        if(activityViewOn||sea.length()!=0||sea!=null)
            setSearched(sea);
    }




    public void refreshList(){
        MangaDatabaseHandler db = new MangaDatabaseHandler(getActivity().getBaseContext());
        if(status==-1) {
            if(!isFav)
                list = db.getAllMangas();
            else
                list = db.getFavorites();
        }
        else {
            if(!isFav)
                list = db.getWithStatus(status);
            else
                list = db.getWithStatusFavorites(status);
        }
        if(list.size()==0)
            Toast.makeText(getActivity().getBaseContext(), "Couldn't find any manga", Toast.LENGTH_SHORT).show();


    }

    public void sortListWithUpdate(){
        String[] ar = tvSort.getText().toString().split(":");
        tvSort.setText("Sorted by Latest Update:" + ar[1]);


        List<Manga> sort = list;

        int c,d, n = list.size();
        Manga swap;

        for (c = 0; c < ( n - 1 ); c++) {
            for (d = 0; d < n - c - 1; d++) {
                if (sort.get(d).updateTime < sort.get(d+1).updateTime) /* For descending order use < */
                {
                    swap       = sort.get(d);
                    sort.set(d, sort.get(d+1));
                    sort.set(d+1, swap);
                }
            }
        }

        list = sort;

        //lvMain.setAdapter(new TemporaryAdapter(getActivity().getBaseContext(), R.layout.temporary, list));
        if(adap==0){
            adapter.setNotifyOnChange(false);
            adapter.clear();
            adapter.addAll(list);
            adapter.notifyDataSetChanged();
        }
        else{
            adapterGrid.setNotifyOnChange(false);
            adapterGrid.clear();
            adapterGrid.addAll(list);
            adapterGrid.notifyDataSetChanged();
        }
    }

    public void sortListWithAdd(){
        String[] ar = tvSort.getText().toString().split(":");
        tvSort.setText("Sorted by Latest Added:" + ar[1]);

        List<Manga> sort = list;

        int c,d, n = list.size();
        Manga swap;

        for (c = 0; c < ( n - 1 ); c++) {
            for (d = 0; d < n - c - 1; d++) {
                if (sort.get(d).addTime < sort.get(d+1).addTime) /* For descending order use < */
                {
                    swap       = sort.get(d);
                    sort.set(d, sort.get(d+1));
                    sort.set(d+1, swap);
                }
            }
        }

        list = sort;

        //lvMain.setAdapter(new TemporaryAdapter(getActivity().getBaseContext(), R.layout.temporary, list));
        if(adap==0){
            adapter.setNotifyOnChange(false);
            adapter.clear();
            adapter.addAll(list);
            adapter.notifyDataSetChanged();
        }
        else{
            adapterGrid.setNotifyOnChange(false);
            adapterGrid.clear();
            adapterGrid.addAll(list);
            adapterGrid.notifyDataSetChanged();
        }
    }

    public void sortListWithABC(){
        String[] ar = tvSort.getText().toString().split(":");
        tvSort.setText("Sorted Alphabetically:"+ar[1]);



        ArrayList<String> names = new ArrayList<String>();

        for(int i = 0; i<list.size(); i++){
            names.add(list.get(i).getTitle() + "===="+ i);
        }

        java.util.Collections.sort(names);


        Manga[] t = new Manga[list.size()];

        for(int b = 0; b<names.size();b++){
            int id = Integer.parseInt(names.get(b).split("====")[1]);
            t[b] = list.get(id);
        }

        for(int i = 0; i<list.size();i++){
            list.set(i, t[i]);
        }


        //lvMain.setAdapter(new TemporaryAdapter(getActivity().getBaseContext(), R.layout.temporary, list));
        if(adap==0){
            adapter.setNotifyOnChange(false);
            adapter.clear();
            adapter.addAll(list);
            adapter.notifyDataSetChanged();
        }
        else{
            adapterGrid.setNotifyOnChange(false);
            adapterGrid.clear();
            adapterGrid.addAll(list);
            adapterGrid.notifyDataSetChanged();
        }
    }


    public void setSearched(String s) {
        search = new ArrayList<Manga>();
        search.clear();
        for(int i = 0; i<list.size(); i++){
            if(list.get(i).getTitle().toLowerCase().contains(s.toLowerCase())||list.get(i).getAuthor().toLowerCase().contains(s.toLowerCase()))
                search.add(list.get(i));
        }

        //lvMain.setAdapter(new TemporaryAdapter(getActivity().getBaseContext(), R.layout.temporary, search));
        if(adap==0){
            adapter.setNotifyOnChange(false);
            adapter.clear();
            adapter.addAll(search);
            adapter.notifyDataSetChanged();
        }
        else{
            adapterGrid.setNotifyOnChange(false);
            adapterGrid.clear();
            adapterGrid.addAll(search);
            adapterGrid.notifyDataSetChanged();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment




        return inflater.inflate(R.layout.fragment_all, container, false);


    }

    public void deleteManga(final int id){
        new AlertDialog.Builder(getActivity())
                .setTitle("Delete manga")
                .setMessage("Are you sure you want to delete this manga?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        db.deleteManga(id);
                        setList();
                        showFabTop();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    public void showFabTop(){
        if(true){
            Animation openAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_open);
            fabTop.startAnimation(openAnim);
            fabTop.setClickable(true);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation closeAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_close);
                    fabTop.startAnimation(closeAnim);
                    fabTop.setClickable(false);
                }
            }, 2000);
            fabTop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lvMain.setSelectionAfterHeaderView();
                    lvMain.setSelectionAfterHeaderView();
                    Animation closeAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_close);
                    fabTop.startAnimation(closeAnim);
                    fabTop.setClickable(false);
                }
            });

        }
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.all_menu, menu);


        if(adap==1){
            MenuItem item = menu.findItem(R.id.layoutAdapter);
            item.setIcon(getResources().getDrawable(R.drawable.ic_view_headline_white_24dp));
        }

        /** Get the action view of the menu item whose id is search */
        View v = menu.findItem(R.id.action_search).getActionView();

        /** Get the edit text from the action view */
        final EditText txtSearch = ( EditText ) v.findViewById(R.id.txt_search);


        v.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v)
            {
                activityViewOn = true;
                txtSearch.requestFocus();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);
            }

            @Override
            public void onViewDetachedFromWindow(View v) {
                activityViewOn = false;
                /*InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(),0);*/
                if(getActivity()!=null)
                    setList();
                txtSearch.setText("");
            }
        });


        /** Setting an action listener */
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                sea = s.toString();
                if (s.length() > 0)
                    setSearched(s.toString());
                else {
                    if (getActivity() != null)
                        setList();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        SharedPreferences.Editor editor = settings.edit();
        ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menuSortUpdate) {
            item.setTitle("Sorting by latest update");
            editor.putString(pref_sort, "Update");
            editor.commit();
            setList();
            lvMain.setSelectionAfterHeaderView();

            return true;
        }
        else if(id == R.id.menuSortABC){
            editor.putString(pref_sort, "ABC");
            editor.commit();
            setList();
            lvMain.setSelectionAfterHeaderView();

            return true;

        }
        else if(id == R.id.menuSortAdd){
            editor.putString(pref_sort, "Add");
            editor.commit();
            setList();
            lvMain.setSelectionAfterHeaderView();

            return true;
        }
        /*Moved to floating action button, way better..
        else if(id == R.id.action_new){
            Intent i = new Intent(getActivity().getBaseContext(), AddMangaActivity.class);
            startActivity(i);
            return true;
        }
        */
        else if(id == R.id.menuAll){
            editor.putString(pref_status, "All");
            editor.commit();
            if(!isFav)
                actionBar.setTitle("All");
            status = -1;
            setList();
            lvMain.setSelectionAfterHeaderView();
            ((MainActivity)this.getActivity()).getmNavigationDrawerFragment().setMangas(0);

            return true;
        }
        else if(id == R.id.menuCompleted){
            editor.putString(pref_status, "Completed");
            editor.commit();
            if(!isFav)
                actionBar.setTitle("Completed");
            status = 1;
            setList();
            lvMain.setSelectionAfterHeaderView();
            ((MainActivity)this.getActivity()).getmNavigationDrawerFragment().setMangas(1);

            return true;
        }
        else if(id == R.id.menuReading){
            editor.putString(pref_status, "Reading");
            editor.commit();
            if(!isFav)
                actionBar.setTitle("Reading");
            status = 0;
            setList();
            lvMain.setSelectionAfterHeaderView();
            ((MainActivity)this.getActivity()).getmNavigationDrawerFragment().setMangas(2);

            return true;
        }
        else if(id == R.id.menuOnWait){
            editor.putString(pref_status, "OnWait");
            editor.commit();
            if(!isFav)
                actionBar.setTitle("On Wait");
            status = 2;
            setList();
            lvMain.setSelectionAfterHeaderView();
            ((MainActivity)this.getActivity()).getmNavigationDrawerFragment().setMangas(3);

            return true;
        }
        else if(id == R.id.menuPlanTo){
            editor.putString(pref_status, "PlanTo");
            editor.commit();
            if(!isFav)
                actionBar.setTitle("Plan To Read");
            status = 3;
            setList();
            lvMain.setSelectionAfterHeaderView();
            ((MainActivity)this.getActivity()).getmNavigationDrawerFragment().setMangas(4);

            return true;
        }
        else if(id == R.id.menuHiatus){
            editor.putString(pref_status, "Hiatus");
            editor.commit();
            if(!isFav)
                actionBar.setTitle("Hiatus");
            status = 4;
            setList();
            lvMain.setSelectionAfterHeaderView();
            ((MainActivity)this.getActivity()).getmNavigationDrawerFragment().setMangas(5);

            return true;
        }
        else if(id == R.id.menuCatchUp){
            editor.putString(pref_status, "CatchUp");
            editor.commit();
            if(!isFav)
                actionBar.setTitle("Catch Up");
            status = 5;
            setList();
            lvMain.setSelectionAfterHeaderView();
            ((MainActivity)this.getActivity()).getmNavigationDrawerFragment().setMangas(6);

            return true;
        }
        else if(id == R.id.layoutAdapter){
            if(adap==0) {
                item.setIcon(getResources().getDrawable(R.drawable.ic_view_headline_white_24dp));
                adap=1;
                int index = lvMain.getFirstVisiblePosition();
                lvMain.setAdapter(adapterGrid);
                lvMain.setSelectionFromTop(index/3, 0);
                editor.putInt(pref_adap, 1).commit();
            }
            else{
                item.setIcon(getResources().getDrawable(R.drawable.ic_grid_on_white_24dp));
                adap = 0;
                int index = lvMain.getFirstVisiblePosition();
                lvMain.setAdapter(adapter);
                lvMain.setSelectionFromTop(index * 3, 0);
                editor.putInt(pref_adap, 0).commit();
            }
            setList();
        }

        else if(id == R.id.action_search){
            /*FragmentManager fragmentManager = getFragmentManager();
            Fragment frag = new SearchFragment();
            fragmentManager.beginTransaction().replace(R.id.container, frag).commit();*/
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()==R.id.lvMain) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            Manga m;
            if(activityViewOn||sea.length()!=0||sea!=null) {
                if(adap==0)
                    m = search.get(info.position);
                else{
                    m = search.get(info.position*3 + adapterGrid.getLonClickPostion());
                }
            }
            else {
                if(adap==0)
                    m = list.get(info.position);
                else{
                    m = list.get(info.position*3 + adapterGrid.getLonClickPostion());
                }
            }

            menu.setHeaderTitle(m.getTitle());
            String[] menuItems = getResources().getStringArray(R.array.manga_context);
            for (int i = 0; i<menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.manga_context);

        Manga m;
        if(activityViewOn||sea.length()!=0||sea!=null) {
            if(adap==0)
                m = search.get(info.position);
            else{
                m = search.get(info.position*3 + adapterGrid.getLonClickPostion());
            }
        }
        else {
            if(adap==0)
                m = list.get(info.position);
            else{
                m = list.get(info.position*3 + adapterGrid.getLonClickPostion());
            }
        }
        final Manga ma = m;

        if(menuItems[menuItemIndex].equals("Edit")){
            Intent i = new Intent(getActivity().getBaseContext(), AddMangaActivity.class);
            i.putExtra("ID", m.getId());
            startActivity(i);
        }
        else if(menuItems[menuItemIndex].equals("Delete")){
            deleteManga(ma.getId());
        }
        else if(menuItems[menuItemIndex].equals("Set Status")){
            AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
            b.setTitle("Set Status");
            String[] statusList = {"Reading", "Completed", "On Wait", "Plan To Read", "Hiatus"};
            b.setItems(statusList, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                    ma.setStatus(which);
                    db.updateManga(ma, getActivity().getBaseContext());
                    setList();
                    showFabTop();
                }

            });

            b.show();
        }
        else if(menuItems[menuItemIndex].equals("Check For Updates")){
            if (isAPI) {
                RestAdapter restAdapter = new RestAdapter.Builder()
                        .setEndpoint(endPoint).build();

                mangaapi manga = restAdapter.create(mangaapi.class);

                if(!ma.getApiID().equals("")) {
                    manga.getManga(siteid, ma.getApiID(), new Callback<mangamodel>() {
                        @Override
                        public void success(mangamodel mangamodel, Response response) {
                            if (mangamodel != null) {
                                int newReleased = mangamodel.getChapters().get(mangamodel.getChapters().size() - 1).getChapterId().intValue();
                                if (newReleased > ma.getReleasedChapter()) {
                                    Toast.makeText(getActivity().getBaseContext(), "Chapters released: " + String.valueOf(newReleased-ma.getReleasedChapter()), Toast.LENGTH_SHORT).show();
                                    ma.setReleasedChapter(newReleased);
                                    db.updateManga(ma, getActivity().getBaseContext());
                                    setList();
                                    showFabTop();
                                }
                                else
                                    Toast.makeText(getActivity().getBaseContext(),"No new chapters", Toast.LENGTH_SHORT).show();
                            } else
                                Toast.makeText(getActivity().getBaseContext(), "Couldn't find any manga with this api id:" + ma.getApiID(), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(getActivity().getBaseContext(), "There was a problem with api call", Toast.LENGTH_SHORT).show();
                        }

                    });
                }
                else
                    Toast.makeText(getActivity().getBaseContext(), "This manga doesn't have an api id", Toast.LENGTH_SHORT).show();
            } else {
                if (!ma.getApiID().equals("")) {
                    final String scrapId = SomeFunctions.apiToScraper(ma.getApiID());
                    Log.d("Refresh from", "Scraper-" + scrapId);
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            try {
                                ScraperManga manga = Scraper.getManga(scrapId);
                                if (manga != null) {
                                    final int newReleased = Integer.parseInt(manga.getChapters().get(0).getChapterId());
                                    if (newReleased > ma.getReleasedChapter()) {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity().getBaseContext(), "Chapters released: " + String.valueOf(newReleased - ma.getReleasedChapter()), Toast.LENGTH_SHORT).show();
                                                ma.setReleasedChapter(newReleased);
                                                db.updateManga(ma, getActivity().getBaseContext());
                                                setList();
                                                showFabTop();
                                            }
                                        });

                                    } else {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Log.d("NewReleased", "" + newReleased);
                                                Toast.makeText(getActivity().getBaseContext(), "No new chapters", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    new Thread(runnable).start();
                } else
                    Toast.makeText(getActivity().getBaseContext(), "This manga doesn't have an api id", Toast.LENGTH_SHORT).show();
            }
        }
        else if(menuItems[menuItemIndex].equals("Catch Up")){
            if(ma.getReadChapter()==ma.getReleasedChapter())
                Toast.makeText(getActivity().getBaseContext(), "You've already caught up.You can check for new chapters", Toast.LENGTH_SHORT).show();
            else{
                ma.setReadChapter(ma.getReleasedChapter());
                db.updateManga(ma, getActivity().getBaseContext());
                setList();
                showFabTop();
            }
        }
        else if(menuItems[menuItemIndex].equals("+1 Read")){
            if(ma.getReadChapter()==ma.getReleasedChapter())
                ma.setReleasedChapter(ma.getReleasedChapter()+1);
            ma.setReadChapter(ma.getReadChapter()+1);
            db.updateManga(ma, getActivity().getBaseContext());
            setList();
            showFabTop();
        }
        else if(menuItems[menuItemIndex].equals("+1 Released")){
            ma.setReleasedChapter(ma.getReleasedChapter()+1);
            db.updateManga(ma, getActivity().getBaseContext());
            setList();
            showFabTop();
        }



        return true;
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
