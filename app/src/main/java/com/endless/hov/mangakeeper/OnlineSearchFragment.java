package com.endless.hov.mangakeeper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.endless.hov.mangakeeper.API.mangaapi;
import com.endless.hov.mangakeeper.CustomAdapters.ListCoverAdapter;
import com.endless.hov.mangakeeper.model.searchmodel;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnlineSearchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OnlineSearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OnlineSearchFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;



    //
    TextView tvFound;

    EditText etSearchTitle;

    ListView lvSearch;
    ListCoverAdapter adapterCover;


    String[] genreList = {"action","adult", "adventure", "comedy", "doujinshi", "drama", "ecchi","fantasy",
            "gender-bender","harem","historical","horror","josei","martial-arts","mature","mecha","mystery","one-shot",
            "psychological","romance","school-life","sci-fi","seinen","shoujo","shoujo-ai","shounen","shounen-ai",
            "slice-of-life","smut","sports","supernatural","tragedy","webtoons","yaoi","yuri"};
    CheckBox cbAction, cbAdult, cbAdventure, cbComedy, cbDouhinshi, cbDrama, cbEcchi, cbFantasy, cbGenderBender, cbHarem, cbHistorical,
            cbHorror, cbJosei, cbMartialArts, cbMature, cbMecha, cbMystery, cbOneSHot, cbPsychological, cbRomance, cbSchoolLife, cbSciFi, cbSeinen, cbShoujo,
            cbShoujoAi, cbShouen, cbShounenAi, cbSliceOfLife, cbSmut, cbSports, cbSupernatural, cbTragedy, cbWebtoons, cbYaoi, cbYuri;
    CheckBox[] cbList;

    HorizontalScrollView genresView;

    Button btnGenres, btnSearch;

    RelativeLayout loading;

    ArrayList<searchmodel> listGot;

    boolean genres = false;

    int count = 1;


    SharedPreferences settings;

    final String endPoint = "https://doodle-manga-scraper.p.mashape.com";

    final String pref_site = "apiList";
    final String pref_name = "PREF_NAME";

    String siteid;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OnlineSearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OnlineSearchFragment newInstance(String param1, String param2) {
        OnlineSearchFragment fragment = new OnlineSearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public OnlineSearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        listGot = new ArrayList<searchmodel>();
    }

    @Override
    public void onStart() {
        super.onStart();
        init();
    }

    public void init(){

        //PREFERENCES
        settings = getActivity().getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        siteid = settings.getString(pref_site, "mangafox.me");

        //ImageLoader
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity()).build();

        ImageLoader.getInstance().init(config);


        //SET LISTVIEWS
        lvSearch = (ListView)getView().findViewById(R.id.lvSearch);
        registerForContextMenu(lvSearch);
        adapterCover = new ListCoverAdapter(getActivity(), R.layout.list_cover_adapter);


        /*lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getActivity(), ShowOnlineActivity.class);
                i.putExtra("mangaId", listGot.get(position).getMangaId());
                startActivity(i);
            }
        });*/

        //RELATIVE LAYOUT
        loading = (RelativeLayout)getView().findViewById(R.id.loadingPanel);


        //SET EDITTEXT
        etSearchTitle = (EditText)getView().findViewById(R.id.etSearchTitle);

        //
        tvFound = (TextView)getView().findViewById(R.id.tvFound);


        //SET CHECKBOXES
        cbAction = (CheckBox)getView().findViewById(R.id.cbAction);
        cbAdult = (CheckBox)getView().findViewById(R.id.cbAdult);
        cbAdventure = (CheckBox)getView().findViewById(R.id.cbAdventure);
        cbComedy = (CheckBox)getView().findViewById(R.id.cbComedy);
        cbDouhinshi = (CheckBox)getView().findViewById(R.id.cbDoujinshi);
        cbDrama = (CheckBox)getView().findViewById(R.id.cbDrama);
        cbEcchi = (CheckBox)getView().findViewById(R.id.cbEcchi);
        cbFantasy = (CheckBox)getView().findViewById(R.id.cbFantasy);
        cbGenderBender = (CheckBox)getView().findViewById(R.id.cbGenderBender);
        cbHarem = (CheckBox)getView().findViewById(R.id.cbHarem);
        cbHistorical = (CheckBox)getView().findViewById(R.id.cbHistorical);
        cbHorror = (CheckBox)getView().findViewById(R.id.cbHorror);
        cbJosei = (CheckBox)getView().findViewById(R.id.cbJosei);
        cbMartialArts = (CheckBox)getView().findViewById(R.id.cbMartialArts);
        cbMature = (CheckBox)getView().findViewById(R.id.cbMature);
        cbMecha = (CheckBox)getView().findViewById(R.id.cbMecha);
        cbMystery = (CheckBox)getView().findViewById(R.id.cbMystery);
        cbOneSHot = (CheckBox)getView().findViewById(R.id.cbOneShot);
        cbPsychological = (CheckBox)getView().findViewById(R.id.cbPsychological);
        cbRomance = (CheckBox)getView().findViewById(R.id.cbRomance);
        cbSchoolLife = (CheckBox)getView().findViewById(R.id.cbSchoolLife);
        cbSciFi = (CheckBox)getView().findViewById(R.id.cbSciFi);
        cbSeinen = (CheckBox)getView().findViewById(R.id.cbSeinen);
        cbShoujo = (CheckBox)getView().findViewById(R.id.cbShoujo);
        cbShoujoAi = (CheckBox)getView().findViewById(R.id.cbShoujoAi);
        cbShouen = (CheckBox)getView().findViewById(R.id.cbShounen);
        cbShounenAi = (CheckBox)getView().findViewById(R.id.cbShounenAi);
        cbSliceOfLife = (CheckBox)getView().findViewById(R.id.cbSliceOfLife);
        cbSmut = (CheckBox)getView().findViewById(R.id.cbSmut);
        cbSports = (CheckBox)getView().findViewById(R.id.cbSports);
        cbSupernatural = (CheckBox)getView().findViewById(R.id.cbSupernatural);
        cbTragedy = (CheckBox)getView().findViewById(R.id.cbTragedy);
        cbWebtoons = (CheckBox)getView().findViewById(R.id.cbWebtoons);
        cbYaoi = (CheckBox)getView().findViewById(R.id.cbYaoi);
        cbYuri = (CheckBox)getView().findViewById(R.id.cbYuri);

        cbList = new CheckBox[]{cbAction, cbAdult, cbAdventure, cbComedy, cbDouhinshi, cbDrama, cbEcchi, cbFantasy, cbGenderBender, cbHarem, cbHistorical,
                cbHorror, cbJosei, cbMartialArts, cbMature, cbMecha, cbMystery, cbOneSHot, cbPsychological, cbRomance, cbSchoolLife, cbSciFi, cbSeinen, cbShoujo,
                cbShoujoAi, cbShouen, cbShounenAi, cbSliceOfLife, cbSmut, cbSports, cbSupernatural, cbTragedy, cbWebtoons, cbYaoi, cbYuri};



        //SET HORIZONTAL VIEWS
        genresView = (HorizontalScrollView)getView().findViewById(R.id.genresView);

        //SET BUTTONS
        btnGenres = (Button)getView().findViewById(R.id.btnGenres);
        btnGenres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (genres) {
                    genres = false;
                    genresView.setVisibility(View.GONE);
                    btnGenres.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.ic_action_expand), null, null, null);
                } else {
                    genres = true;
                    genresView.setVisibility(View.VISIBLE);
                    btnGenres.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.ic_action_collapse), null, null, null);
                }
            }
        });

        btnSearch = (Button)getView().findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading.setVisibility(View.VISIBLE);
                search();
            }
        });
    }

    public void search(){
        //Creating the genre query string
        String gen = "[";
        for(int i = 0; i<genreList.length;i++){
            if(cbList[i].isChecked()){
                gen+="\"" + genreList[i] + "\",";
            }
        }
        if(gen.equals("["))
            gen = "";
        else{
            gen = gen.substring(0, gen.length()-1);
            gen+="]";
        }

        count = 1;

        String title = etSearchTitle.getText().toString();

        RestAdapter ra = new RestAdapter.Builder()
                .setEndpoint(endPoint)
                .build();
        mangaapi m = ra.create(mangaapi.class);

        m.getSearch(siteid, 1, gen, 0, 0, title, new Callback<List<searchmodel>>() {
            @Override
            public void success(List<searchmodel> searchmodels, Response response) {
                listGot.clear();
                listGot.addAll(searchmodels);
                tvFound.setText("Found:"+ listGot.size());
                adapterCover.clear();
                adapterCover.setSize(listGot.size());
                int treshold = 10;
                if (listGot.size() < 10) treshold = listGot.size();
                for (int i = 0; i < treshold; i++)
                    adapterCover.add(listGot.get(i));
                lvSearch.setAdapter(adapterCover);
                loading.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("Call error", error.getLocalizedMessage());
                Toast.makeText(getActivity(), "Couldn't find any manga with given info", Toast.LENGTH_SHORT).show();
            }
        });


        lvSearch.setOnScrollListener(new EndlessScrollListener(10) {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                if (count * 10 + 10 > listGot.size()) {
                    for (int i = count * 10; i < listGot.size(); i++)
                        adapterCover.add(listGot.get(i));
                } else {
                    for (int i = count * 10; i < count * 10 + 10; i++)
                        adapterCover.add(listGot.get(i));
                }
                adapterCover.notifyDataSetChanged();
                count++;
                return false;
            }
        });


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_online_search, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
