package com.endless.hov.mangakeeper.CustomAdapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.endless.hov.mangakeeper.HistoryEntry;
import com.endless.hov.mangakeeper.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;



public class HistoryAdapter extends ArrayAdapter<HistoryEntry>{


    public HistoryAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public HistoryAdapter(Context context, int resource, HistoryEntry[] objects) {
        super(context, resource, objects);
    }

    public HistoryAdapter(Context context, int resource, List<HistoryEntry> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View v = convertView;

        if(v==null){
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.history_adapter,null);
        }

        HistoryEntry s = getItem(position);

        TextView tvTitle = (TextView)v.findViewById(R.id.tvTitle);
        TextView tvHistory = (TextView)v.findViewById(R.id.tvHistory);
        TextView tvDate = (TextView)v.findViewById(R.id.tvDate);

        if(s!=null){
            long date = s.getTime();
            String text = s.getOutput();

            long difference = System.currentTimeMillis() - date;

            String time = "";
            if(difference<=(1000*60)) {
                int sec = Math.round(difference/1000);
                time = sec + " seconds ago";
            }else if(difference<=(1000*60*60)){
                int min = Math.round((difference/(1000*60)));
                time = min + " minutes ago";
            }else if(difference<=1000*60*60*24){
                int hour = Math.round((difference/(1000*60*60)));
                time = hour + " hours ago";
            }else if(difference<=1000*60*60*24*30){
                int day = Math.round((difference/(1000*60*60*24)));
                time = day + " days ago";
            }
            else{

                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                String sort = preferences.getString("historyDate", "date");

                if(sort.equals("date")) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                    Date d = new Date(date);
                    time = sdf.format(d);
                }
                else {
                    int day = Math.round((difference / (1000 * 60 * 60 * 24)));
                    time = day + " days ago";
                }

            }



            if(tvTitle!=null)
                tvTitle.setText(s.getMangaTitle());
            if(tvHistory!=null)
                tvHistory.setText(text);
            if(tvDate!=null)
                tvDate.setText(time);

        }

        return v;
    }
}
