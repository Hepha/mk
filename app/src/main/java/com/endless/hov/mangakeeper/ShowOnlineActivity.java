package com.endless.hov.mangakeeper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.endless.hov.mangakeeper.API.mangaapi;
import com.endless.hov.mangakeeper.Scraper.Scraper;
import com.endless.hov.mangakeeper.Scraper.ScraperChapter;
import com.endless.hov.mangakeeper.Scraper.ScraperManga;
import com.endless.hov.mangakeeper.model.mangamodel;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ShowOnlineActivity extends AppCompatActivity {


    String mangaId;

    ImageView ivCover;
    TextView tvTitle, tvAuthor, tvYear, tvStatus, tvGenres, tvChapter, tvDetails;
    FloatingActionButton fab, fabRead;
    RelativeLayout loading, loadingImage;
    Toolbar toolbar;


    SharedPreferences settings;

    final String endPoint = "https://doodle-manga-scraper.p.mashape.com";

    final String pref_site = "apiList";
    final String pref_name = "PREF_NAME";

    String siteid;


    //MANGA STUFF
    Bitmap cover;

    mangamodel man;
    ScraperManga manga;


    //CALL SYSTEM
    boolean isAPI = true;


    //GEnre
    String[] genreList = {"Action","Adult", "Adventure", "Comedy", "Doujinshi", "Drama", "Ecchi","Fantasy",
            "Gender Bender","Harem","Historical","Horror","Josei","Martial Arts","Mature","Mecha","Mystery","One Shot",
            "Psychological","Romance","School Life","Sci-fi","Seinen","Shoujo","Shoujo Ai","Shounen","Shounen Ai",
            "Slice of Life","Smut","Sports","Supernatural","Tragedy","Webtoons","Yaoi","Yuri"};
    String[] genreListWeb = {"action","adult", "adventure", "comedy", "doujinshi", "drama", "ecchi","fantasy",
            "gender-bender","harem","historical","horror","josei","martial-arts","mature","mecha","mystery","one-shot",
            "psychological","romance","school-life","sci-fi","seinen","shoujo","shoujo-ai","shounen","shounen-ai",
            "slice-of-life","smut","sports","supernatural","tragedy","webtoons","yaoi","yuri"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_online);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Loading...");

        //Get id
        mangaId = getIntent().getExtras().getString("mangaId");
        mangaId = SomeFunctions.apiToScraper(mangaId);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String call = preferences.getString("callSystem", "API");
        if(!call.equals("API")) isAPI = false;

        Log.d("Final ID", mangaId);



        //VIEWS
        ivCover = (ImageView)findViewById(R.id.ivCover);
        tvTitle = (TextView)findViewById(R.id.tvTitle);
        tvAuthor = (TextView)findViewById(R.id.tvAuthor);
        tvYear = (TextView)findViewById(R.id.tvYear);
        tvStatus = (TextView)findViewById(R.id.tvStatus);
        tvGenres = (TextView)findViewById(R.id.tvGenres);
        tvChapter = (TextView)findViewById(R.id.tvChapter);
        tvDetails = (TextView)findViewById(R.id.tvDetails);
        loading = (RelativeLayout)findViewById(R.id.loadingPanel);
        loadingImage = (RelativeLayout)findViewById(R.id.loadingImage);

        //Pref
        settings = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        siteid = settings.getString(pref_site, "mangafox.me");



        //ImageLoader
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);



        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addManga();
            }
        });
        fab.setVisibility(View.GONE);

        fabRead = (FloatingActionButton)findViewById(R.id.fabRead);
        fabRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isAPI)
                    readChapters();
            }
        });
        fabRead.setVisibility(View.GONE);

        if(isAPI)
            loadMangaFromApi();
        else
            loadMangaFromSCraper();
    }

    public void readChapters(){

        final ListView listView = new ListView(this);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        adapter.add("Wait until chapters load...");
        listView.setAdapter(adapter);
        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(listView).create();
        dialog.show();

        final String scrapId = SomeFunctions.apiToScraper(manga.getScrapId());
        Log.d("Loading from", "Scraper-" + scrapId);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    final List<ScraperChapter> chapters = Scraper.getManga(scrapId).getChapters();
                    String[] list = new String[chapters.size()];
                    for (int i = 0; i < list.length; i++) {
                        list[i] = chapters.get(i).toString();
                    }
                    final String[] arr = list;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.clear();
                            adapter.addAll(arr);
                            adapter.notifyDataSetChanged();
                            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Intent i = new Intent(getBaseContext(), ReadChapterActivity.class);
                                    i.putExtra("mangaid", scrapId);
                                    i.putExtra("currentChapter", position);
                                    i.putExtra("title", manga.getTitle());
                                    String[] ch = new String[chapters.size()];
                                    for (int j = 0; j < ch.length; j++)
                                        ch[j] = "" + chapters.get(j).getChapterId();
                                    i.putExtra("chapterids", ch);
                                    String[] vo = new String[chapters.size()];
                                    for (int j = 0; j < ch.length; j++)
                                        vo[j] = "" + chapters.get(j).getVolume().getTitle();
                                    i.putExtra("volumeids", ch);

                                    startActivity(i);
                                }
                            });
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(runnable).start();
    }

    public void addManga(){
        final MangaDatabaseHandler db = new MangaDatabaseHandler(this);

        if(db.exitsMangaID(mangaId)!=-1){
            Toast.makeText(getBaseContext(), "You already have this manga listed!", Toast.LENGTH_SHORT).show();
        }
        else {
            ListView listStatus = new ListView(this);
            listStatus.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, new String[]{"Reading", "Completed", "On Wait", "Plan To Read", "Hiatus"}));
            AlertDialog dialog = new AlertDialog.Builder(this).setView(listStatus).create();


            listStatus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Manga m;
                    if(isAPI){
                     m = new Manga(mangaId, man.getName(), tvAuthor.getText().toString(), man.getInfo(), "",
                            man.getChapters().size(), 0, 0, 0, position, getGenres(), System.currentTimeMillis(), System.currentTimeMillis(), 0);
                    }else{
                        String aut = "";
                        for (int i = 0;i<manga.getAuthors().size();i++){
                            aut+=manga.getAuthors().get(i);
                            if(i!=manga.getAuthors().size()-1)
                                aut+=", ";
                        }
                        m = new Manga(mangaId, manga.getTitle(), aut, manga.getDetails(), "", Integer.parseInt(manga.getChapters().get(manga.getChapters().size()-1).getChapterId()), 0, 0 ,0,position
                        , manga.getGenres(), System.currentTimeMillis(), System.currentTimeMillis(), 0);
                    }
                    db.addManga(m);
                    saveImage(db.getHighestID());
                    Toast.makeText(getBaseContext(), "Manga Added:" + man.getName(), Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
            dialog.show();
        }
    }

    public void loadMangaFromApi(){
        RestAdapter re = new RestAdapter.Builder()
                .setEndpoint(endPoint)
                .build();
        mangaapi m = re.create(mangaapi.class);

        m.getManga(siteid, mangaId, new Callback<mangamodel>() {
            @Override
            public void success(mangamodel mangamodel, Response response) {

                //COVER
                final ImageLoader imageLoader = ImageLoader.getInstance();

                // Load image, decode it to Bitmap and display Bitmap in ImageView (or any other view
                //  which implements ImageAware interface)
                imageLoader.displayImage(mangamodel.getCover(), ivCover, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        Log.d("loading cover failed", failReason.toString());
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        cover = loadedImage;
                        loadingImage.setVisibility(View.GONE);
                        fab.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                    }
                });

                Log.d("Loaded from", "API");
                toolbar.setTitle(mangamodel.getName());
                man = mangamodel;
                tvTitle.setText(mangamodel.getName());
                String author = mangamodel.getAuthor().get(0);
                author = author.replace("-", " ");
                StringBuffer stringbfa = new StringBuffer();
                Matcher ma = Pattern.compile("([a-z])([a-z]*)",
                        Pattern.CASE_INSENSITIVE).matcher(author);
                while (ma.find()) {
                    ma.appendReplacement(stringbfa,
                            ma.group(1).toUpperCase() + ma.group(2).toLowerCase());
                }
                tvAuthor.setText(ma.appendTail(stringbfa).toString());
                tvStatus.setText(mangamodel.getStatus());
                tvYear.setText("" + mangamodel.getYearOfRelease());
                tvChapter.setText("Released Chapters:" + mangamodel.getChapters().size());
                tvDetails.setText(mangamodel.getInfo());

                String s = "";
                for (int i = 0; i < mangamodel.getGenres().size(); i++) {
                    String genre = mangamodel.getGenres().get(i);
                    genre = genre.replace("-", " ");
                    StringBuffer stringbf = new StringBuffer();
                    Matcher m = Pattern.compile("([a-z])([a-z]*)",
                            Pattern.CASE_INSENSITIVE).matcher(genre);
                    while (m.find()) {
                        m.appendReplacement(stringbf,
                                m.group(1).toUpperCase() + m.group(2).toLowerCase());
                    }
                    s += m.appendTail(stringbf).toString();

                    if (i != mangamodel.getGenres().size() - 1)
                        s += ",";
                }
                tvGenres.setText(s);

                loading.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void loadMangaFromSCraper(){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    manga = Scraper.getManga(mangaId);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("Loaded", manga.getTitle());
                            toolbar.setTitle(manga.getTitle());
                            tvTitle.setText(manga.getTitle());
                            String aut = "";
                            for (int i = 0;i<manga.getAuthors().size();i++){
                                aut+=manga.getAuthors().get(i);
                                if(i!=manga.getAuthors().size()-1)
                                    aut+=", ";
                                tvAuthor.setText(aut);
                            }
                            String art = "";
                            for (int i = 0;i<manga.getArtists().size();i++) {
                                art += manga.getArtists().get(i);
                                if (i != manga.getArtists().size() - 1)
                                    art += ", ";
                                //tvAuthor.setText(art);
                            }
                            String genres = "";
                            for (int i = 0;i<manga.getGenres().size();i++) {
                                genres += manga.getGenres().get(i);
                                if (i != manga.getGenres().size() - 1)
                                    genres += ", ";
                                tvGenres.setText(genres);
                            }

                            tvDetails.setText(manga.getDetails());
                            tvStatus.setText(manga.getStatus());
                            tvYear.setText(manga.getReleasedYear());
                            loading.setVisibility(View.GONE);

                            final ImageLoader imageLoader = ImageLoader.getInstance();

                            imageLoader.displayImage(manga.getCover(), ivCover, new ImageLoadingListener() {
                                @Override
                                public void onLoadingStarted(String imageUri, View view) {

                                }

                                @Override
                                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                    Log.d("loading cover failed", failReason.toString());
                                }

                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                    cover = loadedImage;
                                    loadingImage.setVisibility(View.GONE);
                                    fab.setVisibility(View.VISIBLE);
                                    fabRead.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onLoadingCancelled(String imageUri, View view) {

                                }
                            });
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };
        new Thread(runnable).start();

    }


    public List<String> getGenres(){
        List<String> list = new ArrayList<>();

        for(int i = 0; i<genreList.length;i++){
            if(Arrays.asList(man.getGenres()).contains(genreListWeb[i]))
                list.add(genreList[i]);
        }

        return list;
    }

    public void saveImage(int id) {

        File check = new File(Environment.getExternalStorageDirectory(), "MankaKeeper");

        if(!check.mkdir()){
            Log.d("Directory doesn't exist", check.getAbsolutePath());

        }


        String path = Environment.getExternalStorageDirectory().toString()+"/MankaKeeper";
        OutputStream fOut = null;
        File file = new File(path, id +".jpg"); // the File to save to
        try {
            fOut = new FileOutputStream(file);
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }

        if (cover != null){
            //Bitmap pictureBitmap = getBitmapFromURL(imageUrl); // obtaining the Bitmap
            cover.compress(Bitmap.CompressFormat.JPEG, 85, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
            try {
                fOut.flush();
                fOut.close(); // do not forget to close the stream
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
                Log.d("Image saved:", "ID:" + id + ", Path:" + path);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

}
