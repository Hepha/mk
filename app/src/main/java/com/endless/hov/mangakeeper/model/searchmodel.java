package com.endless.hov.mangakeeper.model;


import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;


public class searchmodel {

    @Expose
    private String mangaId;
    @Expose
    private String name;
    @Expose
    private List<String> genres = new ArrayList<String>();
    @Expose
    private String cover;
    @Expose
    private String info;

    /**
     *
     * @return
     * The mangaId
     */
    public String getMangaId() {
        return mangaId;
    }

    /**
     *
     * @param mangaId
     * The mangaId
     */
    public void setMangaId(String mangaId) {
        this.mangaId = mangaId;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The genres
     */
    public List<String> getGenres() {
        return genres;
    }

    /**
     *
     * @param genres
     * The genres
     */
    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    /**
     *
     * @return
     * The cover
     */
    public String getCover() {
        return cover;
    }

    /**
     *
     * @param cover
     * The cover
     */
    public void setCover(String cover) {
        this.cover = cover;
    }

    /**
     *
     * @return
     * The info
     */
    public String getInfo() {
        return info;
    }

    /**
     *
     * @param info
     * The info
     */
    public void setInfo(String info) {
        this.info = info;
    }

}