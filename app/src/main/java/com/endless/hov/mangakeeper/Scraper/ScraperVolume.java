package com.endless.hov.mangakeeper.Scraper;


public class ScraperVolume {
    private String title, span;

    public ScraperVolume(String title, String span) {
        this.title = title;
        this.span = span;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSpan() {
        return span;
    }

    public void setSpan(String span) {
        this.span = span;
    }
}
