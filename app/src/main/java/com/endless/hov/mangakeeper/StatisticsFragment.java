package com.endless.hov.mangakeeper;

import android.app.Activity;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link StatisticsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link StatisticsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StatisticsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;





    TextView tvTotal, tvCompleted, tvReading, tvOnWait, tvPlanTo, tvChapter, tvTime, tvFavorite, tvHiatus, tvTotalTime;


    String[] times = new String[3];
    int count = 0;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment StatisticsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StatisticsFragment newInstance(String param1, String param2) {
        StatisticsFragment fragment = new StatisticsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public StatisticsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public void onStart() {
        super.onStart();

        init();


    }



    public void init(){

        //DEFINE TEXTVIEWS

        tvTotal = (TextView)getView().findViewById(R.id.tvTotal);
        tvCompleted = (TextView)getView().findViewById(R.id.tvCompleted);
        tvReading = (TextView)getView().findViewById(R.id.tvReading);
        tvOnWait = (TextView)getView().findViewById(R.id.tvOnWait);
        tvPlanTo = (TextView)getView().findViewById(R.id.tvPlanTo);
        tvChapter = (TextView)getView().findViewById(R.id.tvChapter);
        tvTime = (TextView)getView().findViewById(R.id.tvTime);
        tvFavorite = (TextView)getView().findViewById(R.id.tvFavorite);
        tvHiatus = (TextView)getView().findViewById(R.id.tvHiatus);
        tvTotalTime = (TextView)getView().findViewById(R.id.tvTotalText);


        //GET THE VALUES

        MangaDatabaseHandler db = new MangaDatabaseHandler(getActivity().getBaseContext());

        List<Manga> list = db.getAllMangas();

        int total = 0, completed = 0, reading = 0, onwait = 0, planto = 0, chapter = 0, favorite = 0, hiatus = 0;

        for(Manga m : list){
            total++;

            switch (m.getStatus()){
                case 0:
                    reading++;
                    break;
                case 1:
                    completed++;
                    break;
                case 2:
                    onwait++;
                    break;
                case 3:
                    planto++;
                    break;
                case 4:
                    hiatus++;
                    break;
            }

            chapter += m.getReadChapter();

            if(m.getFavoriteBool())
                favorite++;
        }


        //SET TEXTVIEWS

        tvTotal.setText(""+total);
        tvCompleted.setText(""+completed);
        tvReading.setText(""+reading);
        tvOnWait.setText(""+onwait);
        tvPlanTo.setText(""+planto);
        tvChapter.setText(""+chapter);
        tvFavorite.setText(""+favorite);
        tvHiatus.setText(""+hiatus);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        int pref = Integer.parseInt(preferences.getString("statisticAverage", "2"));

        tvTotalTime.setText("Total Time Spent("+pref+" min per chapter)");

        int time = chapter*pref;
        int hour = time/60;
        int minute = time%60;

        tvTime.setText(hour + " hours " + minute + " minutes");

        times[0] = hour + " hours " + minute + " minutes";

        int day = hour/24;
        hour = hour%24;
        times[1] = day + " days " + hour+"."+ ((minute*100)/60) + " hours";

        int week = day/7;
        day = day%7;
        times[2] = week + " weeks " + day+"."+ ((hour*100)/24) + " days";

        tvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                if(count==3) count=0;
                tvTime.setText(times[count]);
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_statistics, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
