package com.endless.hov.mangakeeper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.endless.hov.mangakeeper.API.mangaapi;
import com.endless.hov.mangakeeper.Scraper.Scraper;
import com.endless.hov.mangakeeper.Scraper.ScraperManga;
import com.endless.hov.mangakeeper.model.allmodel;
import com.endless.hov.mangakeeper.model.mangamodel;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class AddMangaActivity extends ActionBarActivity implements View.OnClickListener{


    Spinner spinStatuts;

    Button btnGetMF, btnGetMR,btnCoverSet,btnCancel, btnAdd;

    EditText etTitle, etAuthor, etDetail, etChapterRead, etChapterReleased, etVolumeRead, etVolumeReleased, etApiId;

    CheckBox cbAction, cbAdult, cbAdventure, cbComedy, cbDouhinshi, cbDrama, cbEcchi, cbFantasy, cbGenderBender, cbHarem, cbHistorical,
    cbHorror, cbJosei, cbMartialArts, cbMature, cbMecha, cbMystery, cbOneSHot, cbPsychological, cbRomance, cbSchoolLife, cbSciFi, cbSeinen, cbShoujo,
    cbShoujoAi, cbShouen, cbShounenAi, cbSliceOfLife, cbSmut, cbSports, cbSupernatural, cbTragedy, cbWebtoons, cbYaoi, cbYuri;

    CheckBox[] cbList;

    ImageView ivCover;


    Bitmap cover, old;

    boolean isFavorited = false;

    boolean isAPI = true;


    //FOR TEST
    ListView lvSearch;

    final ArrayList<allmodel> searchAll = new ArrayList<allmodel>();
    final ArrayList<allmodel> searchSel = new ArrayList<allmodel>();







    final String endPoint = "https://doodle-manga-scraper.p.mashape.com";

    private String[] statusList = {"Reading", "Completed", "On Wait", "Plan To Read", "Hiatus"};

    final String coverPath = Environment.getExternalStorageDirectory().toString()+"/MankaKeeper/";


    int id = 0;
    boolean coverChanged = false;

    MangaDatabaseHandler db;


    SharedPreferences settings;

    final String pref_name = "PREF_NAME";
    final String pref_site = "apiList";

    String siteid;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_manga);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();

        ImageLoader.getInstance().init(config);

        
        try{
            id = getIntent().getExtras().getInt("ID");
            Log.d("ID", "" + id);
        }
        catch (Exception e){
            Log.d("No ID:", "Add Task");
        }

        if(getIntent().getExtras()!=null)
            isFavorited = getIntent().getExtras().getBoolean("favorites", false);
        
        
        
        intializeComponents();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        db.close();
        ImageLoader.getInstance().destroy();
    }

    public void intializeComponents(){

        //PREFERENCES
        settings = getSharedPreferences(pref_name, MODE_PRIVATE);
        siteid = settings.getString(pref_site, "mangafox.me");

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if(!preferences.getString("callSystem", "API").equals("API")) isAPI = false;


        //DATABASE
        db = new MangaDatabaseHandler(getBaseContext());


        //SET SPINNER
        spinStatuts = (Spinner)findViewById(R.id.spinStatus);

        //SET BUTTONS
        btnGetMF = (Button)findViewById(R.id.btnGetMF);
        btnGetMF.setOnClickListener(this);
        btnGetMR = (Button)findViewById(R.id.btnGetMR);
        btnGetMR.setOnClickListener(this);
        btnCoverSet = (Button)findViewById(R.id.btnCoverSet);
        btnCoverSet.setOnClickListener(this);
        btnCancel = (Button)findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);
        btnAdd = (Button)findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);


        //SET IMAGEVIEWS
        ivCover = (ImageView)findViewById(R.id.ivCover);
        ivCover.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_new));

        //SET EDITTEXTS
        etTitle = (EditText)findViewById(R.id.etTitle);
        etAuthor = (EditText)findViewById(R.id.etAuthor);
        etDetail = (EditText)findViewById(R.id.etDetails);
        etChapterRead = (EditText)findViewById(R.id.etChapterRead);
        etChapterReleased = (EditText)findViewById(R.id.etChapterReleased);
        etVolumeRead = (EditText)findViewById(R.id.etVolumeRead);
        etVolumeReleased = (EditText)findViewById(R.id.etVolumeReleased);
        etApiId = (EditText)findViewById(R.id.etApiId);

        //SET CHECKBOXES
        cbAction = (CheckBox)findViewById(R.id.cbAction);
        cbAdult = (CheckBox)findViewById(R.id.cbAdult);
        cbAdventure = (CheckBox)findViewById(R.id.cbAdventure);
        cbComedy = (CheckBox)findViewById(R.id.cbComedy);
        cbDouhinshi = (CheckBox)findViewById(R.id.cbDoujinshi);
        cbDrama = (CheckBox)findViewById(R.id.cbDrama);
        cbEcchi = (CheckBox)findViewById(R.id.cbEcchi);
        cbFantasy = (CheckBox)findViewById(R.id.cbFantasy);
        cbGenderBender = (CheckBox)findViewById(R.id.cbGenderBender);
        cbHarem = (CheckBox)findViewById(R.id.cbHarem);
        cbHistorical = (CheckBox)findViewById(R.id.cbHistorical);
        cbHorror = (CheckBox)findViewById(R.id.cbHorror);
        cbJosei = (CheckBox)findViewById(R.id.cbJosei);
        cbMartialArts = (CheckBox)findViewById(R.id.cbMartialArts);
        cbMature = (CheckBox)findViewById(R.id.cbMature);
        cbMecha = (CheckBox)findViewById(R.id.cbMecha);
        cbMystery = (CheckBox)findViewById(R.id.cbMystery);
        cbOneSHot = (CheckBox)findViewById(R.id.cbOneShot);
        cbPsychological = (CheckBox)findViewById(R.id.cbPsychological);
        cbRomance = (CheckBox)findViewById(R.id.cbRomance);
        cbSchoolLife = (CheckBox)findViewById(R.id.cbSchoolLife);
        cbSciFi = (CheckBox)findViewById(R.id.cbSciFi);
        cbSeinen = (CheckBox)findViewById(R.id.cbSeinen);
        cbShoujo = (CheckBox)findViewById(R.id.cbShoujo);
        cbShoujoAi = (CheckBox)findViewById(R.id.cbShoujoAi);
        cbShouen = (CheckBox)findViewById(R.id.cbShounen);
        cbShounenAi = (CheckBox)findViewById(R.id.cbShounenAi);
        cbSliceOfLife = (CheckBox)findViewById(R.id.cbSliceOfLife);
        cbSmut = (CheckBox)findViewById(R.id.cbSmut);
        cbSports = (CheckBox)findViewById(R.id.cbSports);
        cbSupernatural = (CheckBox)findViewById(R.id.cbSupernatural);
        cbTragedy = (CheckBox)findViewById(R.id.cbTragedy);
        cbWebtoons = (CheckBox)findViewById(R.id.cbWebtoons);
        cbYaoi = (CheckBox)findViewById(R.id.cbYaoi);
        cbYuri = (CheckBox)findViewById(R.id.cbYuri);

        cbList = new CheckBox[]{cbAction, cbAdult, cbAdventure, cbComedy, cbDouhinshi, cbDrama, cbEcchi, cbFantasy, cbGenderBender, cbHarem, cbHistorical,
                cbHorror, cbJosei, cbMartialArts, cbMature, cbMecha, cbMystery, cbOneSHot, cbPsychological, cbRomance, cbSchoolLife, cbSciFi, cbSeinen, cbShoujo,
                cbShoujoAi, cbShouen, cbShounenAi, cbSliceOfLife, cbSmut, cbSports, cbSupernatural, cbTragedy, cbWebtoons, cbYaoi, cbYuri};


       spinStatuts.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, statusList));

        spinStatuts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 1)
                    etChapterRead.setText(etChapterReleased.getText());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        //EDIT MANGA INIT

        if(id!=0){
            Manga m = db.getManga(id);
            Log.d("Loaded Manga:", m.getTitle());

            btnAdd.setText("Update");
            getSupportActionBar().setTitle("Update Manga");


            File imgFile = new File(coverPath + id + ".jpg");
            if(imgFile.exists()){
                Bitmap bmp = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                ivCover.setImageBitmap(bmp);
            }
            else
                ivCover.setBackgroundColor(Color.BLACK);

            etTitle.setText(m.getTitle());
            etAuthor.setText(m.getAuthor());
            etDetail.setText(m.getDetail());
            etChapterRead.setText(""+m.getReadChapter());
            etChapterReleased.setText(""+m.getReleasedChapter());
            etVolumeRead.setText(""+m.getReadVolume());
            etVolumeReleased.setText(""+m.getReleasedVolume());
            etApiId.setText(m.getApiID());


            spinStatuts.setSelection(m.getStatus());

            setCheckboxes(m.getGenres());

            final ImageLoader imageLoader = ImageLoader.getInstance();

            // Load image, decode it to Bitmap and display Bitmap in ImageView (or any other view
            //  which implements ImageAware interface)
            imageLoader.displayImage("file://"+coverPath + id + ".jpg", ivCover, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    Toast.makeText(getBaseContext(), "Loading Cover Failed:" + failReason.getCause(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    old = cover;
                    cover = loadedImage;
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    Toast.makeText(getBaseContext(), "Loading Cover Canceled", Toast.LENGTH_SHORT).show();
                }
            });

        }

        initSearchList();

    }


    public void initSearchList(){
        if(isAPI) initSearchListFromAPI();
        else initSearchListFromScraper();
    }


    //JUST FOR TEST, FOR NOW -.- Seems like it's working, even i didn't expect that
    public void initSearchListFromAPI(){
        lvSearch = (ListView)findViewById(R.id.lvSearch);



        RestAdapter ra = new RestAdapter.Builder()
                .setEndpoint(endPoint)
                .build();
        mangaapi m = ra.create(mangaapi.class);

        m.getAll(siteid, new Callback<List<allmodel>>() {
            @Override
            public void success(List<allmodel> allmodels, Response response) {
                searchAll.clear();
                searchAll.addAll(allmodels);
                Toast.makeText(getBaseContext(), "List loaded", Toast.LENGTH_SHORT).show();
            }


            @Override
            public void failure(RetrofitError error) {

            }
        });


        final ArrayList<String> names = new ArrayList<String>();

        etTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
                if(s.toString().length()>0){
                    lvSearch.setVisibility(View.VISIBLE);
                    names.clear();
                    searchSel.clear();
                    for(allmodel a: searchAll){
                        if(a.getName().toLowerCase().contains(s.toString().toLowerCase())) {
                            searchSel.add(a);
                            names.add(a.getName());
                        }
                    }
                    lvSearch.setAdapter(new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, names));

                }else
                    lvSearch.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setWithID(searchSel.get(position).getMangaId(), siteid);
            }
        });
    }

    public void initSearchListFromScraper(){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    final String[][] listSearched = Scraper.getAllMangas();
                    final List<String> listSel = new ArrayList<>();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getBaseContext(), "List loaded", Toast.LENGTH_SHORT).show();
                            lvSearch = (ListView)findViewById(R.id.lvSearch);


                            etTitle.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(final CharSequence s, int start, int before, int count) {
                                    if(s.toString().length()>0){
                                        lvSearch.setVisibility(View.VISIBLE);
                                        listSel.clear();
                                        final ArrayList<String> names = new ArrayList<String>();
                                        for(int i = 0;i<listSearched.length;i++) {
                                            if(listSearched[i][1].toLowerCase().contains(s.toString().toLowerCase())){
                                                names.add(listSearched[i][1]);
                                                listSel.add(listSearched[i][0]);
                                            }
                                        }


                                        lvSearch.setAdapter(new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, names));

                                    }else
                                        lvSearch.setVisibility(View.INVISIBLE);
                                }

                                @Override
                                public void afterTextChanged(Editable s) {

                                }
                            });

                            lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    setWithScraper(listSel.get(position));
                                }
                            });
                        }
                    });

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(runnable).start();
    }

    public void setWithScraper(final String scraperid){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    final ScraperManga manga = Scraper.getManga(scraperid);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            etTitle.setText(manga.getTitle());
                            lvSearch.setVisibility(View.INVISIBLE);

                            String authors = "";
                            for (int i = 0; i < manga.getAuthors().size(); i++) {
                                if(manga.getAuthors().size()>0) {
                                    authors += idToName(manga.getAuthors().get(i));
                                    if (i != manga.getAuthors().size() - 1)
                                        authors += ",";
                                }
                            }

                            etApiId.setText(scraperid);

                            etAuthor.setText(authors);

                            etDetail.setText(manga.getDetails());

                            coverChanged = true;
                            setCover(manga.getCover());

                            int released = (int)Double.parseDouble(manga.getChapters().get(0).getChapterId());
                            etChapterReleased.setText("" + released);


                            List<String> genres = manga.getGenres();

                            setCheckboxes(genres);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(runnable).start();
    }


    @Override
    public void onClick(View v) {



        switch (v.getId()){
            case R.id.btnCoverSet:
                coverAlert();
                break;
            case R.id.btnCancel:
                this.finish();
                break;
            case R.id.btnAdd:
                MangaDatabaseHandler db = new MangaDatabaseHandler(getBaseContext());
                if(checkAll()){
                    Manga manga = new Manga(etApiId.getText().toString(), etTitle.getText().toString(), etAuthor.getText().toString(),etDetail.getText().toString(),
                            "", Integer.parseInt(etChapterReleased.getText().toString()), Integer.parseInt(etChapterRead.getText().toString()),
                            Integer.parseInt(etVolumeReleased.getText().toString()),Integer.parseInt(etVolumeRead.getText().toString()),
                            spinStatuts.getSelectedItemPosition(), genres(), System.currentTimeMillis(), System.currentTimeMillis(), 0);
                    if(isFavorited) manga.setFavorite(true);

                    Intent i = new Intent(this, ShowMangaActivity.class);

                    if(id==0) {
                        db.addManga(manga);
                        saveImage(db.getHighestID());
                        i.putExtra("ID", db.getHighestID());
                        HistoryEntry h = new HistoryEntry(db.getHighestID(), "", 1, System.currentTimeMillis(), "");
                        db.addHistory(h);
                    }else{
                        Manga m = db.getManga(id);
                        m.setApiID(etApiId.getText().toString());
                        m.setTitle(etTitle.getText().toString());
                        m.setAuthor(etAuthor.getText().toString());
                        m.setReleasedChapter(Integer.parseInt(etChapterReleased.getText().toString()));
                        m.setReadChapter( Integer.parseInt(etChapterRead.getText().toString()));
                        m.setReleasedVolume(Integer.parseInt(etVolumeReleased.getText().toString()));
                        m.setReadVolume(Integer.parseInt(etVolumeRead.getText().toString()));
                        m.setStatus(spinStatuts.getSelectedItemPosition());
                        m.setGenres(genres());

                        if(coverChanged){
                            //m.addHistoryCoverChange();
                            HistoryEntry h = new HistoryEntry(m.getId(), "", 9, System.currentTimeMillis(), "");
                            db.addHistory(h);
                        }
                        db.updateManga(m, getBaseContext());
                        saveImage(id);
                        i.putExtra("ID", id);
                    }

                    startActivity(i);
                    this.finish();
                }
                break;
            case R.id.btnGetMF:
                String name = etTitle.getText().toString();
                if (!name.equals(""))
                    setWithName(name, "mangafox.me");
                else
                    Toast.makeText(getBaseContext(),"Enter title", Toast.LENGTH_SHORT);
                break;
            case R.id.btnGetMR:
                name = etTitle.getText().toString();
                if (!name.equals(""))
                    setWithName(name, "mangareader.net");
                else
                    Toast.makeText(getBaseContext(),"Enter title", Toast.LENGTH_SHORT);
                break;

        }
    }


    public List<String> genres (){
        String[] genreList = {"Action","Adult", "Adventure", "Comedy", "Doujinshi", "Drama", "Ecchi","Fantasy",
                "Gender Bender","Harem","Historical","Horror","Josei","Martial Arts","Mature","Mecha","Mystery","One Shot",
                "Psychological","Romance","School Life","Sci-fi","Seinen","Shoujo","Shoujo Ai","Shounen","Shounen Ai",
                "Slice of Life","Smut","Sports","Supernatural","Tragedy","Webtoons","Yaoi","Yuri"};

        ArrayList<String> list = new ArrayList<String>();

        for(int i = 0; i<genreList.length;i++){
            if(cbList[i].isChecked()){
                list.add(genreList[i]);
            }
        }

        return list;
    }


    public boolean checkAll(){
        boolean b = true;

        if(etTitle.equals("")||etAuthor.getText().toString().equals("")||etChapterRead.getText().toString().equals("")
                ||etChapterReleased.getText().toString().equals("")||etVolumeRead.getText().toString().equals("")||etVolumeReleased.getText().toString().equals("")) {
            b = false;
            Toast.makeText(this, "Fill necessary places!",Toast.LENGTH_SHORT).show();
        }
        else if(Integer.parseInt(etChapterRead.getText().toString())>Integer.parseInt(etChapterReleased.getText().toString())){
            b = false;
            Toast.makeText(this, "Read chapter cannot be larger than released chapter", Toast.LENGTH_SHORT).show();
        }
        else if(Integer.parseInt(etVolumeRead.getText().toString())>Integer.parseInt(etVolumeReleased.getText().toString())){
            b = false;
            Toast.makeText(this, "Read volume cannot be larger than released volume", Toast.LENGTH_SHORT).show();
        }
        return b;
    }


    public void coverAlert(){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("URL");
        alert.setMessage("Enter the url for cover file:");

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();
                setCover(value);
                coverChanged = true;

            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();
    }

    public void setCover(String url){
        final ImageLoader imageLoader = ImageLoader.getInstance();

        // Load image, decode it to Bitmap and display Bitmap in ImageView (or any other view
        //  which implements ImageAware interface)
        imageLoader.displayImage(url, ivCover, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                Toast.makeText(getBaseContext(), "Loading Cover Started", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                Toast.makeText(getBaseContext(), "Loading Cover Failed:" + failReason.getCause(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                Toast.makeText(getBaseContext(), "Loading Cover Complete", Toast.LENGTH_SHORT).show();
                cover = loadedImage;
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                Toast.makeText(getBaseContext(), "Loading Cover Canceled", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void saveImage(int id) {

        File check = new File(Environment.getExternalStorageDirectory(), "MankaKeeper");

        if(!check.mkdir()){
            Log.d("Directory doesn't exist", check.getAbsolutePath());

        }


        String path = Environment.getExternalStorageDirectory().toString()+"/MankaKeeper";
        OutputStream fOut = null;
        File file = new File(path, id +".jpg"); // the File to save to
        try {
            fOut = new FileOutputStream(file);
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }

        if (cover != null){
            //Bitmap pictureBitmap = getBitmapFromURL(imageUrl); // obtaining the Bitmap
            cover.compress(Bitmap.CompressFormat.JPEG, 85, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
            try {
                fOut.flush();
                fOut.close(); // do not forget to close the stream
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
                Log.d("Image saved:", "ID:" + id + ", Path:" + path);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void setWithName(String name, String site){
        final String id = nameToID(name);
        setWithID(id, site);
    }

    public void setWithID(String id, String site){

        etApiId.setText(id);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(endPoint).build();

        mangaapi manga = restAdapter.create(mangaapi.class);

        manga.getManga(site, id, new Callback<mangamodel>() {
            @Override
            public void success(mangamodel mangamodel, Response response) {
                //SET VIEWS WITH INFO


                if(mangamodel!=null) {

                    etTitle.setText(mangamodel.getName());
                    lvSearch.setVisibility(View.INVISIBLE);

                    String authors = "";
                    for (int i = 0; i < mangamodel.getAuthor().size(); i++) {
                        if(mangamodel.getAuthor().size()>0) {
                            authors += idToName(mangamodel.getAuthor().get(i));
                            if (i != mangamodel.getAuthor().size() - 1)
                                authors += ",";
                        }
                    }

                    etAuthor.setText(authors);

                    etDetail.setText(mangamodel.getInfo());

                    coverChanged = true;
                    setCover(mangamodel.getCover());

                    etChapterReleased.setText("" + mangamodel.getChapters().get(mangamodel.getChapters().size() - 1).getChapterId().intValue());


                    List<String> genres = mangamodel.getGenres();

                    setCheckboxes(genres);

                }
                else
                    Toast.makeText(getBaseContext(), "Error:Check your title or try other site", Toast.LENGTH_LONG).show();

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getBaseContext(), "Error:Check your title or try other site", Toast.LENGTH_LONG).show();
                Log.d("Error:", error.getMessage());
            }
        });
    }



    public void setCheckboxes(List<String> genres){
        if(genres.contains("action")||genres.contains(Manga.getGenre(0)))
            cbAction.setChecked(true);
        if(genres.contains("adult")||genres.contains(Manga.getGenre(1)))
            cbAdult.setChecked(true);
        if(genres.contains("adventure")||genres.contains(Manga.getGenre(2)))
            cbAdventure.setChecked(true);
        if(genres.contains("comedy")||genres.contains(Manga.getGenre(3)))
            cbComedy.setChecked(true);
        if(genres.contains("doujinshi")||genres.contains(Manga.getGenre(4)))
            cbDouhinshi.setChecked(true);
        if(genres.contains("drama")||genres.contains(Manga.getGenre(5)))
            cbDrama.setChecked(true);
        if(genres.contains("ecchi")||genres.contains(Manga.getGenre(6)))
            cbEcchi.setChecked(true);
        if(genres.contains("fantasy")||genres.contains(Manga.getGenre(7)))
            cbFantasy.setChecked(true);
        if(genres.contains("gender-bender")||genres.contains(Manga.getGenre(8)))
            cbGenderBender.setChecked(true);
        if(genres.contains("harem")||genres.contains(Manga.getGenre(9)))
            cbHarem.setChecked(true);
        if(genres.contains("historical")||genres.contains(Manga.getGenre(10)))
            cbHistorical.setChecked(true);
        if(genres.contains("horror")||genres.contains(Manga.getGenre(11)))
            cbHorror.setChecked(true);
        if(genres.contains("josei")||genres.contains(Manga.getGenre(12)))
            cbJosei.setChecked(true);
        if(genres.contains("martial-arts")||genres.contains(Manga.getGenre(13)))
            cbMartialArts.setChecked(true);
        if(genres.contains("mature")||genres.contains(Manga.getGenre(14)))
            cbMature.setChecked(true);
        if(genres.contains("mecha")||genres.contains(Manga.getGenre(15)))
            cbMecha.setChecked(true);
        if(genres.contains("mystery")||genres.contains(Manga.getGenre(16)))
            cbMystery.setChecked(true);
        if(genres.contains("one-shot")||genres.contains(Manga.getGenre(17)))
            cbOneSHot.setChecked(true);
        if(genres.contains("psychological")||genres.contains(Manga.getGenre(18)))
            cbPsychological.setChecked(true);
        if(genres.contains("romance")||genres.contains(Manga.getGenre(19)))
            cbRomance.setChecked(true);
        if(genres.contains("school-life")||genres.contains(Manga.getGenre(20)))
            cbSchoolLife.setChecked(true);
        if(genres.contains("sci-fi")||genres.contains(Manga.getGenre(21)))
            cbSciFi.setChecked(true);
        if(genres.contains("seinen")||genres.contains(Manga.getGenre(22)))
            cbSeinen.setChecked(true);
        if(genres.contains("shoujo")||genres.contains(Manga.getGenre(23)))
            cbShoujo.setChecked(true);
        if(genres.contains("shoujoai")||genres.contains(Manga.getGenre(24)))
            cbShoujoAi.setChecked(true);
        if(genres.contains("shounen")||genres.contains(Manga.getGenre(25)))
            cbShouen.setChecked(true);
        if(genres.contains("shounenai")||genres.contains(Manga.getGenre(26)))
            cbShounenAi.setChecked(true);
        if(genres.contains("slice-of-life")||genres.contains(Manga.getGenre(27)))
            cbSliceOfLife.setChecked(true);
        if(genres.contains("smut")||genres.contains(Manga.getGenre(28)))
            cbSmut.setChecked(true);
        if(genres.contains("sports")||genres.contains(Manga.getGenre(29)))
            cbSports.setChecked(true);
        if(genres.contains("supernatural")||genres.contains(Manga.getGenre(30)))
            cbSupernatural.setChecked(true);
        if(genres.contains("tragedy")||genres.contains(Manga.getGenre(31)))
            cbTragedy.setChecked(true);
        if(genres.contains("webtoons")||genres.contains(Manga.getGenre(32)))
            cbWebtoons.setChecked(true);
        if(genres.contains("yaoi")||genres.contains(Manga.getGenre(33)))
            cbYaoi.setChecked(true);
        if(genres.contains("yuri")||genres.contains(Manga.getGenre(34)))
            cbYuri.setChecked(true);

    }


    public static String nameToID(String name){
        String s = name;

        s = s.toLowerCase();

        s = s.replace(" ", "-");
        s = s.replace("(", "-");
        s = s.replace(")", "-");
        s = s.replace(".", "-");
        s = s.replace("?", "-");
        s = s.replace("!", "-");
        s = s.replace(":", "-");
        s = s.replace("'", "-");




        //s = s.replace("--", "-");
        //s = s.replace("---", "-");
        //s = s.replace("----", "-");


        if((s.charAt(s.length() - 1))=="-".charAt(0))
            s = s.substring(0,s.length() - 1);


        return s;
    }

    public static String idToName(String id){
        String s = id;

        if(id!=""||id!=null) {
            String[] arr = s.split("-");
            s = "";

            for (int i = 0; i < arr.length; i++) {
                String b = Character.toUpperCase(arr[i].charAt(0)) + arr[i].substring(1);
                s += b + " ";
            }
            if(s!="")
                s = s.substring(0, s.length() - 1);
        }

        return s;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_manga, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id==android.R.id.home) {
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            finish();
        }

        return super.onOptionsItemSelected(item);
    }



}
