package com.endless.hov.mangakeeper;

/**
 * Created by Barış on 10.12.2015.
 */
public class SomeFunctions {


    public static   String apiToScraper(String id){
        String s = id;
        s = s.replace("-", "_");
        s = s.replace("___", "_");
        s = s.replace("__", "_");

        Character last = s.charAt(s.length()-1);
        while(last=="_".charAt(0)){
            s = s.substring(0, s.length()-1);
            last = s.charAt(s.length()-1);
        }

        Character first = s.charAt(0);
        while(first=="_".charAt(0)){
            s = s.substring(1, s.length());
            first = s.charAt(0);
        }
        return s;
    }
}
