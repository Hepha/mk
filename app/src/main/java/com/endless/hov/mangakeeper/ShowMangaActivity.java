package com.endless.hov.mangakeeper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.endless.hov.mangakeeper.API.mangaapi;
import com.endless.hov.mangakeeper.CustomAdapters.HistoryAdapter;
import com.endless.hov.mangakeeper.Scraper.Scraper;
import com.endless.hov.mangakeeper.Scraper.ScraperChapter;
import com.endless.hov.mangakeeper.Scraper.ScraperManga;
import com.endless.hov.mangakeeper.model.Chapter;
import com.endless.hov.mangakeeper.model.mangamodel;
import com.melnykov.fab.FloatingActionButton;
import com.melnykov.fab.ObservableScrollView;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ShowMangaActivity extends ActionBarActivity implements View.OnClickListener, View.OnLongClickListener {


    ImageView ivShowCover;

    TextView tvShowTitle, tvShowAuthor, tvShowUpdate, tvShowDetail, tvShowAdd, tvStatus;

    EditText etShowRead, etShowReleased;

    Button btnReadMinus, btnReadPlus, btnReleasedMinues, btnReleasedPlus, btnShowNotes, btnLoadHistory;

    NonScrollListView lvShowNotes, lvHistory;

    ProgressBar progHistory;

    ObservableScrollView scrollView;

    FloatingActionButton fab, fab2, fabRef, fabFav, fabDel, fabEdit;

    LinearLayout llGenres;

    RelativeLayout rlExtra;


    final String coverPath = Environment.getExternalStorageDirectory().toString() + "/MankaKeeper/";

    final String endPoint = "https://doodle-manga-scraper.p.mashape.com";

    String siteid;

    Manga selected;

    int read, released;

    HistoryAdapter historyAdapter;

    List<HistoryEntry> history, historyList;


    SharedPreferences settings, settingsP;

    final String pref_name = "PREF_NAME";
    final String pref_site = "apiList";
    final String pref_sort = "SORT";
    final String pref_status = "STATUS";
    final String pref_frag = "FRAG";


    //
    Boolean isFabOpen = false;

    FloatingActionButton[] fabList;


    boolean isAPI = true;


    //
    public static Context appContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_manga);

        appContext = getApplicationContext();

        settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        if (!settings.getString("callSystem", "API").equals("API")) isAPI = false;

        rlExtra = (RelativeLayout)findViewById(R.id.rlExtra);

        int id = getIntent().getExtras().getInt("ID");
        MangaDatabaseHandler db = new MangaDatabaseHandler(this);
        selected = db.getManga(id);

        history = db.getMangaHistory(selected.getId(), selected.getTitle());

        historyList = new ArrayList<>();

        int historyAmount = Integer.parseInt(settings.getString("historyAmount", "25"));
        if (historyAmount >= history.size() || historyAmount == 0) {
            historyAmount = history.size();
            rlExtra.setVisibility(View.GONE);
            historyList.addAll(history);
        }else{
            historyList.addAll(history.subList(0, historyAmount));
        }


        historyAdapter = new HistoryAdapter(getBaseContext(), R.layout.history_adapter, historyList);

        db.close();

        intializeCompenets();

        //auto refresh
        boolean auto = settings.getBoolean("autoRefresh", false);
        if (auto)
            refreshReleased();
    }


    public void intializeCompenets() {

        //PREF
        settingsP = getSharedPreferences(pref_name, MODE_PRIVATE);
        siteid = settingsP.getString(pref_site, "mangafox.me");

        //
        getSupportActionBar().setTitle(selected.getTitle());

        read = selected.getReadChapter();
        released = selected.getReleasedChapter();

        //SET IMAGEVIEWS
        ivShowCover = (ImageView) findViewById(R.id.ivShowCover);
        File imgFile = new File(coverPath + selected.getId() + ".jpg");
        if (imgFile.exists()) {
            Bitmap bmp = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            ivShowCover.setImageBitmap(bmp);
        } else
            ivShowCover.setBackgroundColor(Color.WHITE);

        //SET TEXTVIEWS
        tvShowTitle = (TextView) findViewById(R.id.tvShowTitle);
        tvShowTitle.setText(selected.getTitle());

        tvShowAuthor = (TextView) findViewById(R.id.tvShowAuthor);
        tvShowAuthor.setText(selected.getAuthor());
        tvShowAuthor.setOnClickListener(this);

        tvShowUpdate = (TextView) findViewById(R.id.tvShowUpdate);
        tvShowUpdate.setText(selected.getUpdateTimeString());
        tvShowDetail = (TextView) findViewById(R.id.tvShowDetail);
        tvShowDetail.setText(selected.getDetail());
        /*Old genre way, can be deleted
        tvShowGenres = (TextView) findViewById(R.id.tvShowGenres);
        String g = "";
        for (int i = 0; i < selected.getGenres().size(); i++) {
            g += selected.getGenres().get(i);
            if (i != selected.getGenres().size() - 1)
                g += ",";
        }
        tvShowGenres.setText(g);
        */
        tvShowAdd = (TextView) findViewById(R.id.tvShowAdd);
        tvShowAdd.setText(selected.getAddTimeString());

        tvShowUpdate.setOnLongClickListener(this);
        tvShowAdd.setOnLongClickListener(this);

        tvStatus = (TextView) findViewById(R.id.tvStatus);
        tvStatus.setText(selected.getStatusAsString());

        //SET LINEAR LAYOUTS
        //genres
        llGenres = (LinearLayout)findViewById(R.id.llGenres);
        for(String s : selected.getGenres()){
            TextView tvGenre = new TextView(this);
            tvGenre.setText(s);
            tvGenre.setTextColor(Color.BLUE);
            tvGenre.setTextSize(18);

            tvGenre.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView tv = (TextView)v;
                    Intent i = new Intent(getAppContext(), MainActivity.class);

                    i.putExtra("frag", "search");
                    i.putExtra("searchType", "genre");
                    i.putExtra("searchValue", tv.getText());
                    startActivity(i);
                }
            });

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.rightMargin = 20;
            tvGenre.setLayoutParams(lp);
            llGenres.addView(tvGenre);
        }


        //SET EDITTEXTS
        etShowRead = (EditText) findViewById(R.id.etShowRead);
        etShowRead.setText("" + selected.getReadChapter());
        etShowReleased = (EditText) findViewById(R.id.etShowReleased);
        etShowReleased.setText("" + selected.getReleasedChapter());

        etShowRead.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etShowRead.getText().toString().length() == 0)
                    etShowRead.setText("0");
                else {
                    if (!etShowRead.getText().toString().equals("0") && etShowRead.getText().toString().substring(0, 1).equals("0"))
                        etShowRead.setText(etShowRead.getText().toString().substring(1));
                }
                read = Integer.parseInt(etShowRead.getText().toString());

            }
        });
        etShowReleased.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etShowReleased.getText().toString().length() == 0)
                    etShowReleased.setText("0");
                else {
                    if (!etShowReleased.getText().toString().equals("0") && etShowReleased.getText().toString().substring(0, 1).equals("0"))
                        etShowReleased.setText(etShowReleased.getText().toString().substring(1));
                }
                released = Integer.parseInt(etShowReleased.getText().toString());

            }
        });

        //SET BUTTONS
        btnReadMinus = (Button) findViewById(R.id.btnReadMinus);
        btnReadMinus.setOnClickListener(this);
        btnReadPlus = (Button) findViewById(R.id.btnReadPlus);
        btnReadPlus.setOnClickListener(this);
        btnReleasedMinues = (Button) findViewById(R.id.btnReleaseddMinus);
        btnReleasedMinues.setOnClickListener(this);
        btnReleasedPlus = (Button) findViewById(R.id.btnReleasedPlus);
        btnReleasedPlus.setOnClickListener(this);
        btnShowNotes = (Button) findViewById(R.id.btnShowNotes);
        btnShowNotes.setOnClickListener(this);

        //SET LISTVIEWS
        lvShowNotes = (NonScrollListView) findViewById(R.id.lvShowNotes);
        registerForContextMenu(lvShowNotes);
        refreshNotes();

        lvHistory = (NonScrollListView) findViewById(R.id.lvHistory);
        lvHistory.setAdapter(historyAdapter);
        lvHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long time = historyAdapter.getItem(position).getTime();
                Date date = new Date(time);
                DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy - HH:mm");
                String dateFormatted = formatter.format(date);
                Toast.makeText(getBaseContext(), dateFormatted, Toast.LENGTH_SHORT).show();
            }
        });


        //SET FAB
        scrollView = (ObservableScrollView) findViewById(R.id.scrollView);
        scrollView.setOnScrollChangedListener(new ObservableScrollView.OnScrollChangedListener() {
            @Override
            public void onScrollChanged(ScrollView who, int l, int t, int oldl, int oldt) {
                if (isFabOpen) {
                    animateFABClose(0);
                    isFabOpen = false;
                }
            }
        });
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.attachToScrollView(scrollView);

        fab.setOnClickListener(this);

        fabRef = (FloatingActionButton) findViewById(R.id.fabRef);
        fabRef.setOnClickListener(this);
        fabFav = (FloatingActionButton) findViewById(R.id.fabFav);
        fabFav.setOnClickListener(this);
        fabDel = (FloatingActionButton) findViewById(R.id.fabDel);
        fabDel.setOnClickListener(this);
        fabEdit = (FloatingActionButton) findViewById(R.id.fabEdit);
        fabEdit.setOnClickListener(this);
        fabList = new FloatingActionButton[]{fabRef, fabFav, fabDel, fabEdit};

        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        //fab2.attachToScrollView(scrollView);
        fab2.setOnClickListener(this);

        if (selected.getFavoriteBool()) {
            fabFav.setColorNormal(getResources().getColor(R.color.red));
        }


        //EXTRA HISTORY PART
        btnLoadHistory = (Button) findViewById(R.id.btnLoadHistory);
        progHistory = (ProgressBar) findViewById(R.id.progHistory);
        if (historyAdapter.getCount() == history.size())
            btnLoadHistory.setVisibility(View.GONE);
        btnLoadHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progHistory.setVisibility(View.VISIBLE);
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        /*
                        int historyAmount = historyAdapter.getCount();
                        final List<String> history = new ArrayList<String>();
                        for (int i = historyAmount; i < selected.historyOuput().size(); i++)
                            history.add(selected.historyOuput().get(selected.historyOuput().size() - 1 - i));
                         */
                        historyList.clear();
                        historyList.addAll(history);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                historyAdapter.notifyDataSetChanged();
                                progHistory.setVisibility(View.GONE);
                                rlExtra.setVisibility(View.GONE);
                            }
                        });

                    }
                };
                new Thread(runnable).start();
            }
        });

    }

    public void animateFAB() {

        if (isFabOpen) {
            isFabOpen = false;
            animateFABClose(0);
        } else {
            isFabOpen = true;
            animateFABOpen(0);
        }
    }

    public void animateFABOpen(final int count) {
        Animation openAnim = AnimationUtils.loadAnimation(this, R.anim.fab_open);
        fabList[count].startAnimation(openAnim);
        openAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (count != fabList.length - 1)
                    animateFABOpen(count + 1);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fabList[count].setClickable(true);
    }

    public void animateFABClose(final int count) {
        Animation closeAnim = AnimationUtils.loadAnimation(this, R.anim.fab_close);
        fabList[fabList.length - count - 1].startAnimation(closeAnim);
        closeAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (count != fabList.length - 1)
                    animateFABClose(count + 1);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fabList[count].setClickable(false);
    }


    public void refreshNotes() {
        lvShowNotes.setAdapter(new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, selected.getNotes()));
    }

    public void refreshReleased() {
        Log.d("Called", "refresh");

        if (isAPI) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(endPoint).build();

            mangaapi manga = restAdapter.create(mangaapi.class);

            if (!selected.getApiID().equals("")) {
                manga.getManga(siteid, selected.getApiID(), new Callback<mangamodel>() {
                    @Override
                    public void success(mangamodel mangamodel, Response response) {
                        if (mangamodel != null) {
                            int newReleased = mangamodel.getChapters().get(mangamodel.getChapters().size() - 1).getChapterId().intValue();
                            if (newReleased > released) {
                                Toast.makeText(getBaseContext(), "Chapters released: " + String.valueOf(newReleased - released), Toast.LENGTH_SHORT).show();
                                released = newReleased;
                                etShowReleased.setText("" + released);
                                update();
                            } else
                                Toast.makeText(getBaseContext(), "No new chapters", Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(getBaseContext(), "Couldn't find any manga with this api id:" + selected.getApiID(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getBaseContext(), "There was a problem with api call", Toast.LENGTH_SHORT).show();
                    }

                });
            } else
                Toast.makeText(getBaseContext(), "This manga doesn't have an api id", Toast.LENGTH_SHORT).show();
        } else {
            if (!selected.getApiID().equals("")) {
                final String scrapId = SomeFunctions.apiToScraper(selected.getApiID());
                Log.d("Refresh from", "Scraper-" + scrapId);
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            ScraperManga manga = Scraper.getManga(scrapId);
                            if (manga != null) {
                                final int newReleased = (int)Double.parseDouble(manga.getChapters().get(0).getChapterId());
                                if (newReleased > released) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ShowMangaActivity.this, "Chapters released: " + String.valueOf(newReleased - released), Toast.LENGTH_SHORT).show();
                                            released = newReleased;
                                            etShowReleased.setText("" + released);
                                            update();
                                        }
                                    });

                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Log.d("NewReleased", "" + newReleased);
                                            Toast.makeText(ShowMangaActivity.this, "No new chapters", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(ShowMangaActivity.this, "Couldn't connect to website", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }
                };
                new Thread(runnable).start();
            } else
                Toast.makeText(getBaseContext(), "This manga doesn't have an api id", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnReadMinus:
                if (read > 0)
                    read -= 1;
                etShowRead.setText("" + read);
                break;
            case R.id.btnReadPlus:
                if (read < released)
                    read += 1;
                else if (read == released) {
                    read += 1;
                    released += 1;
                }
                etShowRead.setText("" + read);
                etShowReleased.setText("" + released);
                break;
            case R.id.btnReleaseddMinus:
                if (read < released)
                    released -= 1;
                else {
                    released -= 1;
                    read -= 1;
                    etShowRead.setText("" + read);
                }
                etShowReleased.setText("" + released);
                break;
            case R.id.btnReleasedPlus:
                released += 1;
                etShowReleased.setText("" + released);
                break;
            case R.id.btnShowNotes:
                AlertDialog.Builder alert = new AlertDialog.Builder(this);

                alert.setTitle("Add Note");
                alert.setMessage("Enter your note:");

                // Set an EditText view to get user input
                final EditText input = new EditText(this);
                alert.setView(input);

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String value = input.getText().toString();
                        addNote(value);
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });

                alert.show();
                break;

            case R.id.fab:
                animateFAB();
                break;
            case R.id.fab2:
                readChapters();
                break;
            case R.id.fabFav:
                MangaDatabaseHandler db = new MangaDatabaseHandler(getBaseContext());
                if (selected.getFavoriteBool()) {
                    selected.setFavorite(false);
                    fabFav.setColorNormal(getResources().getColor(R.color.blue));
                    db.updateManga(selected, getBaseContext());
                    Toast.makeText(getBaseContext(), "Manga Unfavorited", Toast.LENGTH_SHORT).show();

                } else {
                    selected.setFavorite(true);
                    fabFav.setColorNormal(getResources().getColor(R.color.red));
                    db.updateManga(selected, getBaseContext());
                    Toast.makeText(getBaseContext(), "Manga Favorited", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.fabRef:
                refreshReleased();
                break;
            case R.id.fabDel:
                deleteManga();
                animateFAB();
                break;
            case R.id.fabEdit:
                animateFAB();
                Intent e = new Intent(getBaseContext(), AddMangaActivity.class);
                e.putExtra("ID", selected.getId());
                startActivity(e);
                break;
            case R.id.tvShowAuthor:
                Intent i = new Intent(getAppContext(), MainActivity.class);

                i.putExtra("frag", "search");
                i.putExtra("searchType", "entry");
                i.putExtra("searchValue", tvShowAuthor.getText());
                startActivity(i);
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {

        if (v == tvShowAdd) {
            long date = selected.getAddTime();
            long difference = System.currentTimeMillis() - date;

            String time = "back at " + selected.getAddTimeString();
            if (difference <= (1000 * 60)) {
                int sec = Math.round(difference / 1000);
                time = sec + " seconds ago";
            } else if (difference <= (1000 * 60 * 60)) {
                int min = Math.round((difference / (1000 * 60)));
                time = min + " minutes ago";
            } else if (difference <= 1000 * 60 * 60 * 24) {
                int hour = Math.round((difference / (1000 * 60 * 60)));
                time = hour + " hours ago";
            } else {
                int day = Math.round((difference / (1000 * 60 * 60 * 24)));
                time = day + " days ago";
            }

            Toast.makeText(this, "This was " + time, Toast.LENGTH_SHORT).show();
        } else if (v == tvShowUpdate) {
            long date = selected.getUpdateTime();
            long difference = System.currentTimeMillis() - date;

            String time = "back at " + selected.getUpdateTimeString();
            if (difference <= (1000 * 60)) {
                int sec = Math.round(difference / 1000);
                time = sec + " seconds ago";
            } else if (difference <= (1000 * 60 * 60)) {
                int min = Math.round((difference / (1000 * 60)));
                time = min + " minutes ago";
            } else if (difference <= 1000 * 60 * 60 * 24) {
                int hour = Math.round((difference / (1000 * 60 * 60)));
                time = hour + " hours ago";
            } else {
                int day = Math.round((difference / (1000 * 60 * 60 * 24)));
                time = day + " days ago";
            }
            Toast.makeText(this, "This was " + time, Toast.LENGTH_SHORT).show();
        }

        return false;
    }


    @Override
    protected void onPause() {
        super.onPause();

        update();

    }

    public void readChapters() {
        final ListView listView = new ListView(this);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        adapter.add("Wait until chapters load...");
        listView.setAdapter(adapter);
        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(listView).create();
        dialog.show();


        if (isAPI) {
            RestAdapter ra = new RestAdapter.Builder().setEndpoint(endPoint).build();
            mangaapi m = ra.create(mangaapi.class);
            m.getManga("mangareader.net", selected.getApiID(), new Callback<mangamodel>() {
                @Override
                public void success(final mangamodel mangamodel, Response response) {
                    if (mangamodel != null) {
                        final List<Chapter> chapters = mangamodel.getChapters();
                        Collections.reverse(chapters);
                        String[] arr = new String[mangamodel.getChapters().size()];
                        for (int i = 0; i < arr.length; i++) {
                            arr[i] = "" + mangamodel.getChapters().get(i).getChapterId();
                            if (mangamodel.getChapters().get(i).getName() != null)
                                arr[i] += "-" + mangamodel.getChapters().get(i).getName();
                        }
                        adapter.clear();
                        adapter.addAll(arr);
                        adapter.notifyDataSetChanged();
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent i = new Intent(getBaseContext(), ReadChapterActivity.class);
                                i.putExtra("mangaid", selected.getApiID());
                                i.putExtra("currentChapter", position);
                                i.putExtra("title", selected.getTitle());
                                String[] ch = new String[chapters.size()];
                                for (int j = 0; j < ch.length; j++)
                                    ch[j] = "" + chapters.get(j).getChapterId();
                                i.putExtra("chapterids", ch);
                                startActivity(i);
                            }
                        });
                    } else {
                        Toast.makeText(getBaseContext(), "This manga doesn't exist on mangareader.net", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getBaseContext(), "Couldn't find any chapters", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });
        } else {
            if (!selected.getApiID().equals("")) {
                final String scrapId = SomeFunctions.apiToScraper(selected.getApiID());
                Log.d("Loading from", "Scraper-" + scrapId);
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            final List<ScraperChapter> chapters = Scraper.getManga(scrapId).getChapters();
                            String[] list = new String[chapters.size()];
                            for (int i = 0; i < list.length; i++) {
                                list[i] = chapters.get(i).toString();
                            }
                            final String[] arr = list;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.clear();
                                    adapter.addAll(arr);
                                    adapter.notifyDataSetChanged();
                                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                            Intent i = new Intent(getBaseContext(), ReadChapterActivity.class);
                                            i.putExtra("mangaid", scrapId);
                                            i.putExtra("currentChapter", position);
                                            i.putExtra("title", selected.getTitle());
                                            String[] ch = new String[chapters.size()];
                                            for (int j = 0; j < ch.length; j++)
                                                ch[j] = "" + chapters.get(j).getChapterId();
                                            i.putExtra("chapterids", ch);
                                            String[] vo = new String[chapters.size()];
                                            for (int j = 0; j < ch.length; j++)
                                                vo[j] = "" + chapters.get(j).getVolume().getTitle();
                                            i.putExtra("volumeids", ch);

                                            startActivity(i);
                                        }
                                    });
                                }
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                };
                new Thread(runnable).start();
            } else {
                Toast.makeText(getBaseContext(), "This manga doesn't have an api id", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        }

    }


    public void addNote(String value) {
        MangaDatabaseHandler db = new MangaDatabaseHandler(getBaseContext());

        List<String> notes = selected.getNotes();
        Log.d("notes:", selected.getNotesAsString());
        notes.add(value);
        selected.setNotes(notes);
        db.updateManga(selected, getBaseContext());
        refreshNotes();
    }

    public void update() {
        MangaDatabaseHandler db = new MangaDatabaseHandler(getBaseContext());

        Boolean b1 = selected.getReadChapter() != read;
        Boolean b2 = selected.getReleasedChapter() != released;

        if (b1 || b2) {
            if (read <= released) {
                Log.d("Gotta update:", selected.getTitle());
                selected.setReadChapter(read);
                selected.setReleasedChapter(released);
                db.updateManga(selected, getBaseContext());
            } else
                Toast.makeText(this, "Read chapters can't be higher than released chapters", Toast.LENGTH_SHORT).show();
        }
    }


    public void editNote(final int p) {

        final MangaDatabaseHandler db = new MangaDatabaseHandler(getBaseContext());

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Edit Note");
        alert.setMessage("Enter your note:");

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        input.setText(selected.getNotes().get(p));
        alert.setView(input);


        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();
                List<String> notes = selected.getNotes();
                notes.set(p, value);
                selected.setNotes(notes);
                db.updateManga(selected, getBaseContext());
                refreshNotes();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();
    }

    public void deleteNote(int p) {
        MangaDatabaseHandler db = new MangaDatabaseHandler(getBaseContext());
        List<String> notes = selected.getNotes();
        notes.remove(p);
        selected.setNotes(notes);
        db.updateManga(selected, getBaseContext());
        refreshNotes();
    }

    public void deleteManga() {
        new AlertDialog.Builder(this)
                .setTitle("Delete manga")
                .setMessage("Are you sure you want to delete this manga?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MangaDatabaseHandler db = new MangaDatabaseHandler(getBaseContext());
                        db.deleteManga(selected);
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_manga, menu);

        /*MenuItem favorite = menu.findItem(R.id.action_favorite);

        if(selected.getFavoriteBool())
            favorite.setIcon(getResources().getDrawable(R.drawable.ic_action_unfavorite));
        else
            favorite.setIcon(getResources().getDrawable(R.drawable.ic_action_favorite));*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        final int id = item.getItemId();

        final MangaDatabaseHandler db = new MangaDatabaseHandler(getBaseContext());


        /*Using floating action button instead
        if (id == R.id.action_favorite) {
            if(selected.getFavoriteBool()) {
                selected.setFavorite(false);
                item.setIcon(getResources().getDrawable(R.drawable.ic_action_favorite));
                db.updateManga(selected, getBaseContext());
                Toast.makeText(getBaseContext(), "Manga Unfavorited", Toast.LENGTH_SHORT).show();

            }else{
                selected.setFavorite(true);
                item.setIcon(getResources().getDrawable(R.drawable.ic_action_unfavorite));
                db.updateManga(selected, getBaseContext());
                Toast.makeText(getBaseContext(), "Manga Favorited", Toast.LENGTH_SHORT).show();
            }

            return true;
        }

        if(id == R.id.action_discard){
            deleteManga();
        }
        if(id == R.id.action_edit){
            Intent e = new Intent(getBaseContext(), AddMangaActivity.class);
            e.putExtra("ID", selected.getId());
            startActivity(e);
        }
        if(id == R.id.action_refresh){
            refreshReleased();
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.lvShowNotes) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            menu.setHeaderTitle("Notes");
            String[] menuItems = getResources().getStringArray(R.array.notes_context);
            for (int i = 0; i < menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.notes_context);

        if (menuItems[menuItemIndex].equals("Edit")) {
            editNote(info.position);
        } else if (menuItems[menuItemIndex].equals("Delete")) {
            deleteNote(info.position);
        }

        return true;
    }

    public static Context getAppContext() {
        return appContext;
    }
}
