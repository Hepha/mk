package com.endless.hov.mangakeeper.CustomAdapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.endless.hov.mangakeeper.MangaDatabaseHandler;
import com.endless.hov.mangakeeper.R;
import com.endless.hov.mangakeeper.ShowMangaActivity;
import com.endless.hov.mangakeeper.ShowOnlineActivity;
import com.endless.hov.mangakeeper.model.searchmodel;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Barış on 12.11.2015.
 */
public class ListCoverAdapter extends ArrayAdapter<searchmodel> {
    Bitmap[] bitmaps;

    public ListCoverAdapter(Context context, int resource) {
        super(context, resource);
    }

    public ListCoverAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public ListCoverAdapter(Context context, int resource, searchmodel[] objects) {
        super(context, resource, objects);
    }

    public ListCoverAdapter(Context context, int resource, List<searchmodel> objects) {
        super(context, resource, objects);
    }

    public ListCoverAdapter(Context context, int resource, int textViewResourceId, searchmodel[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public void setSize(int size){
        bitmaps = new Bitmap[size];
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        View v = convertView;

        if(v==null){
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_cover_adapter, null);
        }

        ImageView iv = (ImageView)v.findViewById(R.id.imageView);
        TextView tvTitle = (TextView)v.findViewById(R.id.tvTitle);
        TextView tvGenres = (TextView)v.findViewById(R.id.tvGenres);
        final RelativeLayout loading = (RelativeLayout)v.findViewById(R.id.loadingPanel);

        final searchmodel m = getItem(position);

        if(iv!=null){
            if(bitmaps[position]!=null){
                iv.setImageBitmap(bitmaps[position]);
            }
            else
            {
                final ImageLoader imageLoader = ImageLoader.getInstance();

                // Load image, decode it to Bitmap and display Bitmap in ImageView (or any other view
                //  which implements ImageAware interface)
                imageLoader.displayImage(m.getCover(), iv, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        Log.d("loading cover failed", failReason.toString());
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        bitmaps[position] = loadedImage;
                        loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                    }
                });
            }

        }
        if(tvTitle!=null)
            tvTitle.setText(m.getName());
        if(tvGenres!=null){
            String s = "";
            for(int i = 0; i<m.getGenres().size(); i++){
                s += m.getGenres().get(i);
                if(i!=m.getGenres().size()-1)
                    s+=",";
            }
            tvGenres.setText(s);
        }

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MangaDatabaseHandler db = new MangaDatabaseHandler(getContext());
                int id = db.exitsMangaID(m.getMangaId());
                if (id==-1) {
                    Intent i = new Intent(getContext(), ShowOnlineActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("mangaId", m.getMangaId());
                    getContext().startActivity(i);
                } else {
                    Intent i = new Intent(getContext(), ShowMangaActivity.class);
                    i.putExtra("ID", id);
                    getContext().startActivity(i);
                }
            }
        });

        return v;
    }
}
