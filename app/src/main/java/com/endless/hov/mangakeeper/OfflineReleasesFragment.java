package com.endless.hov.mangakeeper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.endless.hov.mangakeeper.CustomAdapters.TemporaryAdapter;
import com.endless.hov.mangakeeper.Scraper.Scraper;
import com.endless.hov.mangakeeper.Scraper.ScraperManga;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OfflineReleasesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OfflineReleasesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OfflineReleasesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public OfflineReleasesFragment() {
        // Required empty public constructor
    }



    ListView lvReleases;
    TemporaryAdapter adapter;

    List<Manga> mangaList, updatedList;

    boolean destroyed = false;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OfflineReleasesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OfflineReleasesFragment newInstance(String param1, String param2) {
        OfflineReleasesFragment fragment = new OfflineReleasesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public void onStart() {
        super.onStart();

        adapter = new TemporaryAdapter(getActivity().getBaseContext(), R.layout.temporary, new ArrayList<Manga>());
        lvReleases = (ListView)getView().findViewById(R.id.lvReleases);
        lvReleases.setAdapter(adapter);
        lvReleases.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getActivity().getBaseContext(), ShowMangaActivity.class);
                i.putExtra("ID", updatedList.get(position).getId());
                startActivity(i);
            }
        });

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());

        MangaDatabaseHandler db = new MangaDatabaseHandler(getActivity().getBaseContext());
        if(settings.getString("offlineReleases", "fav").equals("fav"))
            mangaList = db.getFavorites();
        else
            mangaList = db.getAllMangas();

        checkMangas();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroyed = true;
    }

    public void checkMangas(){
        updatedList = new ArrayList<Manga>();

        Runnable run = new Runnable() {
            @Override
            public void run() {
                int i = 0;
                for(Manga m:mangaList){
                    if(!destroyed) {
                        if (!m.getApiID().equals("")) {
                            i++;
                            try {
                                Log.d("Checking", i + "-" + m.getApiID());
                                ScraperManga sm = Scraper.getManga(SomeFunctions.apiToScraper(m.getApiID()));
                                if (m.getReleasedChapter() < (int) Double.parseDouble(sm.getChapters().get(0).getChapterId())) {
                                    updatedList.add(m);
                                    Log.d("Updated", m.getReleasedChapter() + "-" + sm.getChapters().get(0).getChapterId());
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }else
                        break;
                }
                if(!destroyed) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (updatedList.size() > 0) {
                                adapter.addAll(updatedList);
                            } else
                                Toast.makeText(getActivity().getBaseContext(), "No Updated manga", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        };
        new Thread(run).start();




    }

    @Override
    public void onResume() {
        super.onResume();
        if(updatedList.size()>0)
            adapter.addAll(updatedList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_offline_releases, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
