package com.endless.hov.mangakeeper.Scraper;

import com.endless.hov.mangakeeper.Scraper.ScraperVolume;

/**
 * Created by Barış on 30.11.2015.
 */
public class ScraperChapter {
    private String title, chapterId, releaseDate;
    private ScraperVolume volume;

    public ScraperChapter(String title, ScraperVolume volume, String chapterId, String releaseDate) {
        this.title = title;
        this.volume = volume;
        this.chapterId = chapterId;
        this.releaseDate = releaseDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ScraperVolume getVolume() {
        return volume;
    }

    public void setVolume(ScraperVolume volume) {
        this.volume = volume;
    }

    public String getChapterId() {
        return chapterId;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    @Override
    public String toString(){
        return getChapterId() +"-"+getTitle();
    }
}
