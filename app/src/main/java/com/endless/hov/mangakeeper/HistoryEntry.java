package com.endless.hov.mangakeeper;

/**
 * Created by Hepha on 3.08.2017.
 */

public class HistoryEntry {

    private static String[] statusList = {"Reading", "Completed", "On Wait", "Plan To Read", "Hiatus"};

    private int mangaId;
    private String mangaTitle;
    private int type;
    private long time;
    private String data;

    public HistoryEntry(int mangaId, String mangaTitle, int type, long time, String data) {
        this.mangaId = mangaId;
        this.mangaTitle = mangaTitle;
        this.type = type;
        this.time = time;
        this.data = data;
    }

    public int getMangaId() {
        return mangaId;
    }

    public void setMangaId(int mangaId) {
        this.mangaId = mangaId;
    }

    public String getMangaTitle() {
        return mangaTitle;
    }

    public void setMangaTitle(String mangaTitle) {
        this.mangaTitle = mangaTitle;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getOutput(){
        String[] arr = data.split("==");
        switch (type){
            case 1:
                return "Manga Added!";
            case 2:
                int aoc = Integer.parseInt(arr[0]), start = Integer.parseInt(arr[1]), end = start+aoc-1;
                return aoc+" chapter read!("+start+"-"+end+")";
            case 3:
                aoc = Integer.parseInt(arr[0]);
                start = Integer.parseInt(arr[1]);
                end = start-aoc+1;
                return aoc+" chapter unread!("+start+"-"+end+")";
            case 4:
                aoc = Integer.parseInt(arr[0]);
                start = Integer.parseInt(arr[1]);
                end = start+aoc-1;
                return aoc+" chapter released!("+start+"-"+end+")";
            case 5:
                aoc = Integer.parseInt(arr[0]);
                start = Integer.parseInt(arr[1]);
                end = start-aoc+1;
                return aoc+" chapter unreleased!("+start+"-"+end+")";
            case 6:
                String old = arr[0], ne = arr[1];
                return "Title changed from "+old+" to "+ ne;
            case 7:
                old = arr[0];
                ne = arr[1];
                return "Author changed from "+old+" to "+ ne;
            case 8:
                return "Details changed!";
            case 9:
                return "Cover changed!";
            case 10:
                int from = Integer.parseInt(arr[0]), to = Integer.parseInt(arr[1]);
                return "Status changed from " + statusList[from] + " to ++" + statusList[to];
            case 11:
                String genre = arr[0];
                return "Genre added: "+ genre;
            case 12:
                genre = arr[0];
                return "Genre removed: "+ genre;
            case 13:
                String note = arr[0];
                return "Note added: " + note;
            case 14:
                note = arr[0];
                return "Note removed: " + note;
            case 15:
                return "Manga favorited!";
            case 16:
                return "Manga unfavorited!";
            default:
                return "";

        }
    }
}
