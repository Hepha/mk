package com.endless.hov.mangakeeper;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.endless.hov.mangakeeper.API.mangaapi;
import com.endless.hov.mangakeeper.Scraper.Scraper;
import com.endless.hov.mangakeeper.model.ChapterRead;
import com.endless.hov.mangakeeper.model.Page;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ReadChapterActivity extends AppCompatActivity implements View.OnClickListener {


    RelativeLayout loading;

    TextView tvPage, tvTitle, tvChapter;
    ImageButton ibBack, ibRefresh, ibPrev, ibNext;

    AppBarLayout appBar, bottomBar;

    CustomPagerAdapter mCustomPagerAdapter;
    ViewPager mViewPager;

    String[] chapters, volumes;
    String mangaid, mangaTitle;
    int curChapter;
    List<Page> pages = new ArrayList<>();

    boolean isAPI = true;

    boolean navHidden = false;

    int currentPage = 0;

    final String endPoint = "https://doodle-manga-scraper.p.mashape.com";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_chapter);

        //PREFS
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (!preferences.getString("callSystem", "API").equals("API")) isAPI = false;

        //TextView
        tvPage = (TextView) findViewById(R.id.tvPage);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvChapter = (TextView)findViewById(R.id.tvChapter);

        //AppBar
        appBar = (AppBarLayout) findViewById(R.id.appBar);
        bottomBar = (AppBarLayout) findViewById(R.id.bottomBar);

        //ImageButton
        ibBack = (ImageButton) findViewById(R.id.ibBack);
        ibBack.setOnClickListener(this);
        ibRefresh = (ImageButton) findViewById(R.id.ibRefresh);
        ibPrev = (ImageButton) findViewById(R.id.ibPrev);
        ibPrev.setOnClickListener(this);
        ibNext = (ImageButton) findViewById(R.id.ibNext);
        ibNext.setOnClickListener(this);


        //GET EXTRAS
        chapters = getIntent().getExtras().getStringArray("chapterids");
        mangaid = getIntent().getExtras().getString("mangaid");
        curChapter = getIntent().getExtras().getInt("currentChapter");
        mangaTitle = getIntent().getExtras().getString("title");
        if (!isAPI)
            volumes = getIntent().getExtras().getStringArray("volumeids");


        if (mangaTitle.length() < 15)
            tvTitle.setText(mangaTitle);
        else
            tvTitle.setText(mangaTitle.substring(0, 12) + "...");

        //ImageLoader
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);


        loadChapter();

    }

    public void loadChapter() {
        if (isAPI) loadChapterFromAPI();
        else {
            loadChapterFromScraper();
        }
    }

    public void loadChapterFromAPI() {
        RestAdapter ra = new RestAdapter.Builder()
                .setEndpoint(endPoint)
                .build();
        mangaapi m = ra.create(mangaapi.class);

        m.getChapter("mangareader.net", mangaid, chapters[curChapter], new Callback<ChapterRead>() {
            @Override
            public void success(ChapterRead chapterRead, Response response) {
                String message = "Chapter:" + chapters[curChapter];
                if (chapterRead.getName() != null)
                    message += chapterRead.getName();
                Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();
                pages = chapterRead.getPages();
                Log.d("Page amount", pages.size() + "");
                mCustomPagerAdapter = new CustomPagerAdapter(getBaseContext(), pages, tvPage, appBar, bottomBar, ibRefresh);

                tvPage.setText("1/" + pages.size());

                mViewPager = (ViewPager) findViewById(R.id.pager);
                mViewPager.setAdapter(mCustomPagerAdapter);
                mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        tvPage.setText(String.valueOf(position + 1) + "/" + pages.size());
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void loadChapterFromScraper() {
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    //List<String> list = new ArrayList<>();
                    try {
                        //list = Scraper.getChapterPagesWithStrings(mangaid, volumes[curChapter], chapters[curChapter]);
                        //pages.clear();

                        /*for(int i = 0;i<list.size();i++){
                            Page page = new Page();
                            page.setPageId(i);
                            page.setUrl(list.get(i));
                            pages.add(page);
                        }*/

                        final String[] info = Scraper.getFirstPageAndLimit(mangaid, volumes[curChapter], chapters[curChapter]);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //mCustomPagerAdapter = new CustomPagerAdapter(getBaseContext(), pages, tvPage, appBar, bottomBar, ibRefresh);
                                mCustomPagerAdapter = new CustomPagerAdapter(getBaseContext(), info[0], Integer.parseInt(info[1]),info[2], tvPage, appBar, bottomBar, ibRefresh);

                                tvPage.setText("1/" + info[1]);

                                tvChapter.setText("Chapter:"+chapters[curChapter]);

                                mViewPager = (ViewPager) findViewById(R.id.pager);
                                mViewPager.setAdapter(mCustomPagerAdapter);
                                mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                    @Override
                                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                                    }

                                    @Override
                                    public void onPageSelected(int position) {
                                        tvPage.setText(String.valueOf(position + 1) + "/" + info[1]);
                                    }

                                    @Override
                                    public void onPageScrollStateChanged(int state) {

                                    }
                                });
                            }
                        });
                    } catch (NullPointerException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getBaseContext(), "This chapter can't be viewed in this region!", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        });

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(run).start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mCustomPagerAdapter != null)
            mCustomPagerAdapter.setActivityPaused(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void onClick(View v) {
        if (v == ibBack) {
            mCustomPagerAdapter.setActivityPaused(true);
            finish();
        } else if (v == ibPrev) {
            if (curChapter < chapters.length - 1) {
                curChapter++;
                loadChapter();
            } else
                Toast.makeText(this, "No previous chapter", Toast.LENGTH_SHORT).show();
        } else if (v == ibNext) {
            if (curChapter > 0) {
                curChapter--;
                loadChapter();
            } else
                Toast.makeText(this, "No next chapter", Toast.LENGTH_SHORT).show();
        }
    }
}
