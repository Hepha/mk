package com.endless.hov.mangakeeper.model;

import com.google.gson.annotations.Expose;

public class Chapter {

    @Expose
    private Double chapterId;
    @Expose
    private String name;

    /**
     *
     * @return
     * The chapterId
     */
    public Double getChapterId() {
        return chapterId;
    }

    /**
     *
     * @param chapterId
     * The chapterId
     */
    public void setChapterId(Double chapterId) {
        this.chapterId = chapterId;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

}
