package com.endless.hov.mangakeeper.Scraper;

import java.util.List;

/**
 * Created by Barış on 30.11.2015.
 */
public class ScraperManga {
    String scrapId, title, cover, releasedYear, details, status;
    List<String> authors, artists, genres;
    List<ScraperChapter> chapters;

    public ScraperManga(String scrapId,String title, String cover, String releasedYear, String details, String status, List<String> authors, List<String> artists, List<String>  genres,List<ScraperChapter> chapters) {
        this.scrapId = scrapId;
        this.title = title;
        this.cover = cover;
        this.releasedYear = releasedYear;
        this.details = details;
        this.status = status;
        this.authors = authors;
        this.artists = artists;
        this.genres = genres;
        this.chapters = chapters;
    }

    public String getScrapId() {
        return scrapId;
    }

    public void setScrapId(String scrapId) {
        this.scrapId = scrapId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getReleasedYear() {
        return releasedYear;
    }

    public void setReleasedYear(String releasedYear) {
        this.releasedYear = releasedYear;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public List<String> getArtists() {
        return artists;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public void setArtists(List<String> artists) {
        this.artists = artists;
    }

    public List<ScraperChapter> getChapters() {
        return chapters;
    }

    public void setChapters(List<ScraperChapter> chapters) {
        this.chapters = chapters;
    }
}
