package com.endless.hov.mangakeeper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ListView;

import com.endless.hov.mangakeeper.CustomAdapters.TemporaryAdapter;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    EditText etSearchTitle;

    ListView lvSearch;

    CheckBox cbAction, cbAdult, cbAdventure, cbComedy, cbDouhinshi, cbDrama, cbEcchi, cbFantasy, cbGenderBender, cbHarem, cbHistorical,
            cbHorror, cbJosei, cbMartialArts, cbMature, cbMecha, cbMystery, cbOneSHot, cbPsychological, cbRomance, cbSchoolLife, cbSciFi, cbSeinen, cbShoujo,
            cbShoujoAi, cbShouen, cbShounenAi, cbSliceOfLife, cbSmut, cbSports, cbSupernatural, cbTragedy, cbWebtoons, cbYaoi, cbYuri,

            cbReading, cbCompleted, cbOnWait, cbPlanTo, cbHiatus;

    CheckBox[] cbList;
    CheckBox[] cbStatusList;

    HorizontalScrollView genresView, statusView;

    Button btnGenres, btnStatus;


    MangaDatabaseHandler db;

    List<Manga> listAll;
    ArrayList<Manga> listGot;

    boolean genres = false, statusB = false;




    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public void onStart() {
        super.onStart();

        init();

        Intent i = getActivity().getIntent();

        String searchType = i.getStringExtra("searchType");
        String searchValue = i.getStringExtra("searchValue");

        if(searchType != null && searchValue != null){
            if(searchType.equals("genre")){
                int genre = Manga.getGenreID(searchValue);

                cbList[genre].setChecked(true);
            }
            else if(searchType.equals("entry")){
                etSearchTitle.setText(searchValue);
            }
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        db.close();
    }


    public void init(){

        listGot = new ArrayList<Manga>();

        //DATABASE
        db = new MangaDatabaseHandler(getActivity().getBaseContext());
        listAll = db.getAllMangas();




        //SET LISTVIEWS
        lvSearch = (ListView)getView().findViewById(R.id.lvSearch);
        registerForContextMenu(lvSearch);


        //SET EDITTEXT
        etSearchTitle = (EditText)getView().findViewById(R.id.etSearchTitle);
        etSearchTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                    loadGotList(genres(), s.toString());
                    lvSearch.setAdapter(new TemporaryAdapter(getActivity().getBaseContext(), R.layout.temporary, listGot));
                    lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent i = new Intent(getActivity().getBaseContext(), ShowMangaActivity.class);
                            i.putExtra("ID", listGot.get(position).getId());
                            startActivity(i);
                        }
                    });

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        //SET CHECKBOXES
        cbAction = (CheckBox)getView().findViewById(R.id.cbAction);
        cbAdult = (CheckBox)getView().findViewById(R.id.cbAdult);
        cbAdventure = (CheckBox)getView().findViewById(R.id.cbAdventure);
        cbComedy = (CheckBox)getView().findViewById(R.id.cbComedy);
        cbDouhinshi = (CheckBox)getView().findViewById(R.id.cbDoujinshi);
        cbDrama = (CheckBox)getView().findViewById(R.id.cbDrama);
        cbEcchi = (CheckBox)getView().findViewById(R.id.cbEcchi);
        cbFantasy = (CheckBox)getView().findViewById(R.id.cbFantasy);
        cbGenderBender = (CheckBox)getView().findViewById(R.id.cbGenderBender);
        cbHarem = (CheckBox)getView().findViewById(R.id.cbHarem);
        cbHistorical = (CheckBox)getView().findViewById(R.id.cbHistorical);
        cbHorror = (CheckBox)getView().findViewById(R.id.cbHorror);
        cbJosei = (CheckBox)getView().findViewById(R.id.cbJosei);
        cbMartialArts = (CheckBox)getView().findViewById(R.id.cbMartialArts);
        cbMature = (CheckBox)getView().findViewById(R.id.cbMature);
        cbMecha = (CheckBox)getView().findViewById(R.id.cbMecha);
        cbMystery = (CheckBox)getView().findViewById(R.id.cbMystery);
        cbOneSHot = (CheckBox)getView().findViewById(R.id.cbOneShot);
        cbPsychological = (CheckBox)getView().findViewById(R.id.cbPsychological);
        cbRomance = (CheckBox)getView().findViewById(R.id.cbRomance);
        cbSchoolLife = (CheckBox)getView().findViewById(R.id.cbSchoolLife);
        cbSciFi = (CheckBox)getView().findViewById(R.id.cbSciFi);
        cbSeinen = (CheckBox)getView().findViewById(R.id.cbSeinen);
        cbShoujo = (CheckBox)getView().findViewById(R.id.cbShoujo);
        cbShoujoAi = (CheckBox)getView().findViewById(R.id.cbShoujoAi);
        cbShouen = (CheckBox)getView().findViewById(R.id.cbShounen);
        cbShounenAi = (CheckBox)getView().findViewById(R.id.cbShounenAi);
        cbSliceOfLife = (CheckBox)getView().findViewById(R.id.cbSliceOfLife);
        cbSmut = (CheckBox)getView().findViewById(R.id.cbSmut);
        cbSports = (CheckBox)getView().findViewById(R.id.cbSports);
        cbSupernatural = (CheckBox)getView().findViewById(R.id.cbSupernatural);
        cbTragedy = (CheckBox)getView().findViewById(R.id.cbTragedy);
        cbWebtoons = (CheckBox)getView().findViewById(R.id.cbWebtoons);
        cbYaoi = (CheckBox)getView().findViewById(R.id.cbYaoi);
        cbYuri = (CheckBox)getView().findViewById(R.id.cbYuri);

        cbList = new CheckBox[]{cbAction, cbAdult, cbAdventure, cbComedy, cbDouhinshi, cbDrama, cbEcchi, cbFantasy, cbGenderBender, cbHarem, cbHistorical,
                cbHorror, cbJosei, cbMartialArts, cbMature, cbMecha, cbMystery, cbOneSHot, cbPsychological, cbRomance, cbSchoolLife, cbSciFi, cbSeinen, cbShoujo,
                cbShoujoAi, cbShouen, cbShounenAi, cbSliceOfLife, cbSmut, cbSports, cbSupernatural, cbTragedy, cbWebtoons, cbYaoi, cbYuri};


        cbReading = (CheckBox)getView().findViewById(R.id.cbReading);
        cbCompleted = (CheckBox)getView().findViewById(R.id.cbCompleted);
        cbOnWait = (CheckBox)getView().findViewById(R.id.cbOnWait);
        cbPlanTo = (CheckBox)getView().findViewById(R.id.cbPlanTo);
        cbHiatus = (CheckBox)getView().findViewById(R.id.cbHiatus);

        cbStatusList = new CheckBox[]{cbReading, cbCompleted, cbOnWait, cbPlanTo, cbHiatus};


        for(int i = 0; i<cbList.length; i++){
            CheckBox cb = cbList[i];
            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    loadGotList(genres(), etSearchTitle.getText().toString());
                    lvSearch.setAdapter(new TemporaryAdapter(getActivity().getBaseContext(), R.layout.temporary, listGot));
                    lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent i = new Intent(getActivity().getBaseContext(), ShowMangaActivity.class);
                            i.putExtra("ID", listGot.get(position).getId());
                            startActivity(i);
                        }
                    });
                }
            });
        }


        for(int i = 0; i<cbStatusList.length; i++){
            CheckBox cb = cbStatusList[i];
            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    loadGotList(genres(), etSearchTitle.getText().toString());
                    lvSearch.setAdapter(new TemporaryAdapter(getActivity().getBaseContext(), R.layout.temporary, listGot));
                    lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent i = new Intent(getActivity().getBaseContext(), ShowMangaActivity.class);
                            i.putExtra("ID", listGot.get(position).getId());
                            startActivity(i);
                        }
                    });
                }
            });
        }



        //SET HORIZONTAL VIEWS
        genresView = (HorizontalScrollView)getView().findViewById(R.id.genresView);
        statusView = (HorizontalScrollView)getView().findViewById(R.id.statusView);

        //SET BUTTONS
        btnGenres = (Button)getView().findViewById(R.id.btnGenres);
        btnGenres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!statusB) {
                    if (genres) {
                        genres = false;
                        genresView.setVisibility(View.GONE);
                        btnGenres.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.ic_action_expand), null, null, null);
                    } else {
                        genres = true;
                        genresView.setVisibility(View.VISIBLE);
                        btnGenres.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.ic_action_collapse), null, null, null);
                    }
                }
            }
        });

        btnStatus = (Button)getView().findViewById(R.id.btnStatus);
        btnStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!genres) {
                    if (statusB) {
                        statusB = false;
                        statusView.setVisibility(View.GONE);
                        btnStatus.setCompoundDrawablesWithIntrinsicBounds(null, null, getActivity().getResources().getDrawable(R.drawable.ic_action_expand), null);
                    } else {
                        statusB = true;
                        statusView.setVisibility(View.VISIBLE);
                        btnStatus.setCompoundDrawablesWithIntrinsicBounds(null, null, getActivity().getResources().getDrawable(R.drawable.ic_action_collapse), null);
                    }
                }
            }
        });
    }



    public void loadGotList(List<String> l, String s){
        s = s.toLowerCase();

        if(!listGot.isEmpty())
            listGot.clear();

        if(!l.isEmpty()||s.length()>=2) {
            for (int i = 0; i < listAll.size(); i++) {
                List<String> g = listAll.get(i).getGenres();
                String t = listAll.get(i).getTitle().toLowerCase();
                String a = listAll.get(i).getAuthor().toLowerCase();
                ArrayList<String> status = new ArrayList<>();
                for(int j = 0; j<cbStatusList.length;j++){
                    if(cbStatusList[j].isChecked())
                        status.add(""+j);
                }
                boolean b = false;
                if(!s.equals(""))
                    b = t.contains(s)||a.contains(s);
                else{
                    b = !l.isEmpty();
                }
                if(status.size()>0&&!status.contains(String.valueOf(listAll.get(i).getStatus()))) {
                    b = false;
                }
                if (g.containsAll(l)&&b) {
                    listGot.add(listAll.get(i));
                }
            }
        }
    }


    public List<String> genres (){
        String[] genreList = {"Action","Adult", "Adventure", "Comedy", "Doujinshi", "Drama", "Ecchi","Fantasy",
                "Gender Bender","Harem","Historical","Horror","Josei","Martial Arts","Mature","Mecha","Mystery","One Shot",
                "Psychological","Romance","School Life","Sci-fi","Seinen","Shoujo","Shoujo Ai","Shounen","Shounen Ai",
                "Slice of Life","Smut","Sports","Supernatural","Tragedy","Webtoons","Yaoi","Yuri"};

        ArrayList<String> list = new ArrayList<String>();

        for(int i = 0; i<genreList.length;i++){
            if(cbList[i].isChecked()){
                list.add(genreList[i]);
            }
        }

        return list;
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void deleteManga(final int id){
        new AlertDialog.Builder(getActivity())
                .setTitle("Delete manga")
                .setMessage("Are you sure you want to delete this manga?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        listGot.remove(db.getManga(id));
                        lvSearch.setAdapter(new TemporaryAdapter(getActivity().getBaseContext(), R.layout.temporary, listGot));
                        db.deleteManga(id);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()==R.id.lvSearch) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            Manga m;
            m = listGot.get(info.position);

            menu.setHeaderTitle(m.getTitle());
            String[] menuItems = getResources().getStringArray(R.array.manga_context);
            for (int i = 0; i<menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.manga_context);

        Manga m;
        m = listGot.get(info.position);
        final Manga ma = m;

        if(menuItems[menuItemIndex].equals("Edit")){
            Intent i = new Intent(getActivity().getBaseContext(), AddMangaActivity.class);
            i.putExtra("ID", m.getId());
            startActivity(i);
        }
        else if(menuItems[menuItemIndex].equals("Delete")){
            deleteManga(ma.getId());
        }
        else if(menuItems[menuItemIndex].equals("Set Status")){
            AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
            b.setTitle("Set Status");
            String[] statusList = {"Reading", "Completed", "On Wait", "Plan To Read", "Hiatus"};
            b.setItems(statusList, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                    ma.setStatus(which);
                    db.updateManga(ma, getActivity().getBaseContext());
                }

            });

            b.show();
        }
        else if(menuItems[menuItemIndex].equals("+1 Read")){
            if(ma.getReadChapter()==ma.getReleasedChapter())
                ma.setReleasedChapter(ma.getReleasedChapter()+1);
            ma.setReadChapter(ma.getReadChapter()+1);
            if(ma.getReadChapter()==ma.getReleasedChapter())
                ma.setReleasedChapter(ma.getReleasedChapter()+1);
            db.updateManga(ma, getActivity().getBaseContext());
        }
        else if(menuItems[menuItemIndex].equals("+1 Released")){
            ma.setReleasedChapter(ma.getReleasedChapter() + 1);
            db.updateManga(ma, getActivity().getBaseContext());
        }



        return true;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
