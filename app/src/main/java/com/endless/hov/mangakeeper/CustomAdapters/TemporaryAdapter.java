package com.endless.hov.mangakeeper.CustomAdapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.endless.hov.mangakeeper.Manga;
import com.endless.hov.mangakeeper.R;

import java.io.File;
import java.util.List;

/**
 * Created by Barış on 30.6.2015.
 */
public class TemporaryAdapter extends ArrayAdapter<Manga>{


    final String coverPath = Environment.getExternalStorageDirectory().toString()+"/MankaKeeper/";


    public TemporaryAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public TemporaryAdapter(Context context, int resource, List<Manga> objects) {
        super(context, resource, objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v==null){
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.temporary, null);
        }


        Manga m = getItem(position);

        ImageView iv = (ImageView)v.findViewById(R.id.imageView);
        TextView tvTitle = (TextView)v.findViewById(R.id.tvTitle);
        TextView tvStatus = (TextView)v.findViewById(R.id.tvStatus);
        TextView tvAuthor = (TextView)v.findViewById(R.id.tvAuthor);
        TextView tvUpdate = (TextView)v.findViewById(R.id.tvUpdate);
        TextView tvChapter = (TextView)v.findViewById(R.id.tvChapter);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());


        String field1 = preferences.getString("field1", "status"), field2 = preferences.getString("field2", "update"),
                field3 = preferences.getString("field3", "author"), field4 = preferences.getString("field4", "both");


        if(m!=null){
            if(iv!=null){
                File imgFile = new File(coverPath + m.getId() + ".jpg");
                if(imgFile.exists()){
                    Bitmap bmp = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    iv.setImageBitmap(bmp);
                }
                else
                    iv.setBackgroundColor(Color.BLACK);
            }
            if(tvTitle!=null) {
                if(m.getTitle().length()<50)
                    tvTitle.setText(m.getTitle());
                else
                    tvTitle.setText(m.getTitle().substring(0,50)+"...");

            }
            if(tvAuthor!=null) {
                //tvAuthor.setText(m.getAuthor());
                switch (field3){
                    case "title":
                        tvAuthor.setText(m.getTitle());
                        break;
                    case "author":
                        tvAuthor.setText(m.getAuthor());
                        break;
                    case "status":
                        tvAuthor.setText(m.getStatusAsString());
                        break;
                    case "update":
                        tvAuthor.setText(m.getUpdateTimeString());
                        break;
                    case "add":
                        tvAuthor.setText(m.getAddTimeString());
                        break;
                    case "both":
                        tvAuthor.setText(m.getReadChapter()+"/"+m.getReleasedChapter());
                        break;
                    case "read":
                        tvAuthor.setText(""+m.getReadChapter());
                        break;
                    case "released":
                        tvAuthor.setText(""+m.getReleasedChapter());
                        break;
                    case "details":
                        if(m.getDetail().length()<12)
                            tvAuthor.setText(m.getDetail());
                        else
                            tvAuthor.setText(m.getDetail().substring(0,10)+"...");
                        break;
                    case "history":
                        String s =m.historyOuput().get(m.historyOuput().size()-1);
                        if(s.length()<12)
                            tvAuthor.setText(s);
                        else
                            tvAuthor.setText(s.substring(0,10) + "...");
                        break;
                }
            }
            if(tvStatus!=null) {
                //tvStatus.setText(m.getStatusAsString());
                switch (field1){
                    case "title":
                        tvStatus.setText(m.getTitle());
                        break;
                    case "author":
                        tvStatus.setText(m.getAuthor());
                        break;
                    case "status":
                        tvStatus.setText(m.getStatusAsString());
                        break;
                    case "update":
                        tvStatus.setText(m.getUpdateTimeString());
                        break;
                    case "add":
                        tvStatus.setText(m.getAddTimeString());
                        break;
                    case "both":
                        tvStatus.setText(m.getReadChapter()+"/"+m.getReleasedChapter());
                        break;
                    case "read":
                        tvStatus.setText(""+m.getReadChapter());
                        break;
                    case "released":
                        tvStatus.setText(""+m.getReleasedChapter());
                        break;
                    case "details":
                        if(m.getDetail().length()<12)
                            tvStatus.setText(m.getDetail());
                        else
                            tvStatus.setText(m.getDetail().substring(0,10)+"...");
                        break;
                    case "history":
                        String s =m.historyOuput().get(m.historyOuput().size()-1);
                        if(s.length()<12)
                            tvStatus.setText(s);
                        else
                            tvStatus.setText(s.substring(0,10) + "...");
                        break;
                }
            }
            if(tvUpdate!=null) {
                //tvUpdate.setText(m.getUpdateTimeString());
                switch (field2){
                    case "title":
                        tvUpdate.setText(m.getTitle());
                        break;
                    case "author":
                        tvUpdate.setText(m.getAuthor());
                        break;
                    case "status":
                        tvUpdate.setText(m.getStatusAsString());
                        break;
                    case "update":
                        tvUpdate.setText(m.getUpdateTimeString());
                        break;
                    case "add":
                        tvUpdate.setText(m.getAddTimeString());
                        break;
                    case "both":
                        tvUpdate.setText(m.getReadChapter()+"/"+m.getReleasedChapter());
                        break;
                    case "read":
                        tvUpdate.setText(""+m.getReadChapter());
                        break;
                    case "released":
                        tvUpdate.setText(""+m.getReleasedChapter());
                        break;
                    case "details":
                        if(m.getDetail().length()<12)
                            tvUpdate.setText(m.getDetail());
                        else
                            tvUpdate.setText(m.getDetail().substring(0,10)+"...");
                        break;
                    case "history":
                        String s =m.historyOuput().get(m.historyOuput().size()-1);
                        if(s.length()<12)
                            tvUpdate.setText(s);
                        else
                            tvUpdate.setText(s.substring(0,10) + "...");
                        break;
                }
            }
            if(tvChapter!=null) {
                //tvChapter.setText(m.getReadChapter() + "/" + m.getReleasedChapter());
                switch (field4){
                    case "title":
                        tvChapter.setText(m.getTitle());
                        break;
                    case "author":
                        tvChapter.setText(m.getAuthor());
                        break;
                    case "status":
                        tvChapter.setText(m.getStatusAsString());
                        break;
                    case "update":
                        tvChapter.setText(m.getUpdateTimeString());
                        break;
                    case "add":
                        tvChapter.setText(m.getAddTimeString());
                        break;
                    case "both":
                        tvChapter.setText(m.getReadChapter()+"/"+m.getReleasedChapter());
                        break;
                    case "read":
                        tvChapter.setText(""+m.getReadChapter());
                        break;
                    case "released":
                        tvChapter.setText(""+m.getReleasedChapter());
                        break;
                    case "details":
                        if(m.getDetail().length()<12)
                            tvChapter.setText(m.getDetail());
                        else
                            tvChapter.setText(m.getDetail().substring(0,10)+"...");
                        break;
                    case "history":
                        String s =m.historyOuput().get(m.historyOuput().size()-1);
                        if(s.length()<12)
                            tvChapter.setText(s);
                        else
                            tvChapter.setText(s.substring(0,10) + "...");
                        break;
                }
            }
        }


        return v;

    }
}
