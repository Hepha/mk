package com.endless.hov.mangakeeper;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.endless.hov.mangakeeper.Scraper.Scraper;
import com.endless.hov.mangakeeper.model.Page;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.IOException;
import java.util.List;

class CustomPagerAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    Bitmap[] bitmaps;
    List<Page> pages;
    Boolean activityPaused = false;

    AppBarLayout appBar, bottomBar;
    TextView tvPage;
    ImageButton ibRefresh;

    boolean hidden = false;


    //NEW SYSTEM STUFF
    String url;
    String[] pagesArr;


    public CustomPagerAdapter(Context context, List<Page> pages, TextView tvPage, AppBarLayout appBar, AppBarLayout bottomBar, ImageButton ibRefresh) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        bitmaps = new Bitmap[pages.size()];
        this.pages = pages;
        loadBitmaps(0);
        this.tvPage = tvPage;
        this.appBar = appBar;
        this.bottomBar = bottomBar;
        this.ibRefresh = ibRefresh;
    }

    public CustomPagerAdapter(Context context, String firstPage, int limit, String url, TextView tvPage, AppBarLayout appBar, AppBarLayout bottomBar, ImageButton ibRefresh) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        pagesArr = new String[limit];
        pagesArr[0] = firstPage;
        bitmaps = new Bitmap[limit];
        this.url = url;
        this.tvPage = tvPage;
        this.appBar = appBar;
        this.bottomBar = bottomBar;
        this.ibRefresh = ibRefresh;
        loadURLS(2);
        loadBitmaps(0);
    }

    @Override
    public int getCount() {
        return bitmaps.length;
    }

    public void setSize(int size) {
        bitmaps = new Bitmap[size];
    }

    public void setActivityPaused(boolean b) {
        activityPaused = b;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

        final TouchImageView imageView = (TouchImageView) itemView.findViewById(R.id.imageView);
        loadBitmap(imageView, position);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideInfo();
            }
        });

        ibRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        container.addView(itemView);
        return itemView;
    }

    public void setBitmap(Bitmap bmp, int i) {
        bitmaps[i] = bmp;
    }

    private void loadBitmap(final TouchImageView imageView, final int position) {
        if (bitmaps[position] != null) {

            imageView.setImageBitmap(bitmaps[position]);
            int h = bitmaps[position].getHeight(), w = bitmaps[position].getWidth();
            if(h/w>16/9){
                imageView.setZoom(imageView.getWidth()/imageView.getMeasuredWidth());
            }
        } else {
            Handler mHandler = new Handler();
            mHandler.postDelayed(new Runnable() {
                public void run() {
                    loadBitmap(imageView, position);
                }
            }, 200);
        }
    }

    public void loadURLS(final int page) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    pagesArr[page-1] = Scraper.getChapterPageWithUrl(url, page);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(runnable).start();

        if (page < pagesArr.length&& !activityPaused) {
            loadURLS(page + 1);
        }
    }

    public void loadBitmaps(final int count) {
        if (pagesArr[count] != null) {
            final ImageLoader imageLoader = ImageLoader.getInstance();


            imageLoader.loadImage(pagesArr[count], new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    /*if(count<pages.size()-1&&!activityPaused)
                        loadBitmaps(count + 1);*/
                    loadBitmaps(count);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    if (bitmaps[count] == null)
                        bitmaps[count] = loadedImage;
                    //bitmaps[count] = loadedImage;
                    if (count < pagesArr.length - 1 && !activityPaused)
                        loadBitmaps(count + 1);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });
        } else {
            Handler mHandler = new Handler();
            mHandler.postDelayed(new Runnable() {
                public void run() {
                    loadBitmaps(count);
                }
            }, 200);
        }

    }

    public void hideInfo() {
        if (hidden) {
            appBar.animate().translationY(0).setInterpolator(new DecelerateInterpolator()).start();
            bottomBar.animate().translationY(0).setInterpolator(new DecelerateInterpolator()).start();
            hidden = false;
        } else {
            appBar.animate().translationY(-appBar.getBottom()).setInterpolator(new AccelerateInterpolator()).start();
            bottomBar.animate().translationY(appBar.getBottom()).setInterpolator(new AccelerateInterpolator()).start();
            hidden = true;
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}