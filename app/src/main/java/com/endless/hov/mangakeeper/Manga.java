package com.endless.hov.mangakeeper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Barış on 26.6.2015.
 */
public class Manga {
    protected int id;
    protected String apiID;
    protected String title, author, detail;
    protected int releasedChapter, readChapter, releasedVolume, readVolume, status, favorite;
    protected List<String> genres, notes, history;
    protected long updateTime, addTime;

    String[] genreList = {"Action","Adult", "Adventure", "Comedy", "Doujinshi", "Drama", "Ecchi","Fantasy",
            "Gender Bender","Harem","Historical","Horror","Josei","Martial Arts","Mature","Mecha","Mystery","One Shot",
            "Psychological","Romance","School Life","Sci-fi","Seinen","Shoujo","Shoujo Ai","Shounen","Shounen Ai",
            "Slice of Life","Smut","Sports","Supernatural","Tragedy","Webtoons","Yaoi","Yuri"};

    String[] statusList = {"Reading", "Completed", "On Wait", "Plan To Read", "Hiatus"};



    public Manga(int id, String apiID, String title, String author, String detail, String notes, int releasedChapter,
                 int readChapter, int releasedVolume, int readVolume, int status, List<String> genres, long updateTime, long addTime, int favorite, String history) {
        this.id = id;
        this.apiID = apiID;
        this.title = title;
        this.author = author;
        this.detail = detail;
        this.notes = stringToNotes(notes);
        this.releasedChapter = releasedChapter;
        this.readChapter = readChapter;
        this.releasedVolume = releasedVolume;
        this.readVolume = readVolume;
        this.status = status;
        this.genres = genres;
        this.updateTime = updateTime;
        this.addTime = addTime;
        this.favorite = favorite;
        this.history = stringToNotes(history);
    }

    public Manga(int id, String apiID, String title, String author, String detail, String notes, int releasedChapter,
                 int readChapter, int releasedVolume, int readVolume, int status, String genres, long updateTime, long addTime, int favorite,String history) {
        this.id = id;
        this.apiID = apiID;
        this.title = title;
        this.author = author;
        this.detail = detail;
        this.notes = stringToNotes(notes);
        this.releasedChapter = releasedChapter;
        this.readChapter = readChapter;
        this.releasedVolume = releasedVolume;
        this.readVolume = readVolume;
        this.status = status;
        this.genres = stringToGenre(genres);
        this.updateTime = updateTime;
        this.addTime = addTime;
        this.favorite = favorite;
        this.history = stringToNotes(history);
    }

    public Manga(String apiID,String title,  String author, String detail, String notes, int releasedChapter,
                 int readChapter, int releasedVolume, int readVolume, int status, List<String> genres, long updateTime, long addTime, int favorite) {
        this.apiID = apiID;
        this.title = title;
        this.author = author;
        this.detail = detail;
        this.notes = stringToNotes(notes);
        this.releasedChapter = releasedChapter;
        this.readChapter = readChapter;
        this.releasedVolume = releasedVolume;
        this.readVolume = readVolume;
        this.status = status;
        this.genres = genres;
        this.updateTime = updateTime;
        this.addTime = addTime;
        this.favorite = favorite;
        this.history = new ArrayList<String>();
        this.history.add("1=="+addTime);
    }

    public Manga(String apiID, String title, String author, String detail, String notes, int releasedChapter,
                 int readChapter, int releasedVolume, int readVolume, int status, String genres, long updateTime, long addTime, int favorite) {
        this.apiID = apiID;
        this.title = title;
        this.author = author;
        this.detail = detail;
        this.notes = stringToNotes(notes);
        this.releasedChapter = releasedChapter;
        this.readChapter = readChapter;
        this.releasedVolume = releasedVolume;
        this.readVolume = readVolume;
        this.status = status;
        this.genres = stringToGenre(genres);
        this.updateTime = updateTime;
        this.addTime = addTime;
        this.favorite = favorite;
        this.history = new ArrayList<String>();
        this.history.add("1=="+addTime);
    }


    public String genreToString(List<String> genres){
        String str = "";

        for(int i = 0;i<genreList.length;i++){
            if(genres.contains(genreList[i]))
                str+="1";
            else
                str+="0";
        }
        return str;
    }

    public List<String> stringToGenre(String s){
        ArrayList<String> list = new ArrayList<String>();

        for(int i = 0; i<genreList.length; i++){
            int c = Integer.parseInt(""+s.charAt(i));

            if(c==1)
                list.add(genreList[i]);

        }
        return list;
    }

    public List<String> stringToNotes(String s){
        ArrayList<String> list = new ArrayList<String>();

        String[] arr = s.split("-----");

        if(!s.equals("")) {
            for (int i = 0; i < arr.length; i++)
                list.add(arr[i]);
        }

        return list;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public List<String> getNotes() {
        return notes;
    }

    public String getNotesAsString(){
        String s = "";

        for(int i = 0; i<getNotes().size(); i++){
            s += getNotes().get(i);
            if(i!=(getNotes().size()-1))
                s+="-----";
        }

        return s;
    }

    public void setNotes(List<String> notes) {
        this.notes = notes;
    }

    public void setNotes(String s){this.notes = stringToNotes(s);}


    public int getReleasedChapter() {
        return releasedChapter;
    }

    public void setReleasedChapter(int releasedChapter) {
        this.releasedChapter = releasedChapter;
    }

    public int getReadChapter() {
        return readChapter;
    }

    public void setReadChapter(int readChapter) {
        this.readChapter = readChapter;
    }

    public int getReleasedVolume() {
        return releasedVolume;
    }

    public void setReleasedVolume(int releasedVolume) {
        this.releasedVolume = releasedVolume;
    }

    public int getReadVolume() {
        return readVolume;
    }

    public void setReadVolume(int readVolume) {
        this.readVolume = readVolume;
    }

    public int getStatus() {
        return status;
    }

    public String getStatusAsString(){return statusList[status];}

    public void setStatus(int status) {
        this.status = status;
    }

    public List<String> getGenres() {
        return genres;
    }

    public String getGenresString(){
        return genreToString(getGenres());
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public void setGenres(String s) {
        this.genres = stringToGenre(s);
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public String getUpdateTimeString(){
        long l = getUpdateTime();

        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");
        Date resultDate = new Date(l);
        return sdf.format(resultDate);
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public long getAddTime() {
        return addTime;
    }
    public String getAddTimeString(){
        long l = getAddTime();

        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");
        Date resultDate = new Date(l);
        return sdf.format(resultDate);
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }

    public int getFavorite() {
        return favorite;
    }

    public boolean getFavoriteBool(){
        boolean b = true;
        if(getFavorite() == 0)
            b = false;
        return b;
    }

    public void setFavorite(int favorite) {
        this.favorite = favorite;
    }

    public void setFavorite(Boolean b){
        if(b)
            favorite = 1;
        else
            favorite = 0;
    }

    public static String getGenre(int id){
        String[] list = {"Action","Adult", "Adventure", "Comedy", "Doujinshi", "Drama", "Ecchi","Fantasy",
                "Gender Bender","Harem","Historical","Horror","Josei","Martial Arts","Mature","Mecha","Mystery","One Shot",
                "Psychological","Romance","School Life","Sci-fi","Seinen","Shoujo","Shoujo Ai","Shounen","Shounen Ai",
                "Slice of Life","Smut","Sports","Supernatural","Tragedy","Webtoons","Yaoi","Yuri"};

        return list[id];
    }

    public static int getGenreID(String s){
         String[] list = {"Action","Adult", "Adventure", "Comedy", "Doujinshi", "Drama", "Ecchi","Fantasy",
                "Gender Bender","Harem","Historical","Horror","Josei","Martial Arts","Mature","Mecha","Mystery","One Shot",
                "Psychological","Romance","School Life","Sci-fi","Seinen","Shoujo","Shoujo Ai","Shounen","Shounen Ai",
                "Slice of Life","Smut","Sports","Supernatural","Tragedy","Webtoons","Yaoi","Yuri"};

        int id = -1;
        for (int i = 0;i<list.length;i++){
            if(s.equals(list[i])) {
                id = i;
                break;
            }
        }
        return id;
    }


    public List<String> getHistory() {
        return history;
    }

    public String getHistoryAsString(){
        String s = "";

        for(int i = 0; i<getHistory().size(); i++){
            s += getHistory().get(i);
            if(i!=(getHistory().size()-1))
                s+="-----";
        }

        return s;
    }

    public void setHistory(List<String> history) {
        this.history = history;
    }

    public List<String> historyOuput(){
        ArrayList<String> list = new ArrayList<String>();
        List<String> history = getHistory();

        for(int i= 0; i< history.size();i++){
            String [] arr = history.get(i).split("==");
            int type = Integer.parseInt(arr[0]);
            switch (type){
                case 1:
                    list.add(getTitle()+"=="+arr[1]+"=="+"Manga Added!");
                    break;
                case 2:
                    int aoc = Integer.parseInt(arr[2]), start = Integer.parseInt(arr[3]), end = start+aoc-1;
                    list.add(getTitle()+"=="+arr[1]+"=="+aoc+" chapter read!("+start+"-"+end+")");
                    break;
                case 3:
                    aoc = Integer.parseInt(arr[2]);
                    start = Integer.parseInt(arr[3]);
                    end = start-aoc+1;
                    list.add(getTitle()+"=="+arr[1]+"=="+aoc+" chapter unread!("+start+"-"+end+")");
                    break;
                case 4:
                    aoc = Integer.parseInt(arr[2]);
                    start = Integer.parseInt(arr[3]);
                    end = start+aoc-1;
                    list.add(getTitle()+"=="+arr[1]+"=="+aoc+" chapter released!("+start+"-"+end+")");
                    break;
                case 5:
                    aoc = Integer.parseInt(arr[2]);
                    start = Integer.parseInt(arr[3]);
                    end = start-aoc+1;
                    list.add(getTitle()+"=="+arr[1]+"=="+aoc+" chapter unreleased!("+start+"-"+end+")");
                    break;
                case 6:
                    String old = arr[2], ne = arr[3];
                    list.add(getTitle()+"=="+arr[1]+"=="+"Title changed from "+old+" to "+ ne);
                    break;
                case 7:
                    old = arr[2];
                    ne = arr[3];
                    list.add(getTitle()+"=="+arr[1]+"=="+"Author changed from "+old+" to "+ ne);
                    break;
                case 8:
                    list.add(getTitle()+"=="+arr[1]+"=="+"Details changed!");
                    break;
                case 9:
                    list.add(getTitle()+"=="+arr[1]+"=="+"Cover changed!");
                    break;
                case 10:
                    int from = Integer.parseInt(arr[2]), to = Integer.parseInt(arr[3]);
                    list.add(getTitle()+"=="+arr[1]+"=="+"Status changed from " + statusList[from] + " to ++" + statusList[to]);
                    break;
                case 11:
                    String genre = arr[2];
                    list.add(getTitle()+"=="+arr[1]+"=="+"Genre added: "+ genre);
                    break;
                case 12:
                    genre = arr[2];
                    list.add(getTitle()+"=="+arr[1]+"=="+"Genre removed: "+ genre);
                    break;
                case 13:
                    String note = arr[2];
                    list.add(getTitle()+"=="+arr[1]+"=="+"Note added: " + note);
                    break;
                case 14:
                    note = arr[2];
                    list.add(getTitle()+"=="+arr[1]+"=="+"Note removed: " + note);
                    break;
                case 15:
                    list.add(getTitle()+"=="+arr[1]+"=="+"Manga favorited!");
                    break;
                case 16:
                    list.add(getTitle()+"=="+arr[1]+"=="+"Manga unfavorited!");
                    break;
            }
        }


        return list;
    }

    public String getApiID() {
        return apiID;
    }

    public void setApiID(String apiID) {
        this.apiID = apiID;
    }


    //HISTORY ADDERS

    public void addHistoryRead(int amountOfChapter, int startChapter){
        history.add("2=="+ System.currentTimeMillis()+"=="+amountOfChapter+"=="+startChapter);
    }
    public void addHistoryUnread(int amountOfChapter, int startChapter){
        history.add("3=="+ System.currentTimeMillis()+"=="+amountOfChapter+"=="+startChapter);
    }
    public void addHistoryReleased(int amountOfChapter, int startChapter){
        history.add("4=="+ System.currentTimeMillis()+"=="+amountOfChapter+"=="+startChapter);
    }
    public void addHistoryUnreleased(int amountOfChapter, int startChapter){
        history.add("5=="+ System.currentTimeMillis()+"=="+amountOfChapter+"=="+startChapter);
    }
    public void addHistoryTitleChange(String old, String ne){
        history.add("6=="+ System.currentTimeMillis()+"=="+old+"=="+ne);
    }
    public void addHistoryAuthorChange(String old, String ne){
        history.add("7=="+ System.currentTimeMillis()+"=="+old+"=="+ne);
    }
    public void addHistoryDetailChange(){
        history.add("8=="+ System.currentTimeMillis());
    }
    public void addHistoryCoverChange(){
        history.add("9=="+ System.currentTimeMillis());
    }
    public void addHistoryStatusChange(int old, int ne){
        history.add("10=="+ System.currentTimeMillis()+"=="+old+"=="+ne);
    }
    public void addHistoryGenreAdd(String genre){
        history.add("11=="+ System.currentTimeMillis()+"=="+genre);
    }
    public void addHistoryGenreRemove(String genre){
        history.add("12=="+ System.currentTimeMillis()+"=="+genre);
    }
    public void addHistoryNoteAdd(String note){
        history.add("13=="+ System.currentTimeMillis()+"=="+note);
    }
    public void addHistoryNoteRemove(String note){
        history.add("14=="+ System.currentTimeMillis()+"=="+note);
    }
    public void addHistoryFavorite(){
        history.add("15=="+ System.currentTimeMillis());
    }
    public void addHistoryUnfavorite(){
        history.add("16=="+ System.currentTimeMillis());
    }


}
