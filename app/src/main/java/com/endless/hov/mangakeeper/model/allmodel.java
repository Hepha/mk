package com.endless.hov.mangakeeper.model;


import com.google.gson.annotations.Expose;


public class allmodel {

    @Expose
    private String mangaId;
    @Expose
    private String name;

    /**
     *
     * @return
     * The mangaId
     */
    public String getMangaId() {
        return mangaId;
    }

    /**
     *
     * @param mangaId
     * The mangaId
     */
    public void setMangaId(String mangaId) {
        this.mangaId = mangaId;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

}